program khorr_vds_adi
    implicit none

    double precision, allocatable :: vl_enth_prev(:),vl_u_prev(:),vl_psi_prev(:)
    double precision, allocatable :: vl_enth(:),vl_u(:),vl_psi(:),vl_psi_out_prev(:)
    double precision, allocatable :: vl_u_out(:),vl_psi_out(:),va(:,:),vr(:),vx(:),vl_u_out_prev(:)
    double precision, allocatable :: eta(:),il_u_prev(:),il_v_prev(:),il_rho_prev(:),il_p_prev(:)
    double precision :: Pr,gam,c,v_dy,dx,c7,z,z_prev,del_prev,vl_pres_prev,lam,vl_pres_out_prev
    double precision :: c1,c2,c3,c5,vl_pres,del,rp,t,err_out(3),err_out_max,err_out_max_old,vl_pres_out
    double precision :: del1,enth1,xi1,p1,p2,u1,u2,u4,u5,u7,psi1,psi2,psi3
    integer :: v_ny,v_n,i_ny,nx,v_fid,i_fid,e,j,vcount,mc,m,i,k,f,vcount_s
    character*25 :: str1,str2,str,str_step,str_count
    character(*), parameter :: vlip = 'c:\users\sampson\documents\github\res\research\vl_ip\' !'
    character(*), parameter :: ilip = 'c:\users\sampson\documents\github\res\research\il_ip\' !'
    character(*), parameter :: vlds = 'c:\users\sampson\documents\github\res\research\vl_ds\' !'

    Pr = 1.d0; gam = 14.d-1; c = 1.d0

    v_ny = 501; i_ny = 101

    v_n = 2*v_ny; v_dy = 10.d0/dble(v_ny - 1)

    nx = 101; dx = 1.d0/dble(nx - 1)

    v_fid = 11; i_fid = 12; e = 0

    allocate(vl_enth_prev(1:v_ny)); allocate(vl_u_prev(1:v_ny)); allocate(vl_psi_prev(1:v_ny))
    allocate(vl_enth(1:v_ny)); allocate(vl_u(1:v_ny)); allocate(vl_psi(1:v_ny))
    allocate(vl_u_out(1:v_ny)); allocate(vl_psi_out(1:v_ny))
    allocate(va(1:v_n+1,1:v_n+1)); allocate(vr(1:v_n+1)); allocate(vx(1:v_n+1))
    allocate(eta(1:i_ny)); allocate(il_u_prev(1:i_ny)); allocate(il_v_prev(1:i_ny))
    allocate(il_rho_prev(1:i_ny)); allocate(il_p_prev(1:i_ny)); allocate(vl_u_out_prev(1:v_ny))
    allocate(vl_psi_out_prev(1:v_ny))

    open(unit=9, file=vlip//"c3con_pr1adi.dat", status="old", action="read")
    open(unit=10, file=ilip//"c5con.dat", status="old", action="read")
    read(9,*), c3
    read(10,*), c5
    close(unit=9)
    close(unit=10)

    do e = 1,nx
    print *, "e", e
    z = dble(e)*dx
    z_prev = dble(e-1)*dx

    !----------------------------------------------------------
    !                Previous values
    !----------------------------------------------------------

	if (e == 1) then
        !----------------Viscous Layer (IP)-----------------------
        open(unit=v_fid, file=vlip//'vl_pr1adi.dat', status='old', action='read')
		do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j)
		enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        !----------------Inviscid Layer (IP)----------------------
        open(unit=i_fid, file=ilip//"il_flow_interpm1.dat", status="old", action="read")
		do j = 1,i_ny
            read(i_fid,*), eta(j), il_u_prev(j), il_v_prev(j), il_rho_prev(j), il_p_prev(j)!, xx1,xx2,xx3,xx4
		enddo
        close(unit = i_fid)
        i_fid = i_fid + 2

        del_prev = 1.d0; vl_pres_prev = 1.d0

        c2 = ((c*c3)/il_p_prev(1))**(1.d0/4.d0)
        c1 = (c*c3)/(c2*c2)
 
	else
        !----------------Viscous Layer (DS)-----------------------
        write(str, "('vl_ds_',i3.3,'.dat')"), e-1
        open(unit=v_fid, file=vlds//str, status='old', action='read')
		do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j), vl_pres_prev, del_prev
		enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
	endif

    vl_pres = vl_pres_prev; del = del_prev
    err_out_max_old = 1.d2

    !===============================================================
    !                        Viscous Layer
    !===============================================================

    !---------------------------------------------------------------
    !                  Initial Guesses (Viscous)
    !---------------------------------------------------------------
    vl_enth(1:v_ny) = vl_enth_prev(1:v_ny)
    vl_u(1:v_ny) = vl_u_prev(1:v_ny)
    vl_psi(1:v_ny) = vl_psi_prev(1:v_ny)
    !---------------------------------------------------------------
    !                         Loop Start
    !---------------------------------------------------------------
    vcount = 0
1   vcount = vcount + 1
    print *, "vcount", vcount
    !---------------------------------------------------------------
    !                    Matrix Initialization
    !---------------------------------------------------------------
    va(1:v_n+1,1:v_n+1) = 0.d0; vr(1:v_n+1) = 0.d0

    !Wall BCs
    va(1,1) = 1.d0
    vr(1) = -vl_u_prev(1) - vl_u(1)

    va(2,2) = 1.d0
    vr(2) = -vl_psi_prev(1) - vl_psi(1)

    mc = 1

    do j = 2, v_ny-1
        del1 = del + del_prev; enth1 = vl_enth(j) + vl_enth_prev(j)
        p1 = vl_pres + vl_pres_prev; p2 = vl_pres - vl_pres_prev
        psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j)
        psi3 = vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j)
        u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j)
        u4 = vl_u(j+1) + vl_u(j) + vl_u_prev(j+1) + vl_u_prev(j)
        u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1)
        u7 = vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1)
        xi1 = z + z_prev

    !Streamfunction elements
        va(j+mc,(j+mc)-2) = -v_dy*del1
        va(j+mc,(j+mc)-1) = -4.d0
        va(j+mc,j+mc) = -v_dy*del1
        va(j+mc,(j+mc)+1) = 4.d0
    !----Streamfunction RHS
        vr(j+mc) = v_dy*del1*u4 - 4.d0*psi3

        mc = mc + 1

    !Momentum elements
        va(j+mc,(j+mc)-3) = p1*(16.d0*dx*p1 - v_dy*dx*del1*psi1 - 4.d0*v_dy*xi1*del1*psi2)
        va(j+mc,(j+mc)-1) = 4.d0*v_dy*v_dy*((gam-1.d0)/gam)*xi1*del1*del1*p2*u1&
                    - p1*( 32.d0*dx*p1 + 2.d0*v_dy*v_dy*dx*((gam-1.d0)/gam)*del1*del1*u1 + 4.d0*v_dy*v_dy*xi1*del1*del1*(u2+u1) )
        va(j+mc,j+mc)     = p1*u5*del1*v_dy*(dx + 4.d0*xi1)
        va(j+mc,(j+mc)+1) = p1*(16.d0*dx*p1 + v_dy*dx*del1*psi1 + 4.d0*v_dy*xi1*del*psi2)
        va(j+mc,v_n+1)    = 32.d0*dx*p1*u7 - 4.d0*v_dy*v_dy*xi1*del1*del1*u1*u2 + v_dy*del1*u5*(dx*psi1 + 4.d0*xi1*psi2)&
                    + v_dy*v_dy*((gam-1.d0)/gam)*del1*del1*(enth1 - 25.d-2*u1*u1)*(4.d0*dx - 8.d0*xi1)
    !----Momentum RHS
        vr(j+mc) = 4.d0*v_dy*v_dy*xi1*del1*del1*p1*u1*u2 - 16.d0*dx*p1*p1*u7 - v_dy*del1*p1*u5*(4.d0*xi1*psi2 + dx*psi1)&
                    + v_dy*v_dy*((gam-1.d0)/gam)*del1*del1*(enth1 - 25.d-2*u1*u1)*(8.d0*xi1*p2 - 4.d0*dx*p1)
    enddo
    !---BL BCs
    va(v_n-1,v_n-1) = 1.d0
    vr(v_n-1) = 2.d0 - vl_u_prev(v_ny) - vl_u(v_ny)

    va(v_n,v_n-4) = -v_dy*(del_prev + del)
    va(v_n,v_n-3) = -4.d0
    va(v_n,v_n-1) = -v_dy*(del_prev + del)
    va(v_n,v_n)   = 4.d0
    vr(v_n)   = v_dy*(del_prev + del)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
           - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))

    !Pressure Elements
    va(v_n+1,1) = 25.d-2*(vl_u(1) + vl_u_prev(1))

    mc = 1
    do j = 2,v_ny-1
        va(v_n+1,j+mc) = 5.d-1*(vl_u(j) + vl_u_prev(j))
        mc = mc + 1
    enddo

    va(v_n+1,v_n-1) = 25.d-2*(vl_u(v_ny) + vl_u_prev(v_ny))
    va(v_n+1,v_n+1) = (2.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0))
    !Pressure RHS
    rp = ((vl_enth_prev(1) + vl_enth(1)) - 25.d-2*(vl_u_prev(1) + vl_u(1))**2.d0)&
       + ((vl_enth_prev(v_ny) + vl_enth(v_ny)) - 25.d-2*(vl_u_prev(v_ny) + vl_u(v_ny))**2.d0)
    do j = 2, v_ny-1
        rp = rp + 2.d0*((vl_enth_prev(j) + vl_enth(j)) - 25.d-2*(vl_u_prev(j) + vl_u(j))**2.d0)
    enddo

    vr(v_n+1) = rp - ((2.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0)))*(vl_pres + vl_pres_prev)
    !---------------------------------------------------------------
    !               Gaussian Elimination (subroutine)
    !---------------------------------------------------------------
    do m = v_n+1,1,-1
        do i = m-1,1,-1
            vr(i) = vr(i)-( (va(i,m)*vr(m))/va(m,m) )
            do k = 1,v_n+1
                va(i,k) = va(i,k) - ( (va(i,m)*va(m,k))/va(m,m) )
            enddo
        enddo
    enddo
    !---------------------------------------------------------------
    !               Forward Substitution (subroutine)
    !---------------------------------------------------------------
    vx(1:v_n+1) = 0.d0

    vx(1) = vr(1)/va(1,1)

    do i = 2, v_n+1
        t = vr(i)
        do k = 1,i
            t = t - ( va(i,k)*vx(k) )
        enddo
        vx(i) = t/va(i,i)
    enddo
    !---------------------------------------------------------------
    !              Convergence Check & Uphat Update
    !---------------------------------------------------------------

    mc = 0
    do j = 1,v_ny
        vl_u_out(j) = vl_u(j) + vx(j+mc)
        mc = mc + 1
        vl_psi_out(j) = vl_psi(j) + vx(j+mc)
    enddo
    vl_pres_out = vl_pres + vx(v_n+1)

    err_out_max = dabs(vx(v_n+1))
    print *, err_out_max
    
    vl_u(1:v_ny) = vl_u_out(1:v_ny)
    vl_psi(1:v_ny) = vl_psi_out(1:v_ny)
    vl_pres = vl_pres_out

    if (err_out_max > 1.d-8) goto 1

    write(str, "('vl_ds_',i3.3,'.dat')"), e
    open(unit = v_fid, file=vlds//str, status='new', action='write')
    do j = 1,v_ny
        write(v_fid,*), vl_enth(j), vl_u(j), vl_psi(j), vl_pres, del
    enddo
    close(unit = v_fid)
    v_fid = v_fid + 2

	enddo

    deallocate(vl_enth_prev); deallocate(vl_u_prev); deallocate(vl_psi_prev)
    deallocate(vl_enth); deallocate(vl_u); deallocate(vl_psi)
    deallocate(vl_u_out); deallocate(vl_psi_out)
    deallocate(va); deallocate(vr); deallocate(vx)
    deallocate(eta); deallocate(il_u_prev); deallocate(il_v_prev)
    deallocate(il_rho_prev); deallocate(il_p_prev); deallocate(vl_u_out_prev)
    deallocate(vl_psi_out_prev)

end program