program khorrami_ds_adi
    implicit none

    double precision, allocatable :: vl_enth_prev(:),vl_u_prev(:),vl_psi_prev(:)
    double precision, allocatable :: il_v_prev(:),il_rho_prev(:),il_p_prev(:)
    double precision, allocatable :: vl_enth(:),vl_u(:),vl_psi(:)
    double precision, allocatable :: il_v(:),il_rho(:),il_p(:)
    double precision, allocatable :: va(:,:),ia(:,:),vr(:),ir(:),vx(:),ix(:)
    double precision, allocatable :: vl_u_out(:),vl_psi_out(:)
    double precision, allocatable :: il_v_out(:),il_rho_out(:),il_p_out(:)

    double precision :: v_dy,i_dy,dx,Pr,chi,gam,c,c1,c2,c3,c5,z,z_prev,del_vec(2),delt,del
    double precision :: del_prev,g_prev,vl_pres_prev,g,vl_pres,del1,del2,g1,g2,xi1
    double precision :: p1,p2,p3,psi1,psi2,psi3,u1,u2,u3,u4,u5,u7,v1,v2,v3,rho1,rho2,rho3
    double precision :: enth1,enth2,enth3,eta1,f1,f2,f3,t,lam,vl_pres_out,g_out,enth4
    double precision :: vl_err_out,il_err_out,pres_vec_min,del_c,rp,pres_vec(2)

    integer :: v_ny,v_n,i_ny,i_n,nx,v_fid,i_fid,e,j,d,match_count,vcount,mc,m,i,k,icount,pres_vec_loc,f

    character*40 :: str
    character(*), parameter :: vlip = 'c:\users\sampson\documents\github\res\research\vl_ip\' !'
    character(*), parameter :: ilip = 'c:\users\sampson\documents\github\res\research\il_ip\' !'
    character(*), parameter :: vlds = 'c:\users\sampson\documents\github\res\research\vl_ds\' !'
    character(*), parameter :: ilds = 'c:\users\sampson\documents\github\res\research\il_ds\' !'

    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    !+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   

    v_ny = 501; v_n = 2*v_ny; v_dy = 10.d0/dble(v_ny - 1)
    i_ny = 501; i_n = 3*i_ny; i_dy = 1.d0/dble(i_ny - 1)
    nx = 501; dx = 1.d0/(nx-1.d0)

    v_fid = 11; i_fid = 12

    Pr = 1.d0; chi = 15.d-1; gam = 14.d-1; c = 1.d0

    open(unit=9, file=vlip//"c3con_pr1adi.dat", status="old", action="read")
    open(unit=10, file=ilip//"c5con_khm_500.dat", status="old", action="read")
    read(9,*), c3
    read(10,*), c5
    close(unit=9)
    close(unit=10)

    allocate(vl_enth_prev(v_ny)); allocate(vl_u_prev(v_ny)); allocate(vl_psi_prev(v_ny))
    allocate(il_v_prev(i_ny)); allocate(il_rho_prev(i_ny)); allocate(il_p_prev(i_ny))
    allocate(vl_enth(v_ny)); allocate(vl_u(v_ny)); allocate(vl_psi(v_ny))
    allocate(il_v(i_ny)); allocate(il_rho(i_ny)); allocate(il_p(i_ny))
    allocate(va(v_n+1,v_n+1)); allocate(vr(v_n+1)); allocate(vx(v_n+1))
    allocate(ia(i_n+1,i_n+1)); allocate(ir(i_n+1)); allocate(ix(i_n+1))
    allocate(vl_u_out(v_ny)); allocate(vl_psi_out(v_ny))
    allocate(il_v_out(v_ny)); allocate(il_rho_out(i_ny)); allocate(il_p_out(i_ny))

    e = 0
1   e = e + 1

    z = dble(e)*dx; z_prev = dble(e-1)*dx

    if (e == 1) then
        !----------------Viscous Layer (IP)-----------------------
        open(unit=v_fid, file=vlip//'vl_pr1adi.dat', status='old', action='read')
        do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j)
        enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        !----------------Inviscid Layer (IP)----------------------
        open(unit=i_fid, file=ilip//"il_khm_500.dat", status="old", action="read")
        do j = 1,i_ny
            read(i_fid,*), il_v_prev(j), il_rho_prev(j), il_p_prev(j)
        enddo
        close(unit = i_fid)
        i_fid = i_fid + 2

        del_prev = 1.d0; g_prev = 1.d0; vl_pres_prev = 1.d0
        g = g_prev; vl_pres = vl_pres_prev

        c2 = ((c*c3)/il_p_prev(1))**(1.d0/4.d0)
        c1 = (c*c3)/(c2*c2)
    else
        !----------------Viscous Layer (DS)-----------------------
        write(str, "('vl_ds_',i3.3,'.dat')"), e-1
        open(unit=v_fid, file=vlds//str, status='old', action='read')
        do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j), vl_pres_prev
        enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        !----------------Inviscid Layer (DS)-----------------------
        write(str, "('il_ds_',i3.3,'.dat')"), e-1
        open(unit=i_fid, file=ilds//str, status='old', action='read')
        do j = 1,i_ny
            read(i_fid,*), il_v_prev(j), il_rho_prev(j), il_p_prev(j), del_prev, g_prev
        enddo
        close(unit=i_fid)
        i_fid = i_fid + 2
    endif

    !===============================================================
    !===============================================================
    !                       Matching Loop
    !===============================================================
    !===============================================================
    match_count = 0
    delt = del_prev
2   match_count = match_count + 1
    print *, "match count", match_count
    print *, ""
    del_vec(2) = delt*99.d-2
    del_vec(1) = delt*1.d0
    do d = 1,2
        del = del_vec(d)
        print *, "trial bl thickness", del

    !==============================================================!
    !                        Viscous Layer                         !
    !==============================================================!
    !                                                              !
    !                                                              !

    !---------------------------------------------------------------
    !                  Initial Guesses (Viscous)
    !---------------------------------------------------------------
    vl_enth = vl_enth_prev
    vl_u = vl_u_prev
    vl_psi = vl_psi_prev
    !---------------------------------------------------------------
    !                         Loop Start
    !---------------------------------------------------------------

    vcount = 0
3   vcount = vcount + 1
    print *, "vcount", vcount

    !---------------------------------------------------------------
    !                    Matrix Initialization
    !---------------------------------------------------------------

    va(1:v_n+1,1:v_n+1) = 0.d0; vr(1:v_n+1) = 0.d0

    va(1,1) = 1.d0; vr(1) = -vl_u_prev(1) - vl_u(1)
    va(2,2) = 1.d0; vr(2) = -vl_psi_prev(1) - vl_psi(1)
    
    mc = 1

    do j = 2, v_ny-1
        del1 = del + del_prev; xi1 = z + z_prev
        p1 = vl_pres + vl_pres_prev; p2 = vl_pres - vl_pres_prev
        psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j)
        psi3 = vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j)
        u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j)
        u4 = vl_u(j+1) - vl_u(j) + vl_u_prev(j+1) - vl_u_prev(j)
        u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1)
        u7 = vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1)&
            + vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1)
        enth1 = vl_enth(j) + vl_enth_prev(j);

    !Streamfunction elements
        va(j+mc,(j+mc)-2) = -v_dy*del1
        va(j+mc,(j+mc)-1) = -4.d0
        va(j+mc,(j+mc)) = -v_dy*del1
        va(j+mc,(j+mc)+1) = 4.d0
    !Streamfunction RHS
        vr(j+mc) = v_dy*del1*u4 - 4.d0*psi3

        mc = mc + 1

    !Momentum elements
        va(j+mc,(j+mc)-3) = v_dy*dx*del1*p1*((4.d0/dx)*xi1*psi2 + psi1) - 16.d0*dx*p1*p1
        va(j+mc,(j+mc)-1) = 4.d0*v_dy*v_dy*xi1*del1*del1*p1*(u1+u2)&
            + v_dy*v_dy*((gam-1.d0)/gam)*del1*del1*u1*(2.d0*dx*p1 - 4.d0*xi1*p2)+ 32.d0*dx*p1*p1
        va(j+mc,(j+mc)) = -v_dy*dx*del1*p1*u5*((4.d0/dx)*xi1 + 1.d0)
        va(j+mc,(j+mc)+1) = -v_dy*dx*del1*p1*((4.d0/dx)*xi1*psi2 + psi1) - 16.d0*dx*p1*p1
        va(j+mc,v_n+1) = 4.d0*v_dy*v_dy*xi1*del1*del1*u1*u2 - v_dy*dx*del1*u5*((4.d0/dx)*xi1*psi2 + psi1)&
            + v_dy*v_dy*((gam-1.d0)/gam)*del1*del1*(enth1 - 25.d-2*u1*u1)*(8.d0*xi1 - 4.d0*dx) - 32.d0*dx*p1*u7
    !Momentum RHS
        vr(j+mc) = -4.d0*v_dy*v_dy*xi1*del1*del1*u1*u2*p1 + v_dy*del1*p1*u5*(4.d0*xi1*psi2 + dx*psi1)&
            + 16.d0*dx*u7*p1*p1 + v_dy*v_dy*((gam-1.d0)/gam)*del1*del1&
            *(enth1 - 25.d-2*u1*u1)*(4.d0*dx*p1 - 8.d0*xi1*p2)

    enddo

    !---BL BCs
    va(v_n-1,v_n-1) = 1.d0; vr(v_n-1) = 2.d0 - vl_u_prev(v_ny) - vl_u(v_ny)

    va(v_n,v_n-4) = -v_dy*(del_prev + del)
    va(v_n,v_n-3) = -4.d0
    va(v_n,v_n-1) = -v_dy*(del_prev + del)
    va(v_n,v_n)   = 4.d0
    vr(v_n)   = v_dy*(del_prev + del)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
           - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))

    !Pressure Elements
    va(v_n+1,1) = 5.d-1*(vl_u(1) + vl_u_prev(1))
    mc = 1
    do j = 2,v_ny-1
        va(v_n+1,j+mc) = vl_u(j) + vl_u_prev(j)
        mc = mc + 1
    enddo
    va(v_n+1,v_n-1) = 5.d-1*(vl_u(v_ny) + vl_u_prev(v_ny))
    va(v_n+1,v_n+1) = (2.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0))

    !Pressure RHS
    rp = ( (vl_enth_prev(1) + vl_enth(1)) - 25.d-2*((vl_u_prev(1) + vl_u(1))**2.d0) )&
        + ( (vl_enth_prev(v_ny) + vl_enth(v_ny)) - 25.d-2*((vl_u_prev(v_ny) + vl_u(v_ny))**2.d0) )
    do j = 2, v_ny-1
        rp = rp + 2.d0*( (vl_enth_prev(j) + vl_enth(j)) - 25.d-2*((vl_u_prev(j) + vl_u(j))**2.d0) )
    enddo

    vr(v_n+1) = rp - ((2.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0)))*p1
    !---------------------------------------------------------------
    !               Gaussian Elimination (subroutine)
    !---------------------------------------------------------------
    do m = v_n+1,1,-1
        do i = m-1,1,-1
            vr(i) = vr(i)-( (va(i,m)*vr(m))/va(m,m) )
            do k = 1,v_n+1
                va(i,k) = va(i,k) - ( (va(i,m)*va(m,k))/va(m,m) )
            enddo
        enddo
    enddo
    !---------------------------------------------------------------
    !               Forward Substitution (subroutine)
    !---------------------------------------------------------------
    vx(1:v_n+1) = 0.d0

    vx(1) = vr(1)/va(1,1)

    do i = 2, v_n+1
        t = vr(i)
        do k = 1,i
            t = t - ( va(i,k)*vx(k) )
        enddo
        vx(i) = t/va(i,i)
    enddo

    !---------------------------------------------------------------
    !              Convergence Check & Uphat Update
    !---------------------------------------------------------------

    !if (e == 1) then
    !    lam = 1.d-1
    !else
    !    lam = 1.d0
    !endif

    mc = 0
    do j = 1,v_ny
        vl_u_out(j) = vl_u(j) + lam*vx(j+mc)
        mc = mc + 1
        vl_psi_out(j) = vl_psi(j) + lam*vx(j+mc)
    enddo
    vl_pres_out = vl_pres + lam*vx(v_n+1)

    vl_err_out = dabs(vx(v_n+1))
    print *, vl_pres_out, vl_err_out

    vl_u = vl_u_out; vl_psi = vl_psi_out; vl_pres = vl_pres_out
    
    if (vl_err_out > 1.d-3) goto 3

    !                                                              !
    !                            End VL                            !
    !==============================================================!

    !==============================================================!
    !                        Inviscid Layer                        !
    !==============================================================!
    !                                                              !
    !                                                              !

    !---------------------------------------------------------------
    !                  Initial Guesses (Inviscid)
    !---------------------------------------------------------------
    il_v = il_v_prev
    il_rho = il_rho_prev
    il_p = il_p_prev
    !---------------------------------------------------------------
    !                         Loop Start
    !---------------------------------------------------------------
    icount = 0
5   icount = icount + 1
    print *, "icount", icount

    !---------------------------------------------------------------
    !                       Matrix and RHS
    !---------------------------------------------------------------

    ia(1:i_n+1,1:i_n+1) = 0.d0; ir(1:i_n+1) = 0.d0

    ia(1,1) = 1.d0

    ir(1) = ((z+z_prev)/dx)*(del-del_prev) + 75.d-2*(del+del_prev) - il_v(1) - il_v_prev(1)

    mc = 0

    do j = 2, i_ny
        rho1 = il_rho(j)+il_rho(j-1)+il_rho_prev(j)+il_rho_prev(j-1)
        rho2 = il_rho(j)+il_rho(j-1)-il_rho_prev(j)-il_rho_prev(j-1)
        rho3 = il_rho(j)-il_rho(j-1)+il_rho_prev(j)-il_rho_prev(j-1)
        v1 = il_v(j)+il_v(j-1)+il_v_prev(j)+il_v_prev(j-1)
        v2 = il_v(j)+il_v(j-1)-il_v_prev(j)-il_v_prev(j-1)
        v3 = il_v(j)-il_v(j-1)+il_v_prev(j)-il_v_prev(j-1)
        p1 = il_p(j)+il_p(j-1)+il_p_prev(j)+il_p_prev(j-1)
        p2 = il_p(j)+il_p(j-1)-il_p_prev(j)-il_p_prev(j-1)
        p3 = il_p(j)-il_p(j-1)+il_p_prev(j)-il_p_prev(j-1)
        g1 = g+g_prev; g2 = g-g_prev
        del1 = del+del_prev; del2 = del-del_prev
        xi1 = z+z_prev; eta1 = ( dble(j) + dble(j-1) )*i_dy
        f1 = c5*g1 - del1
        f2 = del2*(eta1 - 2.d0) - eta1*c5*g2
        f3 = 3.d0*del1*(eta1-2.d0) - 3.d0*eta1*c5*g1 + 4.d0*v1

        !continuity
        ia(j+mc,(j+mc)-1) = 4.d0*dx*(rho3-rho1)
        ia(j+mc,j+mc)     = 4.d0*dx*xi1*f1 - 4.d0*xi1*f2 - dx*f3 + 4.d0*dx*v3
        ia(j+mc,(j+mc)+2) = 4.d0*dx*(rho3+rho1)
        ia(j+mc,(j+mc)+3) = 4.d0*dx*xi1*f1 + 4.d0*xi1*f2 + dx*f3 + 4.d0*dx*v3
        ia(j+mc,i_n+1)   = 4.d0*i_dy*xi1*c5*rho2 - 4.d0*xi1*eta1*c5*rho3 - 3.d0*dx*c5*rho3

        ir(j+mc) = -4.d0*i_dy*xi1*f1 - 4.d0*xi1*f2*rho3 - dx*f3*rho3 - 4.d0*dx*rho1*v3
 
        mc = mc + 1

        !momentum
        ia(j+mc,(j+mc)-2) = 4.d0*i_dy*rho1*xi1*f1 - 4.d0*rho1*xi1*f2 - rho1*dx*f3 + 4.d0*dx*rho1*v3 - i_dy*dx*f1*rho1 
        ia(j+mc,(j+mc)-1) = 4.d0*i_dy*v2*xi1*f1 + 4.d0*v3*xi1*f2 + v3*dx*f3 - i_dy*dx*f1*v1
        ia(j+mc,j+mc)     = -64.d0*dx
        ia(j+mc,(j+mc)+1) = 4.d0*i_dy*rho1*xi1*f1 + 4.d0*rho1*xi1*f2 + rho1*dx*f3 + 4.d0*dx*rho1*v3 - i_dy*dx*f1*rho1
        ia(j+mc,(j+mc)+2) = 4.d0*i_dy*v2*xi1*f1 + 4.d0*v3*xi1*f2 + v3*dx*f3 - i_dy*dx*f1*v1
        ia(j+mc,(j+mc)+3) = 64.d0*dx
        ia(j+mc,i_n+1)   = 4.d0*i_dy*xi1*c5*rho1*v2 - 4.d0*xi1*c5*eta1*rho1*v3&
                        - 3.d0*i_dy*eta1*c5*rho1*v3 - i_dy*dx*c5*rho1*v1

        ir(j+mc) = -4.d0*i_dy*xi1*rho1*v2*f1 - 4.d0*xi1*rho1*v3*f2 - dx*rho1*v3*f3&
                - 64.d0*dx*p3 + dx*i_dy*rho1*v1*f1

        mc = mc + 1

        !energy 
        ia(j+mc,(j+mc)-3) = 4.d0*dx*(rho1*p3 - gam*p1*rho3) 
        ia(j+mc,(j+mc)-2) = 4.d0*i_dy*xi1*f1*(p2-gam*p1) + 4.d0*xi1*f2*(p3+gam*p1)&
                        - dx*f3*(p3+gam*p1) - 2.d0*i_dy*dx*f1*p1
        ia(j+mc,(j+mc)-1) = 4.d0*i_dy*xi1*f1*(rho1-gam*rho2) - 4.d0*xi1*f2*(rho1+gam*rho3)&
                        - dx*f3*(rho1+gam*rho3) - 2.d0*i_dy*dx*f1*rho1
        ia(j+mc,j+mc)     = 4.d0*dx*(rho1*p3 - gam*p1*rho3)
        ia(j+mc,(j+mc)+1) = 4.d0*i_dy*xi1*f1*(p2-gam*p1) + 4.d0*xi1*f2*(p3-gam*p1)&
                        - dx*f3*(p3-gam*p1) - 2.d0*i_dy*dx*f1*p1
        ia(j+mc,(j+mc)+2) =  4.d0*i_dy*xi1*f1*(rho1-gam*rho2) + 4.d0*xi1*f2*(rho1-gam*rho3)&
                        + dx*f3*(rho1-gam*rho3) - 2.d0*i_dy*dx*f1*rho1
        ia(j+mc,i_n+1)   = 4.d0*dx*xi1*c5*(rho1*p2-gam*p1*rho2) - 4.d0*eta1*c5*(rho1*p3-gam*p1*rho3)&
                        - 3.d0*dx*eta1*c5*(rho1*p3-gam*p1*rho3) - 2.d0*dx*i_dy*f1*rho1*p1

        ir(j+mc) = -4.d0*i_dy*xi1*f1*(rho1*p2-gam*p1*rho2) - 4.d0*xi1*f2*(rho1*p3-gam*p1*rho3)&
                -dx*f3*(rho1*p3-gam*p1*rho3) + 2.d0*i_dy*dx*f1*rho1*p1

    enddo

    rho1 = il_rho(i_ny)+il_rho(i_ny-1)+il_rho_prev(i_ny)+il_rho_prev(i_ny-1)
    v1 = il_v(i_ny)+il_v(i_ny-1)+il_v_prev(i_ny)+il_v_prev(i_ny-1)
    p1 = il_p(i_ny)+il_p(i_ny-1)+il_p_prev(i_ny)+il_p_prev(i_ny-1)
    g1 = g+g_prev; g2 = g-g_prev
    xi1 = z+z_prev

    !Density
    ia(i_n-1,i_n-4) = ((75.d-2*g1 + (xi1/dx)*g2)**2.d0&
                + ((2.d0*(xi1**(1.d0/2.d0)))/((2.d0**(1.d0/2.d0))*(gam-1.d0)*chi*chi*chi*c2*c2*c5*c5)))/16.d0
    ia(i_n-1,i_n-1) = ((75.d-2*g1 + (xi1/dx)*g2)**2.d0&
                + ((2.d0*(xi1**(1.d0/2.d0)))/((2.d0**(1.d0/2.d0))*(gam-1.d0)*chi*chi*chi*c2*c2*c5*c5)))/16.d0
    ia(i_n-1,i_n+1) = 2.d0*(75.d-2*g1+(xi1/dx)*g2)*(75.d-2+(xi1/dx))*((rho1/16.d0)-25.d-2*((gam+1.d0)/(gam-1.d0)))

    ir(i_n-1) = (-rho1/16.d0)*((75.d-2*g1 + (xi1/dx)*g2)**2.d0&
            + ((2.d0*(xi1**(1.d0/2.d0)))/((2.d0**(1.d0/2.d0))*(gam-1.d0)*chi*chi*chi*c2*c2*c5*c5)))&
            + 25.d-2*((gam+1.d0)/(gam-1.d0))*(75.d-2*g1 + (xi1/dx)*g2)**2.d0

    !Pressure
    ia(i_n,i_n-3) = 1.d0
    ia(i_n,i_n) = 1.d0
    ia(i_n,i_n+1) = -(4.d0*c5*c5*(75.d-2*g1+(xi1/dx)*g2)*(75.d-2+(xi1/dx)))/(gam+1.d0)

    ir(i_n) = (4.d0/(gam+1.d0))*( ((c5*c5)/2.d0)*((75.d-2*g1 + (xi1/dx)*g2)**2.d0)&
            - ((gam-1.d0)/gam)*((xi1**(1.d0/2.d0))/((2.d0**(1.d0/2.d0))*chi*chi*chi*c2*c2)) ) - p1

    !Trans Velocity
    ia(i_n+1,i_n-5) = (75.d-2*g1+(xi1/dx)*g2)/8.d0
    ia(i_n+1,i_n-2) = (75.d-2*g1+(xi1/dx)*g2)/8.d0
    ia(i_n+1,i_n+1) = ((v1*(75.d-2+(xi1/dx)))/8.d0)&
                - ((2.d0*c5*(75.d-2*g1+(xi1/dx)*g2)*(75.d-2+(xi1/dx)))/(gam+1.d0))

    ir(i_n+1) = ((-v1*(75.d-2*g1 + (xi1/dx)*g2))/8.d0)&
            + (2.d0/(gam+1.d0))*((c5/2.d0)*((75.d-2*g1+(xi1/dx)*g2)**2.d0)&
            -((xi1**(1.d0/2.d0))/((2.d0**(1.d0/2.d0))*chi*chi*chi*c2*c2*c5)))

    !---------------------------------------------------------------
    !                    Gaussian Elimination
    !---------------------------------------------------------------
    do m = i_n+1,1,-1
        do i = m-1,1,-1
            ir(i) = ir(i)-( (ia(i,m)*ir(m))/ia(m,m) )
            do k = 1,i_n+1
                ia(i,k) = ia(i,k) - ( (ia(i,m)*ia(m,k))/ia(m,m) )
            enddo
        enddo
    enddo
    !---------------------------------------------------------------
    !                    Forward Substitution
    !---------------------------------------------------------------
    ix(1:i_n+1) = 0.d0

    ix(1) = ir(1)/ia(1,1)
    do i = 2, i_n+1
        t = ir(i)
        do k = 1,i
            t = t - ( ia(i,k)*ix(k) )
        enddo
        ix(i) = t/ia(i,i)
    enddo
    !---------------------------------------------------------------
    !              Convergence Check & Uphat Update
    !---------------------------------------------------------------
    if (e == 1) then
        lam = 1.d-1
    else
        lam = 1.d0
    endif

    mc = 0
    do j = 1,i_ny
        il_v_out(j) = il_v(j) + lam*ix(j+mc)
        mc = mc + 1
        il_rho_out(j) = il_rho(j) + lam*ix(j+mc)
        mc = mc + 1
        il_p_out(j) = il_p(j) + lam*ix(j+mc)
    enddo
    g_out = g + lam*ix(i_n+1)

    il_err_out = dabs(ix(v_n+1))
    print *, g_out, il_err_out

    il_v = il_v_out; il_rho = il_rho_out
    il_p = il_p_out; g = g_out

    if (il_err_out > 1.d-3) goto 5

    !                                                              !
    !                          End IL                              !
    !==============================================================!

!        print *, c1, vl_pres, c2, il_p(1), z**(1.d0/2.d0)
        pres_vec(d) = (c1*vl_pres - c2*c2*il_p(1))/(z**(1.d0/2.d0))
        print *, "pressure diff", pres_vec(d)
        print *, ""
        print *, ""

    enddo

    pres_vec_min = minval(dabs(pres_vec))
    pres_vec_loc = minloc(pres_vec,1)
!    print *, ""
!    print *, "matched pressure difference", pres_vec_min

    if (pres_vec_min > 1.d-4) then
!        print *, del_vec(1), pres_vec(2), "", del_vec(2), pres_vec(2)
!        print *, pres_vec(2)-pres_vec(1)
        del_c = (del_vec(1)*pres_vec(2) - del_vec(2)*pres_vec(1))&
            /(pres_vec(2) - pres_vec(1))
        print *, "better bl thickness", del_c
        print *, ""
        print *, ""
        delt = del_c

        goto 2
    endif

    del = del_vec(pres_vec_loc)

    ! !Viscous Layer
    ! write(str, "('vl_ds_',i3.3,'.dat')"), e
    ! open(unit = v_fid, file=vlds//str, status='new', action='write')
    ! do j = 1,v_ny
    !     write(v_fid,*), vl_enth(j), vl_u(j), vl_psi(j), vl_pres
    ! enddo
    ! close(unit = v_fid)
    ! v_fid = v_fid + 2
    ! !Inviscid Layer
    ! write(str, "('il_ds_',i3.3,'.dat')"), e
    ! open(unit=i_fid, file=ilds//str, status='new', action='write')
    ! do j = 1,i_ny
    !     write(i_fid,*), il_v(j), il_rho(j), il_p(j), del, g
    ! enddo
    ! close(unit=i_fid)
    ! i_fid = i_fid + 2

    if (e < 5) goto 1

end program