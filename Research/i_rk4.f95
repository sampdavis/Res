program rk4invip
    implicit none

    double precision :: gam,err_tol,tol_min,eta_b,eta_s,c5int,c5,dc5
    double precision :: ildeta,eta,ilis(4),u_rh,p_rh,v_rh,rho_rh,il_err_out(3)
    double precision :: err_min,err_min_out(9000),enth,temp,g,del
    double precision :: eta_pr,u_pr,v_pr,rho_pr,p_pr,enth_pr,temp_pr
    double precision :: eta_i,ildeta_i,rho_prev,eta_prev
    double precision :: enth_interp,temp_interp
    double precision :: u_prev,v_prev,enth_prev,temp_prev,p_prev
    integer :: iliter,m,ij,k_out(3),ix,dk,k,f,ni,i,done
    double precision, allocatable :: eta_interp(:),u_interp(:),v_interp(:)
    double precision, allocatable :: rho_interp(:),p_interp(:)

    gam = 14.d-1

    err_tol = 1.d-12
    tol_min = 100.d0

    eta_b = 0.d0
    eta_s = 1.d0

    iliter = 0
    ij = 0

    c5int = 15.d-1
    dc5 = 1.d-3

    k_out(1:3) = (/ -1, 0, 1 /)
!---------------------------------------------------------------
!---------------------------------------------------------------
1   iliter = iliter + 1

    m = 0

   	do k = k_out(1), k_out(3)

        m = m + 1

        c5 = c5int + (dc5*k)

        ildeta = 1.d-2
        eta = eta_s

        ilis(1) = -(9.d0*(c5*c5))/(8.d0*(gam + 1.d0))
        u_rh = ilis(1)
        ilis(4) = (9.d0*(c5*c5))/(8.d0*(gam + 1.d0))
        p_rh = ilis(4)
        ilis(2) = (3.d0*c5)/(2.d0*(gam + 1.d0))
        v_rh = ilis(2)
        ilis(3) = (gam + 1.d0)/(gam - 1.d0)
        rho_rh = ilis(3)

        ix = 0

2       ix = ix + 1

        call rk4(eta,ildeta,ilis,c5,gam,err_tol,tol_min)

        if (eta > eta_b) goto 2

        il_err_out(m) = dabs(0.d0 - ilis(3))

	enddo

    err_min = minval(il_err_out)
    dk = minloc(il_err_out,1)
    ij = ij + 1
    err_min_out(ij) = err_min

    c5int = c5int + (k_out(dk)*dc5)

    if (ij > 2) then
	    if (dabs(err_min_out(ij-1)-err_min_out(ij-2)) >= 5.d-3) then
            dc5 = 25.d-2*dc5
		endif
	endif

    if (err_min > 2.d-4) goto 1

!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
    c5 = c5int

    open(unit=99, file='c5con.dat', status='new', action='write')
    write(99,*), c5
    close(unit=99)

    ildeta = 1.d-2
    eta = eta_s

    ilis(1) = -(9.d0*(c5*c5))/(8.d0*(gam + 1.d0))
    u_rh = ilis(1)
    ilis(4) = (9.d0*(c5*c5))/(8.d0*(gam + 1.d0))
    p_rh = ilis(4)
    ilis(2) = (3.d0*c5)/(2.d0*(gam + 1.d0))
    v_rh = ilis(2)
    ilis(3) = (gam + 1.d0)/(gam - 1.d0)
    rho_rh = ilis(3)

    open(unit=1, file="il_flow.dat", status="new", action="write")
    enth = u_rh + 5.d-1*(v_rh*v_rh) + ((gam*p_rh)/((gam - 1.d0)*rho_rh))
    temp = p_rh/rho_rh
    del = 1.d0
    g = 1.d0
    write(1,*), eta_s, u_rh, v_rh, rho_rh, p_rh, del, g, enth, temp

    ix = 1

3   ix = ix + 1

    call rk4(eta,ildeta,ilis,c5,gam,err_tol,tol_min)

    enth = ilis(1) + 5.d-1*(ilis(2)**2.d0) + ((gam*ilis(4))/((gam - 1.d0)*ilis(3)))
    temp = ilis(4)/ilis(3)
    del = 1.d0
    g = 1.d0
    write(1,*), eta, ilis, del, g, enth, temp
    
	if (eta > eta_b) goto 3

    close(unit=1)

    open(unit=1, file="il_flow.dat", status="old", action="read")

    eta_i = 1.d0
    ni = 102
    ildeta_i = eta_i/dble(ni - 1)
    done = 0

    allocate(eta_interp(1:ni))
    allocate(u_interp(1:ni))
    allocate(v_interp(1:ni))
    allocate(rho_interp(1:ni))
    allocate(p_interp(1:ni))

4   read(1,*), eta_pr, u_pr, v_pr, rho_pr, p_pr, del, g, enth_pr, temp_pr

	if (eta_pr <= eta_i) then
        if (eta_pr == eta_i .or. eta_pr == 0.d0) then
            eta_interp(ni) = eta_pr
            u_interp(ni) = u_pr
            v_interp(ni) = v_pr
            rho_interp(ni) = rho_pr
            p_interp(ni) = p_pr
            if (done == 1) goto 5

	    else
            eta_prev = eta_pr
            u_prev = u_pr
            v_prev = v_pr
            rho_prev = rho_pr
            p_prev = p_pr
!            enth_prev = enth_pr
!            temp_prev = temp_pr

            backspace(unit=1)
            backspace(unit=1)

            read(1,*), eta_pr, u_pr, v_pr, rho_pr, p_pr, del, g, enth_pr, temp_pr
        
            eta_interp(ni) = eta_i
            u_interp(ni) = (eta_i - eta_pr)*((u_prev - u_pr)/(eta_prev - eta_pr)) + u_pr
            v_interp(ni) = (eta_i - eta_pr)*((v_prev - v_pr)/(eta_prev - eta_pr)) + v_pr
            rho_interp(ni) = (eta_i - eta_pr)*((rho_prev - rho_pr)/(eta_prev - eta_pr)) + rho_pr
            p_interp(ni) = (eta_i - eta_pr)*((p_prev - p_pr)/(eta_prev - eta_pr)) + p_pr
!            enth_interp = (eta_i - eta_pr)*((enth_prev - enth_pr)/(eta_prev - eta_pr)) + enth_pr
!            temp_interp = (eta_i - eta_pr)*((temp_prev - temp_pr)/(eta_prev - eta_pr)) + temp_pr

            backspace(unit=1)
            backspace(unit=1)

		endif

        eta_i = eta_i - ildeta_i
        ni = ni - 1
        if (eta_i < 0.d0) then
            eta_i = 0.d0
            done = 1
        endif
		goto 4

	else 
		goto 4
	endif

5   close(unit=1)

    open(unit=2, file="il_flow_interp.dat", status="new", action="write")
    ni = 102
	do i = 1,ni
        write(2,*), eta_interp(i), u_interp(i), v_interp(i), rho_interp(i), p_interp(i), del, g
        !, enth_interp, temp_interp
	enddo
    close(unit=2)

end program

subroutine rk4(eta,ildeta,ilis,c5,gam,err_tol,tol_min)
    implicit none

    double precision :: eta,ildeta,ilis(4),c5,gam,err_tol,tol_min,errf,eta_t
    double precision :: ilis1(4),ilis2(4),ilis3(4),ilis4(4),ilis5(4),il_err(4)
    double precision :: dilis1(4),dilis2(4),dilis3(4),dilis4(4),dilis5(4)
    integer :: q,i,f,rc

    rc = 0
    i = 0
1   i = i + 1
    eta_t = eta - ildeta
    if (eta_t < 0.d0) then
        ildeta = dabs(eta)
        eta = 0.d0 + ildeta
	endif
!-------------1---------------
    call ilderivs(eta, ilis, c5, gam, dilis1)
    do q=1,4
        ilis1(q) = ilis(q) - (1.d0/3.d0)*ildeta*dilis1(q)
    enddo
!-------------2---------------
    call ilderivs(eta - (1.d0/3.d0)*ildeta, ilis1, c5, gam, dilis2)
    do q=1,4
        ilis2(q) = ilis(q) - (1.d0/6.d0)*ildeta*(dilis1(q) + dilis2(q))
    enddo
!-------------3---------------
    call ilderivs(eta - (1.d0/3.d0)*ildeta, ilis2, c5, gam, dilis3)
    do q=1,4
        ilis3(q) = ilis(q) - (1.d0/8.d0)*ildeta*(dilis1(q) + 3.d0*dilis3(q))
    enddo
!-------------4---------------
    call ilderivs(eta - (1.d0/2.d0)*ildeta, ilis3, c5, gam, dilis4)
    do q=1,4
        ilis4(q) = ilis(q) - (1.d0/2.d0)*ildeta*(dilis1(q) - 3.d0*dilis3(q) + 4.d0*dilis4(q))
    enddo
!-------------5---------------
    call ilderivs(eta - ildeta, ilis4, c5, gam, dilis5)
    do q=1,4
        ilis5(q) = ilis(q) - (1.d0/6.d0)*ildeta*(dilis1(q) + 4.d0*dilis4(q) + dilis5(q))
    enddo
!------------err--------------
    do q=1,4
        il_err(q) = dabs((ilis5(q) - ilis4(q))/(5.d0*ilis4(q)))
    enddo
    errf = maxval(il_err)

	if (errf >= err_tol) then
        ildeta = ildeta/2.d0
        goto 1
	elseif (errf <= (err_tol/tol_min)) then
        eta = eta - ildeta
        ildeta = 2.d0*ildeta
        ilis(1) = ilis5(1)
        ilis(2) = ilis5(2)
        ilis(3) = ilis5(3)
        ilis(4) = ilis5(4)
		if (ildeta > 1.d-1) then
            ildeta = 1.d-1
		endif
	else
        eta = eta - ildeta
        ilis(1) = ilis5(1)
        ilis(2) = ilis5(2)
        ilis(3) = ilis5(3)
        ilis(4) = ilis5(4)
	endif

    return
end subroutine

subroutine ilderivs(eta,is,c5,gam,dis)
    implicit none

    double precision :: eta, is(4), c5, gam, vconst, dis(4)

    vconst = 4.d0*is(2) - 3.d0*(c5-1.d0)*eta - 3.d0

    ! is(1) == u, is(2) == v, is(3) == rho, is(4) == p

    dis(3) = ((c5-1.d0)*(4.d0*is(3)*is(2)*vconst - 32.d0*is(4))*is(3))/(vconst*(16.d0*gam*is(4) - is(3)*(vconst**2.d0)))
    dis(4) = ((c5-1.d0)/4.d0)*is(2)*is(3) + ((vconst**2.d0)/16.d0)*dis(3)
    dis(1) = ((2.d0*(c5-1.d0))/vconst)*(is(1) + (is(4)/is(3)) - (is(2)/8.d0)*(vconst - 4.d0*is(2)))&
           - ((vconst*dis(3)*(vconst-4.d0*is(2)))/(16.d0*is(3)))
    dis(2) = -(vconst/(4.d0*is(3)))*dis(3)

    return
end subroutine