program khorr_vds
    implicit none

    double precision, allocatable :: vl_enth_prev(:),vl_u_prev(:),vl_psi_prev(:),vl_enth_out_prev(:)
    double precision, allocatable :: vl_enth(:),vl_u(:),vl_psi(:),vl_enth_out(:),vl_psi_out_prev(:)
    double precision, allocatable :: vl_u_out(:),vl_psi_out(:),va(:,:),vr(:),vx(:),vl_u_out_prev(:)
    double precision, allocatable :: eta(:),il_u_prev(:),il_v_prev(:),il_rho_prev(:),il_p_prev(:)
    double precision :: Pr,gam,c,v_dy,i_dy,dx,c7,chi,z,z_prev,del_prev,g_prev,vl_pres_prev,lam,vl_pres_out
    double precision :: c1,c2,c3,c5,g,vl_pres,del,rp,t,err_out(3),err_out_max,err_out_max_old
    integer :: v_ny,v_n,i_ny,i_n,nx,v_fid,i_fid,e,j,vcount,mc,m,i,k,f,vcount_s
    character*25 :: str1,str2,str,str_step,str_count
    character(*), parameter :: vlip = 'c:\users\sampson\documents\github\res\research\vl_ip\' !'
    character(*), parameter :: ilip = 'c:\users\sampson\documents\github\res\research\il_ip\' !'
    character(*), parameter :: vlds = 'c:\users\sampson\documents\github\res\research\vl_ds\' !'

    Pr = 1.d0;
    !Pr = 725.d-3;
    
    gam = 14.d-1; c = 1.d0

    v_ny = 501; v_n = 3*v_ny; v_dy = 10.d0/dble(v_ny - 1)

    i_ny = 101; i_n = 3*i_ny; i_dy = 1.d0/dble(i_ny - 1)

    nx = 101; dx = 1.d0/dble(nx - 1)

    v_fid = 11; i_fid = 12; e = 0

    allocate(vl_enth_prev(1:v_ny)); allocate(vl_u_prev(1:v_ny)); allocate(vl_psi_prev(1:v_ny))
    allocate(vl_enth(1:v_ny)); allocate(vl_u(1:v_ny)); allocate(vl_psi(1:v_ny))
    allocate(vl_enth_out(1:v_ny)); allocate(vl_u_out(1:v_ny)); allocate(vl_psi_out(1:v_ny))
    allocate(va(1:v_n+1,1:v_n+1)); allocate(vr(1:v_n+1)); allocate(vx(1:v_n+1))
    allocate(eta(1:i_ny)); allocate(il_u_prev(1:i_ny)); allocate(il_v_prev(1:i_ny));
    allocate(il_rho_prev(1:i_ny)); allocate(il_p_prev(1:i_ny)); allocate(vl_u_out_prev(1:v_ny))
    allocate(vl_enth_out_prev(1:v_ny)); allocate(vl_psi_out_prev(1:v_ny));

    open(unit=9, file=vlip//"c3con_pr1adi.dat", status="old", action="read")
    open(unit=10, file=ilip//"c5con.dat", status="old", action="read")
    read(9,*), c3
    read(10,*), c5
    close(unit=9)
    close(unit=10)

    c7 = (gam - 1.d0)/gam
    chi = 10.d0

2   e = e + 1
    print *, "e", e
    z = dble(e)*dx
    z_prev = dble(e-1)*dx

    !----------------------------------------------------------
    !                Previous values
    !----------------------------------------------------------

	if (e == 1) then
        !----------------Viscous Layer (IP)-----------------------
        open(unit=v_fid, file=vlip//'vl_pr1adi.dat', status='old', action='read')
		do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j)
		enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        !----------------Inviscid Layer (IP)----------------------
        open(unit=i_fid, file=ilip//"il_flow_interpm1.dat", status="old", action="read")
		do j = 1,i_ny
            read(i_fid,*), eta(j), il_u_prev(j), il_v_prev(j), il_rho_prev(j), il_p_prev(j)!, xx1,xx2,xx3,xx4
		enddo
        close(unit = i_fid)
        i_fid = i_fid + 2

        del_prev = 1.d0; g_prev = 1.d0; vl_pres_prev = 1.d0

        c2 = ((c*c3)/il_p_prev(1))**(1.d0/4.d0)
        c1 = (c*c3)/(c2*c2)
        print *, c1, c2
        read *, f
	else
        !----------------Viscous Layer (DS)-----------------------
        !write(str, "('vl_ds_',i3.3,'_',i2.2,'.dat')"), e-1, vcount_s
        write(str, "('vl_ds_',i3.3,'.dat')"), e-1
        open(unit=v_fid, file=vlds//str, status='old', action='read')
		do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j), vl_pres_prev, del_prev
		enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
	endif

    g = g_prev; vl_pres = vl_pres_prev; del = del_prev
    err_out_max_old = 1.d2

    !===============================================================
    !                        Viscous Layer
    !===============================================================

    !---------------------------------------------------------------
    !                  Initial Guesses (Viscous)
    !---------------------------------------------------------------

    vl_enth(1:v_ny) = vl_enth_prev(1:v_ny)
    vl_u(1:v_ny) = vl_u_prev(1:v_ny)
    vl_psi(1:v_ny) = vl_psi_prev(1:v_ny)
    !---------------------------------------------------------------
    !                         Loop Start
    !---------------------------------------------------------------

    vcount = 0
1   vcount = vcount + 1
    print *, "vcount", vcount

    !---------------------------------------------------------------
    !                    Matrix Initialization
    !---------------------------------------------------------------
    va(1:v_n+1,1:v_n+1) = 0.d0

    va(1,1) = 1.d0
    !va(1,4) = -1.d0                         !Adiabatic
    va(2,2) = 1.d0
    va(3,3) = 1.d0

    mc = 2

    do j = 2, v_ny-1

    !Streamfunction elements
        va(j+mc,(j+mc)-2) = -v_dy*(del_prev + del)
        va(j+mc,(j+mc)-1) = -4.d0
        va(j+mc,(j+mc)+1) = -v_dy*(del_prev + del)
        va(j+mc,(j+mc)+2) = 4.d0

        mc = mc + 1

    !Momentum elements
        va(j+mc,(j+mc)-3) =  v_dy*dx*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) + vl_psi_prev(j))&
            + 4.d0*v_dy*(z + z_prev)*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) - vl_psi_prev(j))&
            - 16.d0*dx*((vl_pres + vl_pres_prev)**2.d0)

        va(j+mc,(j+mc)-1)   = c7*8.d0*v_dy*v_dy*((del + del_prev)**2.d0)*(z + z_prev)*(vl_pres - vl_pres_prev)&
            - c7*4.d0*dx*v_dy*v_dy*((del + del_prev)**2.d0)*(vl_pres + vl_pres_prev)

        va(j+mc,j+mc) = 4.d0*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)*(vl_pres + vl_pres_prev)&
            *((vl_u(j) + vl_u_prev(j)) + (vl_u(j) - vl_u_prev(j)))&
            - c7*4.d0*v_dy*v_dy*((del_prev + del)**2.d0)*(z_prev + z)*(vl_u_prev(j) + vl_u(j))&
            *(vl_pres - vl_pres_prev) + c7*2.d0*dx*v_dy*v_dy*((del_prev + del)**2.d0)*(vl_u_prev(j) + vl_u(j))&
            *(vl_pres_prev + vl_pres) + 32.d0*dx*((vl_pres_prev + vl_pres)**2.d0)

        va(j+mc,(j+mc)+1) = -v_dy*dx*(del + del_prev)*(vl_pres + vl_pres_prev)&
            *(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))&
            - 4.d0*v_dy*(z + z_prev)*(del + del_prev)*(vl_pres + vl_pres_prev)&
            *(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))

        va(j+mc,(j+mc)+3) = -v_dy*dx*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) + vl_psi_prev(j))&
            - 4.d0*v_dy*(z + z_prev)*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) - vl_psi_prev(j))&
            - 16.d0*dx*((vl_pres + vl_pres_prev)**2.d0)

        va(j+mc,v_n+1)  = c7*8.d0*v_dy*v_dy*((del + del_prev)**2.d0)*(z + z_prev)*(vl_enth(j) + vl_enth_prev(j))&
            - c7*2.d0*v_dy*v_dy*((del + del_prev)**2.d0)*(z + z_prev)*((vl_u(j) + vl_u_prev(j))**2.d0)&
            - c7*4.d0*dx*v_dy*v_dy*((del + del_prev)**2.d0)*(vl_enth(j) + vl_enth_prev(j))&
            + c7*dx*v_dy*v_dy*((del + del_prev)**2.d0)*((vl_u(j) + vl_u_prev(j))**2.d0)&
            + 4.d0*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)*(vl_u(j) + vl_u_prev(j))*(vl_u(j) - vl_u_prev(j))&
            - v_dy*dx*(del + del_prev)*(vl_psi(j) + vl_psi_prev(j))*(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))&
            - 4.d0*v_dy*(z + z_prev)*(del + del_prev)*(vl_psi(j) - vl_psi_prev(j))&
            *(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))&
            - 32.d0*dx*(vl_pres + vl_pres_prev)&
            *(vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1) + vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1))

        mc = mc + 1
   
    !Energy elements
        va(j+mc,(j+mc)-5) = Pr*dx*v_dy*(del + del_prev)*(vl_psi(j) + vl_psi_prev(j))&
            + 4.d0*Pr*v_dy*(z + z_prev)*(del + del_prev)*(vl_psi(j) - vl_psi_prev(j))&
            - 16.d0*dx*(vl_pres + vl_pres_prev)

        va(j+mc,(j+mc)-4) = 4.d0*dx*(Pr-1.d0)*(vl_pres + vl_pres_prev)&
            *(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))&
            - 8.d0*dx*(Pr-1.d0)*(vl_pres + vl_pres_prev)*(vl_u(j) + vl_u_prev(j))

        va(j+mc,(j+mc)-2) = 4.d0*Pr*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)*(vl_u(j) + vl_u_prev(j))&
            + 32.d0*dx*(vl_pres + vl_pres_prev)

        va(j+mc,(j+mc)-1) = 4.d0*Pr*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)*(vl_enth(j) - vl_enth_prev(j))&
            - 8.d0*dx*(Pr-1.d0)*(vl_pres + vl_pres_prev)&
            *((vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1) + vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1))&
            - 2.d0*(vl_u(j) + vl_u_prev(j)))

        va(j+mc,j+mc) = -Pr*dx*v_dy*(del + del_prev)&
            *(vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1))&
            - 4.d0*Pr*v_dy*(z + z_prev)*(del + del_prev)&
            *(vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1))

        va(j+mc,(j+mc)+1) = -Pr*dx*v_dy*(del + del_prev)*(vl_psi(j) + vl_psi_prev(j))&
            - 4.d0*Pr*v_dy*(z + z_prev)*(del + del_prev)*(vl_psi(j) - vl_psi_prev(j))&
            - 16.d0*dx*(vl_pres + vl_pres_prev)

        va(j+mc,(j+mc)+2) = -8.d0*dx*(Pr-1.d0)*(vl_pres_prev + vl_pres)*(vl_u_prev(j) + vl_u(j))&
            - 4.d0*dx*(Pr-1.d0)*(vl_pres_prev + vl_pres)&
            *(vl_u_prev(j+1) - vl_u_prev(j-1) + vl_u(j+1) - vl_u(j-1))

        va(j+mc,v_n+1)  = -16.d0*dx*(vl_enth(j+1) - 2.d0*vl_enth(j) + vl_enth(j-1)&
            + vl_enth_prev(j+1) - 2.d0*vl_enth_prev(j) + vl_enth_prev(j-1))&
            - 8.d0*dx*(Pr-1.d0)*(vl_u_prev(j) + vl_u(j))&
            *(vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1) + vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1))&
            - 2.d0*dx*(Pr-1.d0)*((vl_u_prev(j+1) - vl_u_prev(j-1) + vl_u(j+1) - vl_u(j-1))**2.d0)

    enddo

    !---BL BCs
    va(v_n-2,v_n-2) = 1.d0
    va(v_n-1,v_n-1) = 1.d0

    va(v_n,v_n-4) = -v_dy*(del_prev + del)
    va(v_n,v_n-3) = -4.d0
    va(v_n,v_n-1) = -v_dy*(del_prev + del)
    va(v_n,v_n)   = 4.d0

    va(v_n+1,1) = 2.d0
    va(v_n+1,2) = -(vl_u_prev(1) + vl_u(1))

    mc = 2
    do j = 2,v_ny-1
        va(v_n+1,j+mc) = 4.d0
        mc = mc + 1
        va(v_n+1,j+mc) = -2.d0*(vl_u_prev(j) + vl_u(j))
        mc = mc + 1
    enddo

    va(v_n+1,v_n-2) = 2.d0
    va(v_n+1,v_n-1) = -(vl_u_prev(v_ny) + vl_u(v_ny))
    va(v_n+1,v_n+1) = -(4.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0))

    !---------------------------------------------------------------
    !                      Right Hand Side
    !---------------------------------------------------------------
    vr(1:v_n+1) = 0.d0

    vr(1) = 2.d0*1.d-2 - vl_enth_prev(1) - vl_enth(1)                        !Non-adiabatic
    !vr(1) = vl_enth_prev(2) - vl_enth_prev(1) + vl_enth(2) - vl_enth(1)     !Adiabatic
    vr(2) = -vl_u_prev(1) - vl_u(1)
    vr(3) = -vl_psi_prev(1) - vl_psi(1)

    mc = 2
    do j = 2,v_ny-1

    !----Streamfunction RHS
        vr(j+mc) = v_dy*(del + del_prev)*(vl_u(j+1) + vl_u(j) + vl_u_prev(j+1) + vl_u_prev(j))&
                 - 4.d0*(vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j))

        mc = mc + 1

    !----Momentum RHS
        vr(j+mc) = dx*v_dy*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) + vl_psi_prev(j))&
            *(vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1))&
            - 4.d0*v_dy*(z + z_prev)*(del + del_prev)*(vl_pres + vl_pres_prev)*(vl_psi(j) - vl_psi_prev(j))&
            *(vl_u_prev(j+1) - vl_u_prev(j-1) + vl_u(j+1) - vl_u(j-1))&
            + 16.d0*dx*((vl_pres_prev + vl_pres)**2.d0)&
            *(vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1) + vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1))&
            -4.d0*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)*(vl_pres + vl_pres_prev)&
            *(vl_u(j) + vl_u_prev(j))*(vl_u(j) - vl_u_prev(j))&
            - c7*8.d0*v_dy*v_dy*((del + del_prev)**2.d0)*(z + z_prev)*(vl_enth(j) + vl_enth_prev(j))*(vl_pres - vl_pres_prev)&
            + c7*2.d0*v_dy*v_dy*((del + del_prev)**2.d0)*(z + z_prev)*((vl_u(j) + vl_u_prev(j))**2.d0)*(vl_pres - vl_pres_prev)&
            + c7*4.d0*dx*v_dy*v_dy*((del + del_prev)**2.d0)*(vl_enth(j) + vl_enth_prev(j))*(vl_pres + vl_pres_prev)&
            - c7*dx*v_dy*v_dy*((del + del_prev)**2.d0)*(vl_pres + vl_pres_prev)*((vl_u(j) + vl_u_prev(j))**2.d0)

        mc = mc + 1

    !----Energy RHS
        vr(j+mc) = Pr*dx*v_dy*(del + del_prev)*(vl_psi(j) + vl_psi_prev(j))&
            *(vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1))&
            + 4.d0*Pr*v_dy*(z + z_prev)*(del + del_prev)*(vl_psi(j) - vl_psi_prev(j))&
            *(vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1))&
            - 4.d0*Pr*v_dy*v_dy*(z + z_prev)*((del + del_prev)**2.d0)&
            *(vl_u(j) + vl_u_prev(j))*(vl_enth(j) - vl_enth_prev(j))&
            + 8.d0*dx*(Pr-1.d0)*(vl_pres_prev + vl_pres)*(vl_u_prev(j) + vl_u(j))&
            *(vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1) + vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1))&
            + 2.d0*dx*(Pr-1.d0)*(vl_pres_prev + vl_pres)*((vl_u_prev(j+1) - vl_u_prev(j-1) + vl_u(j+1) - vl_u(j-1))**2.d0)&
            + 16.d0*dx*(vl_pres_prev + vl_pres)&
            *(vl_enth_prev(j+1) - 2.d0*vl_enth_prev(j) + vl_enth_prev(j-1) + vl_enth(j+1) - 2.d0*vl_enth(j) + vl_enth(j-1))

    enddo

    !---BL BCs
    vr(v_n-2) = 1.d0 - vl_enth_prev(v_ny) - vl_enth(v_ny)
    vr(v_n-1) = 2.d0 - vl_u_prev(v_ny) - vl_u(v_ny)
    vr(v_n)   = v_dy*(del_prev + del)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
           - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))

    rp = 5.d-1*(4.d0*(vl_enth_prev(1) + vl_enth(1)) - (vl_u_prev(1) + vl_u(1))**2.d0)&
       + 5.d-1*(4.d0*(vl_enth_prev(v_ny) + vl_enth(v_ny)) - (vl_u_prev(v_ny) + vl_u_prev(v_ny))**2.d0)
    do j = 2, v_ny-1
        rp = rp + (4.d0*(vl_enth_prev(j) + vl_enth(j)) - (vl_u_prev(j) + vl_u(j))**2.d0)
    enddo

    vr(v_n+1) = ((4.d0*((gam*c3)**(1.d0/2.d0))*(vl_pres_prev + vl_pres))/(v_dy*(gam - 1.d0))) - rp

    !---------------------------------------------------------------
    !               Gaussian Elimination (subroutine)
    !---------------------------------------------------------------
    do m = v_n+1,1,-1
        do i = m-1,1,-1
            vr(i) = vr(i)-( (va(i,m)*vr(m))/va(m,m) )
            do k = 1,v_n+1
                va(i,k) = va(i,k) - ( (va(i,m)*va(m,k))/va(m,m) )
            enddo
        enddo
    enddo

    !---------------------------------------------------------------
    !               Forward Substitution (subroutine)
    !---------------------------------------------------------------
    vx(1:v_n+1) = 0.d0

    vx(1) = vr(1)/va(1,1)

    do i = 2, v_n+1
        t = vr(i)
        do k = 1,i
            t = t - ( va(i,k)*vx(k) )
        enddo
        vx(i) = t/va(i,i)
    enddo
 !print *, "6"
    !---------------------------------------------------------------
    !              Convergence Check & Uphat Update
    !---------------------------------------------------------------

    lam = 2.d0/(1.d0 + (5.d0**(5.d-1)))

3   mc = 0
    do j = 1,v_ny
        vl_enth_out(j) = vl_enth(j) + lam*vx(j+mc)
        mc = mc + 1
        vl_u_out(j) = vl_u(j) + lam*vx(j+mc)
        mc = mc + 1
        vl_psi_out(j) = vl_psi(j) + lam*vx(j+mc)
    enddo
    vl_pres_out = vl_pres + lam*vx(v_n+1)

	if (vcount == 1) then
        vl_enth_out_prev(1:v_ny) = vl_enth_prev(1:v_ny)
        vl_u_out_prev(1:v_ny) = vl_u_prev(1:v_ny)
        vl_psi_out_prev(1:v_ny) = vl_psi_prev(1:v_ny)

        !vl_enth(1:v_ny) = vl_enth_out(1:v_ny)
        !vl_u(1:v_ny) = vl_u_out(1:v_ny)
        !vl_psi(1:v_ny) = vl_psi_out(1:v_ny)
        !vl_pres = vl_pres_out
        !vl_pres = 1.d0

        !goto 1
	endif

    err_out(1) = maxval(dabs(vl_enth_out(1:v_ny) - vl_enth_out_prev(1:v_ny)))
    err_out(2) = maxval(dabs(vl_u_out(1:v_ny) - vl_u_out_prev(1:v_ny)))
    err_out(3) = maxval(dabs(vl_psi_out(1:v_ny) - vl_psi_out_prev(1:v_ny)))
    err_out_max = maxval(err_out)

    print *, '','under relaxation lambda', lam
    print *, '','previous error', err_out_max_old
    print *, '','current error', err_out_max
    print *, ''
    
	if (err_out_max > 1.d-3) then
		if (err_out_max < err_out_max_old) then
            vl_enth_out_prev(1:v_ny) = vl_enth(1:v_ny)
            vl_u_out_prev(1:v_ny) = vl_u(1:v_ny)
            vl_psi_out_prev(1:v_ny) = vl_psi(1:v_ny)

            vl_enth(1:v_ny) = vl_enth_out(1:v_ny)
            vl_u(1:v_ny) = vl_u_out(1:v_ny)
            vl_psi(1:v_ny) = vl_psi_out(1:v_ny)
            vl_pres = vl_pres_out
            vl_pres = 1.d0

            err_out_max_old = err_out_max

			goto 1
		else
            lam = lam*(2.d0/(1.d0 + (5.d0**(5.d-1))))

			if (lam < 1.d-16) then
                vl_enth(1:v_ny) = vl_enth_out_prev(1:v_ny)
                vl_u(1:v_ny) = vl_u_out_prev(1:v_ny)
                vl_psi(1:v_ny) = vl_psi_out_prev(1:v_ny)
			else
				goto 3
			endif
		endif
	endif

    !write(str, "('vl_ds_',i3.3,'_',i2.2,'.dat')"), e, vcount
    write(str, "('vl_ds_',i3.3,'.dat')"), e
    open(unit = v_fid, file=vlds//str, status='new', action='write')
    do j = 1,v_ny
        write(v_fid,*), vl_enth(j), vl_u(j), vl_psi(j), vl_pres, del
    enddo
    close(unit = v_fid)
    v_fid = v_fid + 2
    vcount_s = vcount

	if (e <= nx-1) goto 2

    deallocate(vl_enth_prev); deallocate(vl_u_prev); deallocate(vl_psi_prev)
    deallocate(vl_enth); deallocate(vl_u); deallocate(vl_psi)
    deallocate(vl_enth_out); deallocate(vl_u_out); deallocate(vl_psi_out)
    deallocate(va); deallocate(vr); deallocate(vx)
    deallocate(eta); deallocate(il_u_prev); deallocate(il_v_prev);
    deallocate(il_rho_prev); deallocate(il_p_prev); deallocate(vl_u_out_prev)
    deallocate(vl_enth_out_prev); deallocate(vl_psi_out_prev);

end program