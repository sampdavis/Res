function [wall_geo_y] = defor_geo(pts,s_p,e_p,h,con)

dp = 1/(pts-1);

wall_geo_y = zeros(1,pts);

a = e_p - s_p; 

radi = (1/(2*h))*(0.25*a*a + h*h);
ry = radi-h;
rx = a/2 + s_p;

xr = radi*sind(-180:0.001:180) + rx;
yr = radi*cosd(-180:0.001:180) - ry;
if con ~= 1
    yr = -yr;
end

di = 1; fr = 1;
for iy = 1:length(yr)
    if con == 1
        if yr(iy) > 0
            if fr == 1
                iyf = iy;
                fr = 0;
            end
            di = di + 1;
            def_vec_y(di) = yr(iy);
            def_vec_x(di) = xr(iy);
        end
    else
        if yr(iy) < 0
            if fr == 1
                iyf = iy;
                fr = 0;
            end
            di = di + 1;
            def_vec_y(di) = yr(iy);
            def_vec_x(di) = xr(iy);
        end
    end
end
def_vec_y(1) = yr(iyf-1); def_vec_x(1) = xr(iyf-1);
def_vec_y(end+1) = yr(iyf+(di-1)); def_vec_x(end+1) = yr(iyf+(di-1));

def_vec_interp = interp1(def_vec_x,def_vec_y,s_p:dp:e_p,'spline');
def_vec_interp(1) = 0; def_vec_interp(end) = 0;

s_p_ind = round(s_p*(pts-1) + 1);
e_p_ind = round(e_p*(pts-1) + 1);

wall_geo_y(s_p_ind:e_p_ind) = def_vec_interp;

%figure
%plot(wall_geo_x,wall_geo_y)
%grid on
%axis([0 1 -0.01 0.1])
end