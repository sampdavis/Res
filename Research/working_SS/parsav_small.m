function parsav_small(vl_pres,vl_del,vl_u_save,vl_psi_save,chi,s_p1,e_p1)
%%%
ttg = sprintf('_%d_%d_%d',chi,s_p1,e_p1);
save(strcat('u_bar',ttg,'.mat'),'vl_u_save')
save(strcat('sf_bar',ttg,'.mat'),'vl_psi_save')
save(strcat('p_bar',ttg,'.mat'),'vl_pres')
save(strcat('bl_bar',ttg,'.mat'),'vl_del')

end