#!/bin/bash
# Sample script that creates an sbatch script for running a STARCCM+ job and also submits the job to SLURM.
# Instructions:
#   1.  Modify the items under "job parameters" below. Save the file. 
#   2.  Make this file executable (e.g. chmod +x starccm.sh)
#   3.  Run this file  (.e.g   ./starccm.sh ) 

# job parameters: (modify as needed)
JOBNAME=janis_joblin
WORKDIR=/work/thomasw-free/sampson
SIMFILE=vdefds_adi_tw.f95
PARTITION=freelong
NUM_NODES=1
PROCS_PER_NODE=12
MAX_TIME=7-00:00:00
EMAIL=sampson@iastate.edu
MAIL_TYPES=BEGIN,FAIL,END
# end of job parameters.   

TOTAL_PROCS=$((NUM_NODES*PROCS_PER_NODE))
ERROR_FILE=${JOBNAME}.%j.error
OUTPUT_FILE=${JOBNAME}.%j.output

# Everything below until END_OF_SCRIPT gets passed to sbatch.  Edit carefully.
# Note that the regular shell variables (i.e.  $var,  ${var} ) are 
# filled in by bash when you run this script.
# The escaped variables (i.e.  \$var ) are filled in by SLURM at run time.
cat <<END_OF_SCRIPT > ${JOBNAME}.sbatch
#!/bin/bash
#SBATCH -J $JOBNAME
#SBATCH -D $WORKDIR
#SBATCH -N $NUM_NODES
#SBATCH -n $TOTAL_PROCS
#SBATCH --partition=$PARTITION
#SBATCH --ntasks-per-node=$PROCS_PER_NODE

#SBATCH --time=$MAX_TIME
#SBATCH --error=$ERROR_FILE
#SBATCH --output=$OUTPUT_FILE
#SBATCH --mail-type=$MAIL_TYPES
#SBATCH --mail-user=$EMAIL

gfortran -I/work/thomasw-free/sampson/include -L/work/thomasw-free/sampson/lib -lumfpack /work/thomasw-free/sampson/lib/* /work/thomasw-free/sampson/umf4_f77wrapper.c /work/thomasw-free/sampson/vdefds_adi_tw.f95 -lsuitesparseconfig -lcholmod -lmetis
export LD_LIBRARY_PATH=/work/thomasw-free/sampson/lib
./a.out

END_OF_SCRIPT

# Now send the sbatch script created above to sbatch..
echo "running: sbatch ./${JOBNAME}.sbatch"
sbatch ./${JOBNAME}.sbatch

