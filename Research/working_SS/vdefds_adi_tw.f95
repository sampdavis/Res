program vds_adi_tanwedge_deformed
    
    implicit none
    
    double precision :: pr,gam,c,v_dy,dx,vl_ip_enth(501),vl_ip_u(501),vl_ip_psi(501),x1,x2,il_p_wall
    double precision :: c1,c2,c3,chi,vl_del(1001),vl_pres_prev(1001),ramp_func(1001),rf_b(1001)
    double precision :: vl_u_save(501,1001),vl_psi_save(501,1001),vl_pres(1001),z,z_prev,pres_con,pres_res
    double precision :: vl_enth_prev(501),vl_enth(501),vl_u_prev(501),vl_u(501),vl_psi_prev(501),vl_psi(501)
    double precision :: xi1,p1,p2,pp1,del1,ramf1,va(1003,1003),vr(1003),vx(1003)
    double precision :: enth1,psi1,psi2,psi3,u1,u2,u4,u5,u7,rp,tg,vl_u_out(501),vl_psi_out(501),vl_pres_out
    integer :: v_ny,v_n,nx,v_fid,sweep,i,vcount,j,mc,mg,ig,kg,f,si,ci,new_col,iii,jjj
    character*8 :: cf
    logical :: ex
    
    double precision :: control(20),info(90),sprs_val(4999)
    integer*8 :: symbolic,numeric
    integer*4 :: sys,sprs_cc(1004),sprs_rind(4999)
    
    character*10 :: time
    character*8 :: date
    character*5 :: zone
    integer*8 :: values(8),starttime(5),currenttime(5),elapsetime(5)

    double precision :: dx1,dx2,nvec1(47),nvec2(51),nvec(98),nxf,vl_pres_interp(1001),def_points(2,6),s_p,e_p
    integer :: nx1,nx2,ii,done,incres,dp
    character*20 :: spf,epf
    
    !-----------------HERE WE GOOOOOOO----------------------
    call date_and_time(date,time,zone,values)
    starttime = (/values(3), values(5), values(6), values(7), values(8)/)
    
    pr = 1.d0; gam = 14.d-1; c = 1.d0
    v_ny = 501; v_n = 2*v_ny; v_dy = 10.d0/dble(v_ny-1)
    nx1 = 51; dx1 = 1.d0/dble(nx1-1)
    nx2 = 1001; dx2 = 1.d0/dble(nx2-1)

    !-------------------The Coarsening-----------------------
    do i = 1,47
        nvec1(i) = dble(i-1)*dx1
    enddo
    do i = 951,1001
        f = i-950
        nvec2(f) = dble(i-1)*dx2
    enddo
    nvec = (/nvec1,nvec2/)
    nx = 98
    
    !-------------------Viscous Layer (IP)-------------------
    v_fid = 11;
    open(unit=v_fid, file='init_dataz/vl_pr1adi.dat', status='old', action='read')
    do j = 1,v_ny
        read(v_fid,*), vl_ip_enth(j), vl_ip_u(j), vl_ip_psi(j)
    enddo
    close(unit=v_fid)
    v_fid = v_fid + 2
    !-------------------Inviscid Layer (IP)------------------
    open(unit=12, file="init_dataz/il_khm_500.dat", status="old", action="read")
    read(12,*), x1, x2, il_p_wall
    close(unit = 12)
    
    !-------------------C1, C2, C3 constants------------------
    open(unit=9, file="init_dataz/c3con_pr1adi.dat", status="old", action="read")
    read(9,*), c3
    close(unit=9)

    c2 = ((c*c3)/il_p_wall)**(25.d-2)
    c1 = (c*c3)/(c2*c2)
    
    !-----------------------Chi and deformations-----------------------
    chi = 1.d0
    def_points(1,:) = (/1.d0, 2.d0, 3.d0, 4.d0, 5.d0, 6.d0/)
    def_points(2,:) = (/4.d0, 5.d0, 6.d0, 7.d0, 8.d0, 9.d0/)
    
    do dp = 1,6
    
        s_p = def_points(1,dp)
        e_p = def_points(2,dp)
    
    !-----------------Initialized BL & Pres------------------
    vl_del = 0.d0; vl_del(1) = 1.d0
    vl_pres_prev = 0.d0
    vl_pres_prev(1:nx-1) = 1.d0; vl_pres_prev(nx) = 1.d0/(c1*gam*chi*chi*chi)

    !------------------Deformation shaping-------------------
    write(spf,'(I1.1)') int(s_p)
    write(epf,'(I1.1)') int(e_p)
    open(unit=14, file='init_dataz/coarse_ramp'//trim(spf)//trim(epf)//'_xl.csv', status='old', action='read')
    read(14,*), ramp_func(1:98)
    close(unit=14)
    
    rf_b = 0.d0
    do i = 1,nx
        if (i == 1) then
            rf_b(i) = ramp_func(i)
        else
            rf_b(i) = ramp_func(i)/( c2*(nvec(i)**(75.d-2)) )
        endif
    enddo

    !----------------Saved Layer Profiles--------------------
    vl_u_save(1:v_ny,1:nx2) = 0.d0; vl_u_save(1:v_ny,1) = vl_ip_u
    vl_psi_save(1:v_ny,1:nx2) = 0.d0; vl_psi_save(1:v_ny,1) = vl_ip_psi
    
    !----------------Oh boy! Here I go looping!--------------
    pres_con = 100; sweep = 0; incres = 0
    
10  sweep = sweep + 1

    vl_pres = vl_pres_prev

    !------------Make it fiiiiiiine-------------------------
    if (sweep == 1) then !(pres_con < 1.d-2 .and. incres == 0) then

        incres = 1

        nx = 1001; dx = 1.d0/dble(nx-1)

        vl_del = 0.d0; vl_del(1) = 1.d0
        
        write(spf,'(I1.1)') int(s_p)
        write(epf,'(I1.1)') int(e_p)
        open(unit=16, file='init_dataz/ramp'//trim(spf)//trim(epf)//'_xl.csv', status='old', action='read')
        read(16,*), ramp_func(1:nx)
        close(unit=16)

        do i = 1,nx
            if (i == 1) then
                rf_b(i) = ramp_func(i)
            else
                rf_b(i) = ramp_func(i)/( c2*(((i-1)*dx)**(75.d-2)) )
            endif
        enddo

        i = 1; ii = 1

20      nxf = dble(i-1)*dx

        if (nxf <= nvec(ii)) then
            if (nxf == nvec(ii)) then
                vl_pres_interp(i) = vl_pres(ii)
                i = i + 1
                ii  = ii + 1
                if (nxf == 1.d0) goto 25
            else
                vl_pres_interp(i) = vl_pres(ii-1) + (nxf - nvec(ii-1))&
                    *((vl_pres(ii)-vl_pres(ii-1))/(nvec(ii)-nvec(ii-1)))
                i = i + 1
            endif
        else
            ii = ii + 1
        endif
        goto 20

25      done = 1
        
        vl_pres_prev = vl_pres_interp
        vl_pres = vl_pres_prev

    endif

    !------------------Marching Loop------------
    do i = 2,nx-1
    
        if (incres == 0) then
            z = nvec(i)
            z_prev = nvec(i-1)
            dx = z - z_prev
        else
            z = (i-1)*dx
            z_prev = (i-2)*dx
        endif

        if (i == 2) then
            vl_enth_prev = vl_ip_enth
            vl_u_prev = vl_ip_u
            vl_psi_prev = vl_ip_psi
        endif
        
        !-----------Initial Guesses------------
        vl_enth = vl_enth_prev
        vl_u = vl_u_prev
        vl_psi = vl_psi_prev
        
        pres_res = 100; vcount = 0
    
15      vcount = vcount + 1
    
        xi1 = z + z_prev
        p1 = vl_pres(i) + vl_pres(i-1)
        p2 = vl_pres(i+1) - vl_pres(i)
    
        !-------------Tangent Wedging-------------
        pp1 = ( (c1*p1)/((2.d0*xi1)**(5.d-1)) ) - (1.d0/(gam*chi*chi*chi))

        vl_del(i) = ( ((8.d0*dx)/c2)*((xi1/2.d0)**(25.d-2))*pp1&
            *( (((gam+1.d0)/2.d0)*pp1 + (1.d0/(chi*chi*chi)))**(-5.d-1) )&
            - vl_del(i-1)*(3.d0*dx - 4.d0*xi1) )/(3.d0*dx + 4.d0*xi1)

        del1 = vl_del(i) + vl_del(i-1)

        ramf1 = rf_b(i) + rf_b(i-1)
    
        !---------Filling out Matrix and RHS-------
        
        va(1:v_n+1,1:v_n+1) = 0.d0; vr(1:v_n+1) = 0.d0; vx(1:v_n+1) = 0.d0
    
        !----------Wall BCs------------------------
        va(1,1) = 1.d0; vr(1) = -vl_u_prev(1) - vl_u(1)
        va(2,2) = 1.d0; vr(2) = -vl_psi_prev(1) - vl_psi(1)
        
        mc = 1
        
        do j = 2,v_ny-1
            enth1 = vl_enth(j) + vl_enth_prev(j)
            psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j)
            psi3 = vl_psi(j) - vl_psi(j-1) + vl_psi_prev(j) - vl_psi_prev(j-1)
            u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j)
            u4 = vl_u(j) + vl_u(j-1) + vl_u_prev(j) + vl_u_prev(j-1)
            u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1)
            u7 = vl_u(j+1) - 2*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2*vl_u_prev(j) + vl_u_prev(j-1)
    
            !-------Streamfunction Matrix Elements---
            va(j+mc,(j+mc)-2) = -v_dy*(del1-ramf1)
            va(j+mc,(j+mc)-1) = -4.d0
            va(j+mc,j+mc) = -v_dy*(del1-ramf1)
            va(j+mc,(j+mc)+1) = 4.d0
            !----------Streamfunction RHS------------
            vr(j+mc) = v_dy*(del1-ramf1)*u4 - 4.d0*psi3
            mc = mc + 1
            
            !-------Momentum Matrix Elements---------
            va(j+mc,(j+mc)-3) = p1*(16.d0*dx*p1 - v_dy*dx*(del1-ramf1)*psi1 - 4.d0*v_dy*xi1*(del1-ramf1)*psi2)
            va(j+mc,(j+mc)-1) = 4.d0*v_dy*v_dy*((gam-1.d0)/gam)*xi1*((del1-ramf1)**2.d0)*p2*u1&
                - p1*( 32.d0*dx*p1 + 2.d0*v_dy*v_dy*dx*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*u1&
                + 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*(u2+u1) )
            va(j+mc,j+mc)     = p1*u5*(del1-ramf1)*v_dy*(dx + 4.d0*xi1)
            va(j+mc,(j+mc)+1) = p1*(16.d0*dx*p1 + v_dy*dx*(del1-ramf1)*psi1 + 4.d0*v_dy*xi1*(del1-ramf1)*psi2)
            va(j+mc,v_n+1)    = 32.d0*dx*p1*u7 - 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*u1*u2&
                + v_dy*(del1-ramf1)*u5*(dx*psi1 + 4.d0*xi1*psi2)&
                + v_dy*v_dy*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*(enth1 - 25.d-2*u1*u1)*(4.d0*dx + 8.d0*xi1)
            !------------Momentum RHS----------------
            vr(j+mc) = 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*p1*u1*u2 - 16.d0*dx*p1*p1*u7&
                - v_dy*(del1-ramf1)*p1*u5*(4.d0*xi1*psi2 + dx*psi1)&
                + v_dy*v_dy*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*(enth1 - 25.d-2*u1*u1)*(8.d0*xi1*p2 - 4.d0*dx*p1)

        enddo
        
        !---------------BL BCs------------------
        va(v_n-1,v_n-1) = 1.d0; vr(v_n-1) = 2.d0 - vl_u_prev(v_ny) - vl_u(v_ny)
        
        va(v_n,v_n-3) = -v_dy*(del1-ramf1)
        va(v_n,v_n-2) = -4.d0
        va(v_n,v_n-1) = -v_dy*(del1-ramf1)
        va(v_n,v_n)   = 4.d0
        vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
            - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))
            
        !------------Pressure Elements-----------
        va(v_n+1,1) = 5.d-1*(vl_u(1) + vl_u_prev(1))
        mc = 1
        do j = 2,v_ny-1
            va(v_n+1,j+mc) = vl_u(j) + vl_u_prev(j)
            mc = mc + 1
        enddo
        va(v_n+1,v_n-1) = 5.d-1*(vl_u(v_ny) + vl_u_prev(v_ny))
        va(v_n+1,v_n+1) = (2.d0*((gam*c3)**(5.d-1)))/(v_dy*(gam-1.d0))
        
        !-----------------Pressure RHS---------------
        rp = ( (vl_enth_prev(1) + vl_enth(1)) - 25.d-2*((vl_u_prev(1) + vl_u(1))**2.d0) )&
            + ( (vl_enth_prev(v_ny) + vl_enth(v_ny)) - 25.d-2*((vl_u_prev(v_ny) + vl_u(v_ny))**2.d0) )
        do j = 2,v_ny-1
            rp = rp + 2.d0*( (vl_enth_prev(j) + vl_enth(j)) - 25.d-2*((vl_u_prev(j) + vl_u(j))**2.d0) )
        enddo
        vr(v_n+1) = rp - ((2.d0*((gam*c3)**(5.d-1)))/(v_dy*(gam-1.d0)))*p1
        
        !-----------------CC Formating---------------
        si = 1; ci = 1; new_col = 1
        do iii = 1,v_n+1
            do jjj = 1,v_n+1
                !-------------------------------
                if (va(jjj,iii) /= 0.d0) then
                    sprs_val(si) = va(jjj,iii)
                    sprs_rind(si) = jjj-1
                    if (new_col == 1) then
                        sprs_cc(ci) = si-1
                        ci = ci + 1
                        new_col = 0
                    endif
                    si = si + 1
                endif
                !-------------------------------
            enddo
            new_col = 1
        enddo
        sprs_cc(ci) = si-1
        
    ! !---------------------------------------------------------------
    ! !               Gaussian Elimination (subroutine)
    ! !---------------------------------------------------------------
    ! do mg = v_n+1,1,-1
    !     do ig = mg-1,1,-1
    !         vr(ig) = vr(ig)-( (va(ig,mg)*vr(mg))/va(mg,mg) )
    !         do kg = 1,v_n+1
    !             va(ig,kg) = va(ig,kg) - ( (va(ig,mg)*va(mg,kg))/va(mg,mg) )
    !         enddo
    !     enddo
    ! enddo

    ! !---------------------------------------------------------------
    ! !               Forward Substitution (subroutine)
    ! !---------------------------------------------------------------
    ! vx(1:v_n+1) = 0.d0

    ! vx(1) = vr(1)/va(1,1)

    ! do ig = 2, v_n+1
    !     tg = vr(ig)
    !     do kg = 1,ig
    !         tg = tg - ( va(ig,kg)*vx(kg) )
    !     enddo
    !     vx(ig) = tg/va(ig,ig)
    ! enddo

        !--------------Matrix Solve------------------
        !   Set default control parameters
        call umf4def(control)
        !
        !   From the matrix data, create the symbolic factorization information
        call umf4sym(v_n+1,v_n+1,sprs_cc,sprs_rind,sprs_val,symbolic,control,info)
        if (info(1) < 0) then
            print *, 'We did a bad'
            print *, 'umf4sym', info(1)
            stop 1
        endif
        !
        !   From the symbolic factorization information, carry out the numeric factorization
        call umf4num(sprs_cc,sprs_rind,sprs_val,symbolic,numeric,control,info)
        if (info(1) < 0) then
            print *, 'We did a bad'
            print *, 'umf4num', info(1)
            stop 1
        endif
        !
        !   Free the memory associated with the symbolic factorization
        ! call umf4fsym(symbolic)
        !
        !   Solve the linear system
        call umf4sol(sys,vx,vr,numeric,control,info)
        if (info(1) < 0) then
            print *, 'We did a bad'
            print *, 'umf4sol', info(1)
            stop 1
        endif
        !
        !   Free the memory associated with the numeric factorization
        ! call umf4fnum(numeric)
        !
        !-------------------------------------------
        
        mc = 0
        vl_u_out(1:v_ny) = 0.d0
        vl_psi_out(1:v_ny) = 0.d0
        do j = 1,v_ny
            vl_u_out(j) = vl_u(j) + vx(j+mc)
            mc = mc + 1
            vl_psi_out(j) = vl_psi(j) + vx(j+mc)
        enddo
        vl_pres_out = vl_pres(i) + vx(v_n+1)
        
        if (vl_pres_out < 0.d0) then
            vl_pres_prev(1:i-1) = vl_pres(1:i-1)
            vl_pres_prev(i:nx-1) = vl_pres(i)
            goto 10
        endif
        
        vl_u = vl_u_out
        vl_psi = vl_psi_out
        vl_pres(i) = vl_pres_out
        
        pres_res = dabs(vx(v_n+1))
        
        if (pres_res > 1.d-8) goto 15
        
        vl_u_save(1:v_ny,i) = vl_u(1:v_ny)
        vl_psi_save(1:v_ny,i) = vl_psi(1:v_ny)
    
    enddo
    
    pres_con = maxval(dabs(vl_pres(1:nx) - vl_pres_prev(1:nx)))
    
    if (mod(sweep,10) == 0) then
        inquire(file='stat_us.txt',exist = ex)
        if (ex) then
            open(unit = v_fid, file='stat_us.txt', status='old', action='write')
            call date_and_time(date,time,zone,values)
            currenttime = (/values(3), values(5), values(6), values(7), values(8)/)
            elapsetime = abs(currenttime - starttime)
            write(v_fid,*) 'starting point = ', s_p,'sweep = ', sweep,'pressure_diff = ', pres_con
            write(v_fid,*) 'elapsed(D,H,Mi,S,Ms)', elapsetime
            close(unit = v_fid)
            v_fid = v_fid + 2
        else
            open(unit = v_fid, file='stat_us.txt', status='new', action='write')
            call date_and_time(date,time,zone,values)
            currenttime = (/values(3), values(5), values(6), values(7), values(8)/)
            elapsetime = abs(currenttime - starttime)
            write(v_fid,*) 'starting point = ', s_p,'sweep = ', sweep,'pressure_diff = ', pres_con
            write(v_fid,*) 'elapsed(D,H,Mi,S,Ms)', elapsetime
            close(unit = v_fid)
            v_fid = v_fid + 2
        endif
    endif

    if (pres_con > 1.d-8) then
        vl_pres_prev = vl_pres
        goto 10
    endif
    
    write(spf,'(I1.1)') int(s_p)
    write(epf,'(I1.1)') int(e_p)
    
    open(unit=15, file='datadump/u_'//trim(spf)//'_'//trim(epf)//'.dat', status='new', action='write')
    do j = 1,501
        write(15,*), vl_u_save(j,1:1001)
    enddo
    close(unit=15)
    
    open(unit=20, file='datadump/psi_'//trim(spf)//'_'//trim(epf)//'.dat', status='new', action='write')
    do j = 1,501
        write(20,*), vl_psi_save(j,1:1001)
    enddo
    close(unit=20)
    
    open(unit=25, file='datadump/pres_'//trim(spf)//'_'//trim(epf)//'.dat', status='new', action='write')
    do j = 1,1001
        write(25,*), vl_pres(j)
    enddo
    close(unit=25)
    
    open(unit=30, file='datadump/del_'//trim(spf)//'_'//trim(epf)//'.dat', status='new', action='write')
    do j = 1,1001
        write(30,*), vl_del(j)
    enddo
    close(unit=30)
    
    enddo
    
end program
