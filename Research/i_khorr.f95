program inv_korr
    implicit none
    
    double precision, allocatable :: eta(:),vvel(:),rho(:),pres(:),up(:),a(:,:),q(:)
    double precision, allocatable :: r(:),p(:),vvel_out(:),rho_out(:),pres_out(:)
    double precision, allocatable :: xx(:),yy(:),x(:)
    double precision :: h,gam,c5,sumx,sumy,t,rho1,rho2,v1,v2,p1,p2,eta1,f1,c5_out
    double precision :: errf, errf_prev
    integer :: iter,fid,nj,n,j,i,mc,m,k,l,f
    character*15 :: str

    iter = 0; fid = 11

    nj = 501; n = 3*nj; h = 1.d0/dble(nj-1); gam = 14.d-1
    errf = 1.d2
    allocate(eta(1:nj))

	do j = 1, nj
        eta(j) = h*dble(j-1)
	enddo

    allocate(vvel(1:nj)); allocate(rho(1:nj)); allocate(pres(1:nj))
    allocate(a(1:n+1,1:n+1)); allocate(up(1:n)); allocate(r(1:n+1))
    allocate(vvel_out(1:nj)); allocate(rho_out(1:nj)); allocate(pres_out(1:nj))
    allocate(xx(1:n)); allocate(yy(1:n)); allocate(x(1:n+1))

!-------------------------------------------------------
!   Initial Guesses
!-------------------------------------------------------
    c5 = 3.d0/2.d0
	do j = 1, nj
        vvel(j) = 75.d-2*(1.d0-eta(j)) + ((3.d0*c5*eta(j))/(2.d0*(gam+1.d0)))
        rho(j)  = ((gam+1.d0)/(gam-1.d0))*eta(j)
        pres(j) = (9.d0*(c5**2.d0)*eta(j))/(8.d0*(gam+1.d0))
	enddo

!-------------------------------------------------------
!   Loop Start
!-------------------------------------------------------

1   iter = iter + 1
    print *, iter
 
    a(1:n+1,1:n+1) = 0.d0; r(1:n+1) = 0.d0; x(1:n+1) = 0.d0

!-------------------------------------------------------
!   Matrix Initialization
!-------------------------------------------------------
    !----Boundary Layer BCs
    a(1,1) = 1.d0; r(1) = 75.d-2 - vvel(1)

    mc = 0

	do j = 2, nj
        v1 = vvel(j) + vvel(j-1); v2 = vvel(j) - vvel(j-1)
        rho1 = rho(j) + rho(j-1); rho2 = rho(j) - rho(j-1)
        p1 = pres(j) + pres(j-1); p2 = pres(j) - pres(j-1)
        eta1 = eta(j) + eta(j-1)
        f1 = 3.d0*(eta1-2.d0) - 3.d0*c5*eta1 + 4.d0*v1

        !------Continuity
        a(j+mc,(j+mc)-1) = 4.d0*(rho2-rho1)
        a(j+mc,j+mc)     = 4.d0*v2 - f1
        a(j+mc,(j+mc)+2) = 4.d0*(rho2+rho1)
        a(j+mc,(j+mc)+3) = 4.d0*v2 + f1
        a(j+mc,n+1) = -3.d0*eta1*rho2
        !
        r(j+mc) = -f1*rho2 - 4.d0*rho1*v2

        mc = mc + 1

        !------Momentum
        a(j+mc,(j+mc)-2) = -rho1*f1 + 4.d0*rho1*v2 - h*rho1*(c5 - 1.d0)
        a(j+mc,(j+mc)-1) = v2*f1 - h*(c5 - 1.d0)*v1
        a(j+mc,j+mc)     = -16.d0
        a(j+mc,(j+mc)+1) = rho1*f1 + 4.d0*rho1*v2 - h*rho1*(c5 - 1.d0)
        a(j+mc,(j+mc)+2) = v2*f1 - h*(c5 - 1.d0)*v1
        a(j+mc,(j+mc)+3) = 16.d0
        a(j+mc,n+1) = -3.d0*eta1*rho1*v2 - h*rho1*v1
        !
        r(j+mc) = -f1*rho1*v2 - 16.d0*p2 + h*rho1*v1*(c5-1.d0)

        mc = mc + 1

        !------Energy
        a(j+mc,(j+mc)-3) = 4.d0*(rho1*p2 - gam*p1*rho2)
        a(j+mc,(j+mc)-2) = f1*(p2 + gam*p1) - 2.d0*h*(c5-1.d0)*p1
        a(j+mc,(j+mc)-1) = -f1*(rho1+gam*rho2) - 2.d0*h*(c5-1.d0)*rho1
        a(j+mc,j+mc)     = 4.d0*(rho1*p2 - gam*p1*rho2)
        a(j+mc,(j+mc)+1) = f1*(p2 - gam*p1) - 2.d0*h*(c5-1.d0)*p1
        a(j+mc,(j+mc)+2) = f1*(rho1-gam*rho2) - 2.d0*h*(c5-1.d0)*rho1
        a(j+mc,n+1) = -3.d0*eta1*(rho1*p2 - gam*p1*rho2) - 2.d0*h*rho1*p1
        !
        r(j+mc) = 2.d0*h*(c5-1.d0)*rho1*p1 - f1*(rho1*p2 - gam*p1*rho2)
	enddo

    !---Shock BCs
    a(n-1,n-1) = 1.d0
    r(n-1) = ((gam+1.d0)/(gam-1.d0)) - rho(nj)

    a(n,n) = 1.d0
    a(n,n+1) = -(9.d0*c5)/(4.d0*(gam+1.d0))
    r(n)   = ((9.d0*c5*c5)/(8.d0*(gam+1.d0))) - pres(nj)

    a(n+1,n-2) = 1.d0
    a(n+1,n+1) = -3.d0/(2.d0*(gam+1.d0))
    r(n+1) = ((3.d0*c5)/(2.d0*(gam+1.d0))) - vvel(nj)

!-----------------------------------------------------
!   Gaussian Elimination
!-----------------------------------------------------
    do m = n+1,1,-1
		do i = m-1,1,-1
            r(i) = r(i)-( (a(i,m)*r(m))/a(m,m) )
			do k = 1,n+1
                a(i,k) = a(i,k) - ( (a(i,m)*a(m,k))/a(m,m) )
			enddo
		enddo
	enddo
!-----------------------------------------------------
!   Forward Substitution
!-----------------------------------------------------
    x(1) = r(1)/a(1,1)

	do i = 2,n+1
        t = r(i)
		do k = 1,i
            t = t - ( a(i,k)*x(k) )
		enddo
        x(i) = t/a(i,i)
	enddo
! !----------------------------------------------------------------------------
! ! Matrix solving shenanigans
! !----------------------------------------------------------------------------

!     A(n-2,1:9) = A(n-2,1:9) + A(n-1,1:9)
!     if (Pr == 1.d0) then
!         A(n-3,1:9) = A(n-3,1:9) + A(n-2,1:9)
!         do l = 6,n-6,3
!             A(n-(l-1),1:9) = A(n-(l-1),1:9) + A(n-l,1:9)
!         enddo
!     endif

!     ! l = 9
!     do k = n-4,5,-3
!         do m = 2,9
!             R(k) = R(k) - (A(k,9)/A(k+1,8))*R(k+1)
!             A(k,m) = A(k,m) - (A(k,9)/A(k+1,8))*A(k+1,m-1)
!         enddo
!     enddo
!     ! l = 8
!     do k = n-3,4,-1
!         do m = 2,8
!             R(k) = R(k) - (A(k,8)/A(k+1,7))*R(k+1)
!             A(k,m) = A(k,m) - (A(k,8)/A(k+1,7))*A(k+1,m-1)
!         enddo
!     enddo
!     ! l = 7
!     do k = n-2,4,-1
!         do m = 2,7
!             R(k) = R(k) - (A(k,7)/A(k+1,6))*R(k+1)
!             A(k,m) = A(k,m) - (A(k,7)/A(k+1,6))*A(k+1,m-1)
!         enddo
!     enddo

!     if (A(1,9) == 0) then
!         x(1) = R(1)/A(1,6)
!     else
!         x(1) = (R(1) - x(4)*A(1,9))/A(1,6)
!     endif

!     m = 4
!     do k = 2,5
!         axsum = 0
!         do l = 1,k-1
!             axsum = axsum + A(k,l+m)*x(l)
!         enddo
!         x(k) = (R(k) - axsum)/A(k,6)
!         m = m - 1
!     enddo

!     do k = 6,n
!         axsum = 0
!         do l = 1,5
!             axsum = axsum + A(k,l)*x(k-l)
!         enddo
!         x(k) = (R(k) - axsum)/A(k,6)
!     enddo
!-----------------------------------------------------
!   Update Uphats
!-----------------------------------------------------

    mc = 0
	do j = 1, nj
        vvel_out(j) = vvel(j) + x(j+mc)
        mc = mc + 1
        rho_out(j) = rho(j) + x(j+mc)
        mc = mc + 1
        pres_out(j) = pres(j) + x(j+mc)
	enddo
    c5_out = c5 + x(n+1)
    
    errf_prev = errf
    errf = dabs(x(n+1))

    if (errf < errf_prev) then
        vvel(1:nj) = vvel_out(1:nj)
        rho(1:nj) = rho_out(1:nj)
        pres(1:nj) = pres_out(1:nj)
        c5 = c5_out
        goto 1
    endif

    open(unit=fid, file='il_khm_500.dat', status='new', action='write')
    do j = 1,nj
        write(fid,*), vvel(j), rho(j), pres(j)
    enddo
    close(unit=fid)
    fid = fid + 1

    open(unit=fid, file='c5con_khm_500.dat', status='new', action='write')
    write(fid,*), c5
    close(unit=fid)
    fid = fid + 1

end program