program kmethod

    implicit none

    double precision, allocatable :: A(:,:), x(:), R(:), up(:)
    double precision, allocatable :: enth(:), vel(:), psi(:), eta(:)
    double precision, allocatable :: enth_out(:), vel_out(:), psi_out(:)
    double precision :: Pr,h,gam,c6,su,t,d1,d2,d3,d,c3t,c3,c3_prev,c3diff
    double precision :: entherr, velerr, psierr, errf, errf_prev, axsum
    integer :: n,i,l,k,m,q,cou,f,fid,nn,mc,pts,check,pop
    character*15 :: str

    Pr = 1.d0

    cou = 0; check = 0; fid = 11; c3_prev = 0.d0; errf = 1.d10

    nn = 5001; n = 2*nn; h = 10.d0/(nn-1); gam = 14.d-1
    
    c6 = 4.d0*(h**2.d0)*((gam-1.d0)/gam)

    allocate(A(1:n,1:5)); allocate(x(1:n)); allocate(R(1:n))
    allocate(enth(1:nn)); allocate(vel(1:nn))
    allocate(psi(1:nn)); allocate(eta(1:nn)); allocate(enth_out(1:nn))
    allocate(vel_out(1:nn)); allocate(psi_out(1:nn))

    do i = 1,nn
        eta(i) = (i-1)*h
    enddo
!-----------------------------------------------------------------
!Initialize up-hat elements
!-----------------------------------------------------------------

	do i = 1,nn
        enth(i) = 5.d-1 
        vel(i)  = 1.d0 - exp(-2.d0*eta(i))                             !Momentum
        psi(i)  = eta(i) + 5.d-1*(exp(-2.d0*eta(i)) - 1.d0)            !StreamFunc
	enddo

!-----------------------------------------------------------------
! Loop start
!-----------------------------------------------------------------
1   cou = cou + 1

    A(1:n,1:5) = 0.d0
    x(1:n) = 0.d0
    R(1:n) = 0.d0

!-----------------------------------------------------------------
!Matrix initialization
!-----------------------------------------------------------------
    
    !A(1,1) = 1.d0; R(1) = -vel(1)
    A(1,4) = 1.d0; R(1) = -vel(1)

    !A(2,2) = 1.d0; R(2) = -psi(1)
    A(2,4) = 1.d0; R(2) = -psi(1)
    
    mc = 1

	do i = 2,nn-1

!Steamfunction elements
        A(i+mc,2) = h
        A(i+mc,3) = 2.d0
        A(i+mc,4) = h
        A(i+mc,5) = -2.d0
        ! A(i+mc,(i-2)+mc) = h
        ! A(i+mc,(i-1)+mc) = 2.d0
        ! A(i+mc,i+mc) = h
        ! A(i+mc,(i+1)+mc) = -2.d0
        
        R(i+mc) = 2.d0*(psi(i) - psi(i-1)) - h*(vel(i) + vel(i-1))

        mc = mc + 1

!Momentum elements
        A(i+mc,1) = 8.d0 - h*psi(i)                   
        A(i+mc,3) = -(16.d0 + c6*vel(i))
        A(i+mc,4) = h*(vel(i+1) - vel(i-1))
        A(i+mc,5) = 8.d0 + h*psi(i)
        ! A(i+mc,(i-3)+mc) = 8.d0 - h*psi(i)
        ! A(i+mc,(i-1)+mc) = -(16.d0 + c6*vel(i))
        ! A(i+mc,i+mc) = h*(vel(i+1) - vel(i-1))
        ! A(i+mc,(i+1)+mc) = 8.d0 + h*psi(i)
        
        R(i+mc) = -8.d0*(vel(i+1) - 2.d0*vel(i) + vel(i-1))&
            - h*psi(i)*(vel(i+1) - vel(i-1))&
            + (c6/2.d0)*(vel(i)**2.d0 - 2.d0*enth(i))

    enddo

    A(n-1,4) = 1.d0; R(n-1) = 1.d0 - vel(nn)
    !A(n-1,n-1) = 1.d0; R(n-1) = 1.d0 - vel(nn)

    A(n,1) = h
    A(n,2) = 2.d0
    A(n,3) = h
    A(n,4) = -2.d0
    ! A(n,n-3) = h
    ! A(n,n-2) = 2.d0
    ! A(n,n-1) = h
    ! A(n,n)   = -2.d0
    
    R(n)   = 2.d0*(psi(nn) - psi(nn-1)) - h*(vel(nn) + vel(nn-1))
    
! !----------------------------------------------------------------------------
! ! Matrix solving shenanigans
! !----------------------------------------------------------------------------

    ! l = 5
    do k = n-2,3,-1
        R(k) = R(k) - (A(k,5)/A(k+1,4))*R(k+1)
        do m = 2,5
            A(k,m) = A(k,m) - (A(k,5)/A(k+1,4))*A(k+1,m-1)
        enddo
    enddo

    x(1) = R(1)/A(1,4)
    x(2) = R(2)/A(2,4)
    x(3) = (R(3) - x(1)*A(3,2) - x(2)*A(3,3))/A(3,4)

    pop = 0
    do k = 4,n
        axsum = R(k)
        do l = 1,3
            axsum = axsum - ( A(k,l)*x(l+pop) )
        enddo
        pop = pop + 1
        x(k) = axsum/A(k,4)
    enddo
    
!----------------------------------------------------------------------------
! Matrix solving shenanigans
!----------------------------------------------------------------------------

    ! l = 5
    ! do m = n-1,4,-1
        ! do i = m-1,1,-1
        
            ! R(i) = R(i)-( (A(i,m)*R(m))/A(m,m) )
        
            ! if ( m > 4) then
                ! do k  = m-3,m+1
                    ! A(i,k) = A(i,k) - ( (A(i,m)*A(m,k))/A(m,m) )
                ! enddo
            ! elseif (m == 4) then
                ! do k = 1,4
                    ! A(i,k) = A(i,k) - ( (A(i,m)*A(m,k))/A(m,m) )
                ! enddo
            ! endif
            
        ! enddo
    ! enddo
    
    ! x(1) = R(1)/A(1,1)
    ! x(2) = R(2)/A(2,2)
    ! x(3) = (R(3) - (A(3,1)*x(1)) - (A(3,2)*x(2)))/A(3,3)
    
    ! !open(unit = fid, file='bad_ta.dat', status='new', action='write')
    ! do i = 4,n
        ! t = R(i)
		! do k = i-3,i-1
            ! print *, k
            ! t = t - ( A(i,k)*x(k) )
		! enddo
        ! stop 1
    ! !   write(fid,*), t, A(i,i)
        ! x(i) = t/A(i,i)
	! enddo
    ! !close(unit=fid)

!----------------------------------------------------------------------------
! Update uphat values
!----------------------------------------------------------------------------
    mc = 0
	do i = 1,nn
        vel_out(i) = vel(i) + x(i+mc)
        mc = mc + 1
        psi_out(i) = psi(i) + x(i+mc)
	enddo

    velerr = maxval(dabs(vel_out(1:nn) - vel(1:nn)))
    psierr = maxval(dabs(psi_out(1:nn) - psi(1:nn)))
    
    print *, cou, velerr, psierr

    errf_prev = errf
    errf = max(velerr,psierr)
    
    vel(1:nn) = vel_out(1:nn)
    psi(1:nn) = psi_out(1:nn)

	if (errf > 1.d-8) goto 1

!------------------------------------
    
    open(unit=fid, file='vl_pr1adi_5000.dat', status='new', action='write')
    do i = 1,nn
                     !Enthalpy,   Velocity,  Streamfunction
        write(fid,*), enth(i),     vel(i),      psi(i)
    enddo
    close(unit=fid)
    fid = fid + 1

!--------------
! c3
!--------------
    c3t = 5.d-1*(2.d0*enth(1) - vel(1)**2.d0)
    do i = 2,nn-1
        c3t = c3t + (2.d0*enth(i) - vel(i)**2.d0)
    enddo
    c3t = c3t + 5.d-1*(2.d0*enth(nn) - vel(nn)**2.d0)

    c3 = (((h**2.d0)*((gam-1.d0)**2.d0))/(4.d0*gam))*(c3t**2.d0)
    
    open(unit=99, file='c3con_pr1adi_5000.dat', status='new', action='write')
    write(99,*), c3
    close(unit=99) 

    deallocate(A); deallocate(x); deallocate(R)
    deallocate(enth); deallocate(vel); deallocate(psi); deallocate(eta)
    deallocate(enth_out); deallocate(vel_out); deallocate(psi_out)

end program