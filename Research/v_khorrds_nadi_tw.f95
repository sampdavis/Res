program khorr_vds_nadi_tanwedge
    implicit none

    double precision, allocatable :: vl_ip_enth(:),vl_ip_u(:),vl_ip_psi(:),vl_del(:),vl_pres(:)
    double precision, allocatable :: vl_enth(:),vl_u(:),vl_psi(:),vl_u_out(:),vl_psi_out(:)
    double precision, allocatable :: vl_enth_prev(:),vl_u_prev(:),vl_psi_prev(:),vl_u_save(:,:)
    double precision, allocatable :: va(:,:),vr(:),vx(:),vl_pres_prev(:),vl_psi_save(:,:)
    double precision, allocatable :: ramp_func(:),v_dens(:,:),v_temp(:,:),v_mu(:,:),eta(:),v_v(:,:)
    double precision, allocatable :: vl_enth_save(:,:),rf_bar(:),vl_enth_out(:)
    double precision :: Pr,gam,c,chi,v_dy,dx,c1,c2,c3,x1,x2,x3,x4,il_p_wall,z,z_prev,lam
    double precision :: xi1,p1,p2,pp1,del1,enth1,psi1,psi2,psi3,u1,u2,u4,u5,u7,rp,t,enth2,enth3,enth4
    double precision :: vl_pres_out,pres_sweep_err,pend,err_out,rf1,rf_ang,rs
    integer :: v_ny,v_n,nx,v_fid,j,sweep,e,vcount,mc,m,i,k,f,e_curr,e_prev,hlfpt

    !character(*), parameter :: vlip = 'c:\users\sampson\documents\github\res\research\vl_ip\' !'
    !character(*), parameter :: ilip = 'c:\users\sampson\documents\github\res\research\il_ip\' !'
    !character(*), parameter :: vlds = 'c:\users\sampson\documents\github\res\research\vl_ds\' !'

    Pr = 1.d0; gam = 14.d-1; c = 1.d0;

    v_ny = 501; v_n = 3*v_ny; v_dy = 10.d0/dble(v_ny - 1)

    nx = 201; dx = 1.d0/dble(nx - 1)

    v_fid = 11

    allocate(vl_ip_enth(v_ny)); allocate(vl_ip_u(v_ny)); allocate(vl_ip_psi(v_ny))
    allocate(vl_enth(v_ny)); allocate(vl_u(v_ny)); allocate(vl_psi(v_ny))
    allocate(vl_enth_prev(v_ny)); allocate(vl_u_prev(v_ny)); allocate(vl_psi_prev(v_ny))
    allocate(va(v_n+1,v_n+1)); allocate(vr(v_n+1)); allocate(vx(v_n+1))
    allocate(vl_pres(nx)); allocate(vl_del(nx)); allocate(vl_pres_prev(nx))
    allocate(vl_u_save(v_ny,nx)); allocate(vl_psi_save(v_ny,nx))
    allocate(vl_u_out(v_ny)); allocate(vl_psi_out(v_ny))
    allocate(ramp_func(nx)); allocate(v_dens(v_ny,nx)); allocate(v_temp(v_ny,nx))
    allocate(v_mu(v_ny,nx)); allocate(v_v(v_ny,nx)); allocate(eta(v_ny))
    allocate(rf_bar(nx)); allocate(vl_enth_out(v_ny)); allocate(vl_enth_save(v_ny,nx))

    open(unit=9, file="c3con_pr101.dat", status="old", action="read")
    read(9,*), c3
    close(unit=9)

    !----------------Viscous Layer (IP)-----------------------
    open(unit=v_fid, file='vl_pr101.dat', status='old', action='read')
    do j = 1,v_ny
        read(v_fid,*), vl_ip_enth(j), vl_ip_u(j), vl_ip_psi(j)
    enddo
    close(unit=v_fid)
    v_fid = v_fid + 2
    !----------------Inviscid Layer (IP)----------------------
    open(unit=12, file="il_khm_500.dat", status="old", action="read")
    read(12,*), x1, x2, il_p_wall
    close(unit = 12)

    !------C1 and C2 constants
    c2 = ((c*c3)/il_p_wall)**(1.d0/4.d0)
    c1 = (c*c3)/(c2*c2)

    !------Chi values
    !chi_vec(1:7) = (/ 78.d-2, 1.d0, 2.d0, 3.d0, 5.d0, 10.d0, 20.d0 /)

    !do cc = 1,7

        chi = 4.d0 !chi_vec(cc)

    !------Initial BL thickness vector
    vl_del(1:nx) = 0.d0
    vl_del(1) = 1.d0

    !------Initial Pressure vector
    vl_pres_prev(1:nx-1) = 1.d0
    vl_pres_prev(nx) = 1.d0/(c1*gam*chi*chi*chi)

    !------Ramp function
    hlfpt = (nx-1)/2
    rf_ang = 10.d0
    rs = dtan(rf_ang)/dble(nx-1)
    ramp_func(1:nx) = 0.d0
    do i = 1,hlfpt+1
        ramp_func(hlfpt+i) = rs*dble(i-1)
    enddo
    rf_bar(1:nx) = ramp_func(1:nx)
    do i = 2,nx
        rf_bar(i) = ramp_func(i)/(c2*((dble(i-1)*dx)**(3.d0/4.d0)))
    enddo

    !------Downstream data arrays
    do e = 1,nx
        vl_enth_save(1:v_ny,e) = vl_ip_enth(1:v_ny)
        vl_u_save(1:v_ny,e) = vl_ip_u(1:v_ny)
        vl_psi_save(1:v_ny,e) = vl_ip_psi(1:v_ny)
    enddo

    sweep = 0
2   sweep = sweep + 1
    print *, "sweep", sweep
    vl_pres(1:nx) = vl_pres_prev(1:nx)

    do e = 2,nx-1
        print *, 'downstream step', e
        z = dble(e-1)*dx
        z_prev = dble(e-2)*dx
        !---------------------------------------------------------------
        !                       Previous values
        !---------------------------------------------------------------
        if (e == 2) then
            vl_enth_prev(1:v_ny) = vl_ip_enth(1:v_ny)
            vl_u_prev(1:v_ny) = vl_ip_u(1:v_ny)
            vl_psi_prev(1:v_ny) = vl_ip_psi(1:v_ny)
        endif
        !---------------------------------------------------------------
        !                        Initial Guesses
        !---------------------------------------------------------------
        vl_enth(1:v_ny) = vl_enth_prev(1:v_ny)
        vl_u(1:v_ny) = vl_u_prev(1:v_ny)
        vl_psi(1:v_ny) = vl_psi_prev(1:v_ny)

        !---------------------------------------------------------------
        !                         Loop Start
        !---------------------------------------------------------------
        vcount = 0
1       vcount = vcount + 1

        xi1 = z + z_prev; p1 = vl_pres(e) + vl_pres(e-1); p2 = vl_pres(e+1) - vl_pres(e)
        !---------------------------------------------------------------
        !                        BL Thickness
        !---------------------------------------------------------------
        pp1 = ( (c1*p1)/((2.d0*xi1)**(1.d0/2.d0)) ) - (1.d0/(gam*chi*chi*chi))

        vl_del(e) = ( ((8.d0*dx)/c2)*((xi1/2.d0)**(1.d0/4.d0))*pp1&
            *( (((gam+1.d0)/2.d0)*pp1 + (1.d0/(chi*chi*chi)))**(-1.d0/2.d0) )&
             - vl_del(e-1)*(3.d0*dx - 4.d0*xi1) )/(3.d0*dx + 4.d0*xi1)

        del1 = vl_del(e) + vl_del(e-1)

        rf1 = rf_bar(e) + rf_bar(e-1)
        !---------------------------------------------------------------
        !                        Matrix Solve
        !---------------------------------------------------------------
        va(1:v_n+1,1:v_n+1) = 0.d0; vr(1:v_n+1) = 0.d0

        !Wall BCs
        va(1,1) = 1.d0
        vr(1) = 2.d0*1.d-1 - vl_enth_prev(1) - vl_enth(1)

        va(2,2) = 1.d0
        vr(2) = -vl_u_prev(1) - vl_u(1)

        va(3,3) = 1.d0
        vr(3) = -vl_psi_prev(1) - vl_psi(1)

        mc = 2

    do j = 2, v_ny-1 
        enth1 = vl_enth(j) + vl_enth_prev(j); enth2 = vl_enth(j) - vl_enth_prev(j)
        enth3 = vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1)
        enth4 = vl_enth(j+1) - 2.d0*vl_enth(j) + vl_enth(j-1)&
            + vl_enth_prev(j+1) - 2.d0*vl_enth_prev(j) + vl_enth_prev(j-1)
        psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j)
        psi3 = vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j)
        u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j)
        u4 = vl_u(j+1) + vl_u(j) + vl_u_prev(j+1) + vl_u_prev(j)
        u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1)
        u7 = vl_u(j+1) - 2.d0*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2.d0*vl_u_prev(j) + vl_u_prev(j-1)

        !Streamfunction elements
        va(j+mc,(j+mc)-2) = -v_dy*(del1-rf1)
        va(j+mc,(j+mc)-1) = -4.d0
        va(j+mc,(j+mc)+1) = -v_dy*(del1-rf1)
        va(j+mc,(j+mc)+2) = 4.d0
        !----Streamfunction RHS
        vr(j+mc) = v_dy*(del1-rf1)*u4 - 4.d0*psi3

        mc = mc + 1

        !Momentum elements
        va(j+mc,(j+mc)-3) = p1*(16.d0*dx*p1 - v_dy*dx*(del1-rf1)*psi1 - 4.d0*v_dy*xi1*(del1-rf1)*psi2)
        va(j+mc,(j+mc)-1) = v_dy*v_dy*((gam-1.d0)/gam)*((del1-rf1)**2.d0)*(-8.d0*xi1*p2 + dx*p1)
        va(j+mc,(j+mc)) = 4.d0*v_dy*v_dy*((gam-1.d0)/gam)*xi1*(del1-rf1)*(del1-rf1)*p2*u1&
                    - p1*( 32.d0*dx*p1 + 2.d0*v_dy*v_dy*dx*((gam-1.d0)/gam)*(del1-rf1)*(del1-rf1)*u1&
                    + 4.d0*v_dy*v_dy*xi1*(del1-rf1)*(del1-rf1)*(u2+u1) )
        va(j+mc,(j+mc)+1) = p1*u5*(del1-rf1)*v_dy*(dx + 4.d0*xi1)
        va(j+mc,(j+mc)+3) = p1*(16.d0*dx*p1 + v_dy*dx*(del1-rf1)*psi1 + 4.d0*v_dy*xi1*(del1-rf1)*psi2)
        va(j+mc,v_n+1)    = 32.d0*dx*p1*u7 - 4.d0*v_dy*v_dy*xi1*(del1-rf1)*(del1-rf1)*u1*u2&
                    + v_dy*(del1-rf1)*u5*(dx*psi1 + 4.d0*xi1*psi2)&
                    + v_dy*v_dy*((gam-1.d0)/gam)*(del1-rf1)*(del1-rf1)*(enth1 - 25.d-2*u1*u1)*(4.d0*dx + 8.d0*xi1)
        !----Momentum RHS
        vr(j+mc) = 4.d0*v_dy*v_dy*xi1*(del1-rf1)*(del1-rf1)*p1*u1*u2 - 16.d0*dx*p1*p1*u7&
                    - v_dy*(del1-rf1)*p1*u5*(4.d0*xi1*psi2 + dx*psi1)&
                    + 4.d0*v_dy*v_dy*((gam-1.d0)/gam)*(del1-rf1)*(del1-rf1)*(enth1 - 25.d-2*u1*u1)*(2.d0*xi1*p2 - dx*p1)

        mc = mc + 1

        !Energy elements
        va(j+mc,(j+mc)-5) = v_dy*(del1-rf1)*(4.d0*Pr*xi1*psi2 + dx*psi1) - 16.d0*dx*p1
        va(j+mc,(j+mc)-4) = 8.d0*dx*(Pr-1.d0)*p1*(u5 - u1)
        va(j+mc,(j+mc)-2) = v_dy*(del1-rf1)*(8.d0*v_dy*Pr*xi1 + dx*psi1) + 32.d0*dx*p1
        va(j+mc,(j+mc)-1) = -8.d0*dx*(Pr-1.d0)*p1*(u7 - 2.d0*u1)
        va(j+mc,(j+mc)) = -v_dy*enth3*(del1-rf1)*(4.d0*Pr*xi1 + dx)
        va(j+mc,(j+mc)+1) = -v_dy*(del1-rf1)*(4.d0*Pr*xi1*psi2 + dx*psi1) - 16.d0*dx*p1
        va(j+mc,(j+mc)+2) = -8.d0*dx*(Pr-1.d0)*p1*(u5 + u1)
        va(j+mc,v_n+1) = -16.d0*dx*enth4 - dx*(Pr-1.d0)*(4.d0*u5*u5 + 8.d0*u1*u7)

        !----Energy RHS
        vr(j+mc) = -8.d0*v_dy*v_dy*Pr*xi1*(del1-rf1)*enth2&
            + 4.d0*v_dy*Pr*xi1*(del1-rf1)*psi2*enth3&
            + v_dy*dx*(del1-rf1)*psi1*enth3&
            + 16.d0*dx*p1*enth4&
            + 4.d0*dx*(Pr-1.d0)*p1*(u5*u5 + 2.d0*u1*u7)
    enddo
        !---BL BCs
        va(v_n-2,v_n-2) = 1.d0
        vr(v_n-2) = 1.d0 - vl_enth_prev(v_ny) - vl_enth(v_ny)

        va(v_n-1,v_n-1) = 1.d0
        vr(v_n-1) = 2.d0 - vl_u_prev(v_ny) - vl_u(v_ny)

        va(v_n,v_n-4) = -v_dy*(del1-rf1)
        va(v_n,v_n-3) = -4.d0
        va(v_n,v_n-1) = -v_dy*(del1-rf1)
        va(v_n,v_n)   = 4.d0
        vr(v_n)   = v_dy*(del1-rf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
           - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))

        !Pressure Elements
        va(v_n+1,1) = 2.d0
        va(v_n+1,2) = -(vl_u_prev(1) + vl_u(1))

        mc = 2
        do j = 2,v_ny-1
            va(v_n+1,j+mc) = 4.d0
            mc = mc + 1
            va(v_n+1,j+mc) = -2.d0*(vl_u_prev(j) + vl_u(j))
            mc = mc + 1
        enddo

        va(v_n+1,v_n-2) = 2.d0
        va(v_n+1,v_n-1) = -(vl_u_prev(v_ny) + vl_u(v_ny))
        va(v_n+1,v_n+1) = -(4.d0*((gam*c3)**(1.d0/2.d0)))/(v_dy*(gam-1.d0))

        !Pressure RHS
        rp = (2.d0*(vl_enth_prev(1) + vl_enth(1)) - 5.d-1*(vl_u_prev(1) + vl_u(1))**2.d0)&
            + (2.d0*(vl_enth_prev(v_ny) + vl_enth(v_ny)) - 5.d-1*(vl_u_prev(v_ny) + vl_u_prev(v_ny))**2.d0)
        do j = 2, v_ny-1
            rp = rp + (4.d0*(vl_enth_prev(j) + vl_enth(j)) - (vl_u_prev(j) + vl_u(j))**2.d0)
        enddo

        vr(v_n+1) = ((4.d0*((gam*c3)**(1.d0/2.d0))*(vl_pres(e) + vl_pres(e-1)))/(v_dy*(gam - 1.d0))) - rp
        !---------------------------------------------------------------
        !                      Gaussian Elimination
        !---------------------------------------------------------------
        do m = v_n+1,1,-1
            do i = m-1,1,-1
                vr(i) = vr(i)-( (va(i,m)*vr(m))/va(m,m) )
                do k = 1,v_n+1
                    va(i,k) = va(i,k) - ( (va(i,m)*va(m,k))/va(m,m) )
                enddo
            enddo
        enddo
        !---------------------------------------------------------------
        !                      Forward Substitution
        !---------------------------------------------------------------
        vx(1:v_n+1) = 0.d0

        vx(1) = vr(1)/va(1,1)

        do i = 2, v_n+1
            t = vr(i)
            do k = 1,i
                t = t - ( va(i,k)*vx(k) )
            enddo
            vx(i) = t/va(i,i)
        enddo
        !---------------------------------------------------------------
        !              Convergence Check & Uphat Update
        !---------------------------------------------------------------
        if (e == 2) then
            lam = 1.d-2
        else
            lam = 1.d0
        endif

        mc = 0
        do j = 1,v_ny
            vl_enth_out(j) = vl_enth(j) + lam*vx(j+mc)
            mc = mc + 1
            vl_u_out(j) = vl_u(j) + lam*vx(j+mc)
            mc = mc + 1
            vl_psi_out(j) = vl_psi(j) + lam*vx(j+mc)
        enddo
        vl_pres_out = vl_pres(e) + lam*vx(v_n+1)

        if (vl_pres_out < 0) then
            vl_pres_prev(1:e-1) = vl_pres(1:e-1)
            vl_pres_prev(e:nx-1) = vl_pres(e)
            goto 2
        endif

        err_out = dabs(vx(v_n+1))
        print *, err_out

        vl_enth(1:v_ny) = vl_enth_out(1:v_ny)
        vl_u(1:v_ny) = vl_u_out(1:v_ny)
        vl_psi(1:v_ny) = vl_psi_out(1:v_ny)
        vl_pres(e) = vl_pres_out

        if (err_out > 1.d-4) goto 1

        vl_enth_save(1:v_ny,e) = vl_enth(1:v_ny)
        vl_u_save(1:v_ny,e) = vl_u(1:v_ny)
        vl_psi_save(1:v_ny,e) = vl_psi(1:v_ny)

	enddo

    pres_sweep_err = maxval(dabs(vl_pres(1:nx) - vl_pres_prev(1:nx)))
    print *, pres_sweep_err
    !if (pres_sweep_err > 1.d-4) then
    if (sweep < 10) then
        vl_pres_prev(1:nx) = vl_pres(1:nx)
        goto 2
    endif

    do i = 1,nx-1
        do j = 1,v_n
            if (i == 1) then
                v_dens(j,i) = (2.d0*gam*c3)/( (gam-1.d0)*(1.d0 - vl_u_save(j,i)**2.d0) )
                v_temp(j,i) = c3/v_dens(j,i)
                v_mu(j,i) = gam*v_temp(j,i)
                if (j == 1) then
                    eta(j) = 0.d0
                elseif (j == v_n) then
                    eta(j) = 1.d0
                else
                    eta(j) = 5.d-1*v_dy*((c3*gam)**(1.d0/2.d0))*( (1/v_dens(j,i)) + (1/v_dens(j-1,i)) )
                endif
                v_v(j,i) = 75.d-2*eta(j)*vl_u_save(j,i) - (25.d-2*((c3*gam)**(1.d0/2.d0))*vl_psi_save(j,i))/v_dens(j,i)
            else
                v_dens(j,i) = (2.d0*gam*c3*vl_pres(i))/((gam-1.d0)*(1.d0 - vl_u_save(j,i)**2.d0))
                v_temp(j,i) = (c3*vl_pres(i))/v_dens(j,i)
                v_mu(j,i) = gam*v_temp(j,i)
                if (j == 1) then
                    eta(j) = 0.d0
                elseif (j == v_n) then
                    eta(j) = 1.d0
                else
                    eta(j) = 5.d-1*v_dy*((c3*gam)**(1.d0/2.d0))*( (1/v_dens(j,i)) + (1/v_dens(j-1,i)) )
                endif
                v_v(j,i) = 75.d-2*eta(j)*vl_u_save(j,i)*vl_del(i)&
                    + (dble(i-1)*dx)*eta(j)*((vl_del(i) - vl_del(i-1))/dx)&
                    - (((gam*c3)**(1.d0/2.d0))/(4.d0*v_dens(i,j)))*( vl_psi_save(j,i)&
                    + (4.d0*(dble(i-1)*dx)*((vl_psi_save(j,i)&
                    - vl_psi_save(j-1,i))/dx)) )
            endif
        enddo
    enddo

    open(unit=v_fid, file='vl_pres.dat', status='new', action='read')
    do i = 1,nx
        write(v_fid,*), vl_pres(i)
    enddo
    close(unit=v_fid)
    v_fid = v_fid + 2

    open(unit=v_fid, file='vl_del.dat', status='new', action='read')
    do i = 1,nx
        write(v_fid,*), vl_del(i)
    enddo
    close(unit=v_fid)
    v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_enth.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), vl_enth_save(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_strv.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), vl_u_save(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_strf.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), vl_psi_save(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_dens.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), v_dens(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_temp.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), v_temp(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_visc.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), v_mu(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    ! open(unit=v_fid, file=vlds//'vl_tranv.dat', status='new', action='read')
    ! do j = 1,v_ny
    !     write(v_fid,*), v_v(j,1:nx)
    ! enddo
    ! close(unit=v_fid)
    ! v_fid = v_fid + 2

    !enddo

    deallocate(vl_ip_enth); deallocate(vl_ip_u); deallocate(vl_ip_psi)
    deallocate(vl_enth); deallocate(vl_u); deallocate(vl_psi)
    deallocate(vl_enth_prev); deallocate(vl_u_prev); deallocate(vl_psi_prev)
    deallocate(va); deallocate(vr); deallocate(vx)
    deallocate(vl_pres); deallocate(vl_del); deallocate(vl_pres_prev)
    deallocate(vl_u_save); deallocate(vl_psi_save)
    deallocate(vl_u_out); deallocate(vl_psi_out)
    deallocate(ramp_func); deallocate(v_dens); deallocate(v_temp)
    deallocate(v_mu); deallocate(v_v); deallocate(eta)
    deallocate(rf_bar); deallocate(vl_enth_out); deallocate(vl_enth_save)
end program