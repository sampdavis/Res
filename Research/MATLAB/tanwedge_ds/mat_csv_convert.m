clear
clc
close all

parent = 'chi20/Pr1/semicircle/convex/1000pts/';
dx1 = 1/50; dx2 = 1/1000;
nvec1 = 0:dx1:0.95-dx1; nvl1 = length(nvec1);
nvec2 = 0.95:dx2:1; nvl2 = length(nvec2);
ff = 'csv_ramps';

% Deformed shapes
%for i = 1:6
i = 6;
    ramp_func1 = defor_geo(51,i/10,(i+3)/10,0.025,1);
    ramp_func2 = defor_geo(1001,i/10,(i+3)/10,0.025,1);
    ramp_func = [ramp_func1(1:nvl1) ramp_func2(end-nvl2+1:end)];
    
    %fil = sprintf('%d',i);
    
    %load(strcat(parent,fil,'/rampf.mat'));
    
    fil_atrib = sprintf('%d%d',i,i+3);
    
    fil_n = strcat(parent,ff,'/coarse_ramp',fil_atrib,'_xl.csv');
    disp(fil_n)
    csvwrite(fil_n,ramp_func)
%end
