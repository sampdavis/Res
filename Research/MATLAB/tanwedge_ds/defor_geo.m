function [wall_geo_y] = defor_geo(pts,s_p,e_p,h,con)

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% clear
% clc
% close all
% 
% pts = 1001;
% %hc = [0.01,1; 0.025,1; 0.05,1]; %0.01,0; 0.025,0; 0.05,0];
% hc = [0.05,1]; hh = 1;
% %con = 1;
% %def_p = [0.1,0.4;0.2,0.5;0.3,0.6;0.4,0.7;0.5,0.8;0.6,0.9];
% def_p = [0.1,0.6;0.2,0.7;0.3,0.8;0.4,0.9];
% %def_p = [0.1,0.9]; jj = 1;
% %for hh = 1:length(hc)
% for jj = 1:length(def_p)
% h = hc(hh,1); con = hc(hh,2);
% s_p = def_p(jj,1); e_p = def_p(jj,2);
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

dp = 1/(pts-1);

wall_geo_y = zeros(1,pts);

a = e_p - s_p; 

radi = (1/(2*h))*(0.25*a*a + h*h);
ry = radi-h;
rx = a/2 + s_p;

xr = radi*sind(-180:0.001:180) + rx;
yr = radi*cosd(-180:0.001:180) - ry;
if con ~= 1
    yr = -yr;
end

def_vec_y = 0; def_vec_x = 0;

di = 1; fr = 1;
for iy = 1:length(yr)
    if con == 1
        if yr(iy) > 0
            if fr == 1
                iyf = iy;
                fr = 0;
            end
            di = di + 1;
            def_vec_y(di) = yr(iy);
            def_vec_x(di) = xr(iy);
        end
    else
        if yr(iy) < 0
            if fr == 1
                iyf = iy;
                fr = 0;
            end
            di = di + 1;
            def_vec_y(di) = yr(iy);
            def_vec_x(di) = xr(iy);
        end
    end
end
def_vec_y(1) = yr(iyf-1); def_vec_x(1) = xr(iyf-1);
def_vec_y(end+1) = yr(iyf+(di-1)); def_vec_x(end+1) = yr(iyf+(di-1));

def_vec_interp = interp1(def_vec_x,def_vec_y,s_p:dp:e_p,'spline');
def_vec_interp(1) = 0; def_vec_interp(end) = 0;

s_p_ind = round(s_p*(pts-1) + 1);
e_p_ind = round(e_p*(pts-1) + 1);

wall_geo_y(s_p_ind:e_p_ind) = def_vec_interp;

%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% figure(1)
% if jj == 1
%     plot(0:dp:1,wall_geo_y,'b','LineWidth',1)
% else
%     plot(0:dp:1,wall_geo_y,'--b','LineWidth',1)
% end
% hold on
% axis([0 1 -0.001 0.051])
% title('Medium Wall Deformation Locations')
% xlabel('x/(e^4 M_i_n_f^4 L)'); ylabel('y/(e^4 M_i_n_f^3 L)')
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% clear
% clc
% close all
% 
% pts = 1001; s_p = 0.1; e_p = 0.9; 
% h = 0.05; con = 1; dp = 1/(pts-1);
% 
% sc_wall = defor_geo(pts,s_p,e_p,h,con);
% si_wall = sine_defor_geo(pts,s_p,e_p,h,con);
% 
% figure(1)
% plot(0:dp:1,sc_wall,'b','LineWidth',1)
% hold on
% plot(0:dp:1,si_wall,'--b','LineWidth',1)
% axis([0 1 -0.001 0.051])
% title('Wall Deformation Shapes')
% xlabel('x/(e^4 M_i_n_f^4 L)'); ylabel('y/(e^4 M_i_n_f^3 L)')
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
end