clear
clc
close all

dx = 1/1000; dy = 1/50;

c3_1 = importdata('ip_data/c3con_pr1adi.dat'); gam = 1.4; c = 1;
il_ip = importdata('ip_data/il_khm_500.dat');
il_p_bl = il_ip(1,3);
c2 = ((c*c3_1)/il_p_bl)^(1/4); c1 = (c*c3_1)/(c2*c2);

load('chi_1/flat/nd_pres.mat'); flat_pres_1 = nd_pres;
load('chi_1/flat/u_bar.mat'); flat_str_vel_1 = vl_u_save;
load('chi_1/flat/v_bar.mat'); flat_tra_vel_1 = vl_v;
load('chi_1/flat/mu_bar.mat'); flat_visc_1 = vl_mu;
load('chi_1/flat/dens_bar.mat'); flat_dens_1 = vl_dens;
flat_wall_1 = zeros(1001,1);
load('chi_1/flat/bl_bar.mat'); flat_bl_1 = vl_del;

c1_1 = c1; c2_1 = c2;

flat_shear_1 = zeros(1001,1);
flat_txx_1 = zeros(1001,1);
flat_txy_1 = zeros(1001,1);
flat_tyy_1 = zeros(1001,1);
flat_pres_grad = zeros(1001,1);

% streamwise velocity averaging
%
%
fsv = flat_str_vel_1(2,:); fsv_1 = fsv(2:2:end); fsv_2 = fsv(3:2:end-2);
fsv_1_interp = interp1(1/1000:1/500:1-(1/1000),fsv_1,1/1000:1/1000:1-(1/1000));
fsv_2_interp = interp1(1/500:1/500:1-(1/500),fsv_2,1/500:1/1000:1-(1/500));
fsv_2_interp = [fsv_1_interp(1), fsv_2_interp, fsv_1_interp(end)];
fsv_average = 0.5.*(fsv_1_interp + fsv_2_interp);
fsv_average = [flat_str_vel_1(2,1),fsv_average,flat_str_vel_1(2,end)];
%
%
%

% transverse velocity averaging
%
%
ftv = flat_tra_vel_1(2,:); ftv_1 = ftv(2:2:end); ftv_2 = ftv(3:2:end-2);
ftv_1_interp = interp1(1/1000:1/500:1-(1/1000),ftv_1,1/1000:1/1000:1-(1/1000));
ftv_2_interp = interp1(1/500:1/500:1-(1/500),ftv_2,1/500:1/1000:1-(1/500));
ftv_2_interp = [ftv_1_interp(1), ftv_2_interp, ftv_1_interp(end)];
ftv_average = 0.5.*(ftv_1_interp + ftv_2_interp);
ftv_average = [flat_tra_vel_1(2,1),ftv_average,flat_tra_vel_1(2,end)];
%
%
%

% figure(100)
% plot(0:dx:1,flat_str_vel_1(2,:))
% hold on
% plot(0:dx:1,fsv_average,'y')
% title('Streamwise velocity 1 point above the plate')
% 
% stop

for i = 2:1000
    %   Pr = 1
    %
    %
    drdxi = (flat_wall_1(i+1) - flat_wall_1(i-1))/(2*dx);
    n1 = -drdxi/((drdxi^2 + 1)^(1/2)); n2 = 1/((drdxi^2 + 1)^(1/2));
    t1 = n2; t2 = -n1;
    
    % Raw
%     dudeta = (flat_dens_1(1,i)/((gam*c3_1)^(1/2)))*(flat_str_vel_1(2,i)/dy);
%     dvdeta = (flat_dens_1(1,i)/((gam*c3_1)^(1/2)))*(flat_tra_vel_1(2,i)/dy);
    % Averaged
    dudeta = (flat_dens_1(1,i)/((gam*c3_1)^(1/2)))*(fsv_average(i)/dy);
    dvdeta = (flat_dens_1(1,i)/((gam*c3_1)^(1/2)))*(ftv_average(i)/dy);

    txx = ((2*flat_visc_1(1,i))/(3*(i-1)*dx))*(...
        -((2*dudeta)/(flat_bl_1(i) - flat_wall_1(i)))*( (i-1)*dx*drdxi + 0.75*flat_wall_1(i) )...
        - (dvdeta/(flat_bl_1(i) - flat_wall_1(i))) );
    
    tyy = ((2*flat_visc_1(1,i))/(3*(i-1)*dx))*(...
        ((2*dvdeta)/(flat_bl_1(i) - flat_wall_1(i)))...
        - (dudeta/(flat_bl_1(i) - flat_wall_1(i)))*( (i-1)*dx*drdxi + 0.75*flat_wall_1(i) ) );
    
    txy = (flat_visc_1(1,i)*dudeta)/(c2_1*(((i-1)*dx)^(0.75))*(flat_bl_1(i) - flat_wall_1(i)));
    
    flat_txx_1(i) = txx; flat_txy_1(i) = txy; flat_tyy_1(i) = tyy;
    
    flat_shear_1(i) = n1*(txx*t1 + txy*t2) + n2*(txy*t1 + tyy*t2);
    %

    %
    flat_pres_grad(i) = (flat_pres_1(i+1) - flat_pres_1(i-1))/(2*dx);
    %
    %
    %

end
flat_shear_1(1) = flat_shear_1(2); flat_shear_1(end) = flat_shear_1(end-1);
flat_txx_1(1) = flat_txx_1(2); flat_txx_1(end) = flat_txx_1(end-1);
flat_txy_1(1) = flat_txy_1(2); flat_txy_1(end) = flat_txy_1(end-1);
flat_tyy_1(1) = flat_tyy_1(2); flat_tyy_1(end) = flat_tyy_1(end-1);
flat_pres_grad(1) = flat_pres_grad(2); flat_pres_grad(end) = flat_pres_grad(end-1);

%   Flat Shear diff and pressure diff plots
%
%
% figure(100)
% plot(0:dx:1,flat_shear_1,'b')
% plot(0:dx:1,flat_txy_1)
% hold on
% plot(0:dx:1,flat_txx_1)
% plot(0:dx:1,flat_tyy_1)
% hold off
% legend('Pr = 1.0')
% axis([0 1 0 2.5])
% title('Flat Plate Shear Stress Profiles')
% xlabel('x/L'); ylabel('t_n_t')
% set(figure(100), 'Position', [100, 100, 1000, 600])
% saveas(figure(100),'chi20/plots/flat_shear_prcomp.jpg')
% %
% figure(200)
% plot(0:dx:1,flat_pres_1,'b')
% % hold on
% % plot(0:dx:1,flat_pres_0725,'r')
% % hold off
% legend('Pr = 1.0')
% axis([0 1 0 2.5])
% title('Flat Plate Pressure Profiles')
% xlabel('x/L'); ylabel('p/p_i_n_f')
% set(figure(200), 'Position', [100, 100, 1000, 600])
% % saveas(figure(200),'chi20/plots/flat_pres_prcomp.jpg')
%
%
%

fid_p = 1; fid_s = 2;

%--------------------------------------------------
%                  File Locations
%--------------------------------------------------
% s counter
sisc{1} = sprintf('semicircle/'); sisc{2} = sprintf('sine/');
% c counter
coco{1} = sprintf('c0/'); coco{2} = sprintf('c1/');
% h counter
hhh{1} = sprintf('h01/'); hhh{2} = sprintf('h025/'); hhh{3} = sprintf('h05/');
% d counter
d1d2{1} = sprintf('d1/'); d1d2{2} = sprintf('d2/');
% dp counter
def_p1 = {'1_4/','2_5/','3_6/','4_7/','5_8/','6_9/'};
def_p2 = {'1_6/','2_7/','3_8/','4_9/','1_9/'};
%--------------------------------------------------
%           Short Length Deformations
%--------------------------------------------------
d = 1;
s = 1; % for s = 1:2
c = 2; %    for c = 1:2
h = 3; %        for h = 1:3
dp = 3; %            for dp = 1:6
    % Pr 1
    %
    %
fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p1{dp}));
% if ~exist(strcat('chi_1/',fina),'dir')
%     continue
% end
shear_stress_1 = zeros(1001,1);
txx_1 = zeros(1001,1);
txy_1 = zeros(1001,1);
tyy_1 = zeros(1001,1);
pres_grad = zeros(1001,1);

try
    load(strcat('chi_1/',fina,'u_bar.mat')); str_vel_1 = vl_u_save;
catch
    continue
end
load(strcat('chi_1/',fina,'nd_pres.mat')); pres_1 = nd_pres;
load(strcat('chi_1/',fina,'v_bar.mat')); tra_vel_1 = vl_v;
load(strcat('chi_1/',fina,'mu_bar.mat')); visc_1 = vl_mu;
load(strcat('chi_1/',fina,'dens_bar.mat')); dens_1 = vl_dens;
load(strcat('chi_1/',fina,'bl_bar.mat')); blayer_1 = vl_del;

if h == 1
    he = 0.01;
elseif h == 2
    he = 0.025;
else
    he = 0.05;
end

if c == 1
    ce = 0;
else
    ce = 1;
end

if s == 1
    ramp_func_1 = defor_geo(1001,dp/10,(dp+3)/10,he,ce);
else
    ramp_func_1 = sine_defor_geo(1001,dp/10,(dp+3)/10,he,ce);
end

% str velocity
%
%
fsv = str_vel_1(2,:); fsv_1 = fsv(2:2:end); fsv_2 = fsv(3:2:end-2);
fsv_1_interp = interp1(1/1000:1/500:1-(1/1000),fsv_1,1/1000:1/1000:1-(1/1000));
fsv_2_interp = interp1(1/500:1/500:1-(1/500),fsv_2,1/500:1/1000:1-(1/500));
fsv_2_interp = [fsv_1_interp(1), fsv_2_interp, fsv_1_interp(end)];
fsv_average = 0.5.*(fsv_1_interp + fsv_2_interp);
fsv_average = [str_vel_1(2,1),fsv_average,str_vel_1(2,end)];
%
%
%

% tra velocity
%
%
ftv = tra_vel_1(2,:); ftv_1 = ftv(2:2:end); ftv_2 = ftv(3:2:end-2);
ftv_1_interp = interp1(1/1000:1/500:1-(1/1000),ftv_1,1/1000:1/1000:1-(1/1000));
ftv_2_interp = interp1(1/500:1/500:1-(1/500),ftv_2,1/500:1/1000:1-(1/500));
ftv_2_interp = [ftv_1_interp(1), ftv_2_interp, ftv_1_interp(end)];
ftv_average = 0.5.*(ftv_1_interp + ftv_2_interp);
ftv_average = [tra_vel_1(2,1),ftv_average,tra_vel_1(2,end)];
%
%
%
    
    for i = 2:1000
        
        %	Pr = 1
        %   Shear
        %
        drdxi = (ramp_func_1(i+1) - ramp_func_1(i-1))/(2*dx);
        n1 = -drdxi/((drdxi^2 + 1)^(1/2)); n2 = 1/((drdxi^2 + 1)^(1/2));
        t1 = n2; t2 = -n1;

        % Raw
%         dudeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(str_vel_1(2,i)/dy);
%         dvdeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(tra_vel_1(2,i)/dy);
        % Averaged
        dudeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(fsv_average(i)/dy);
        dvdeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(ftv_average(i)/dy);
    
        txx = ((2*visc_1(1,i))/(3*(i-1)*dx))*(...
            -((2*dudeta)/(blayer_1(i) - ramp_func_1(i)))*( (i-1)*dx*drdxi + 0.75*ramp_func_1(i) )...
            - (dvdeta/(blayer_1(i) - ramp_func_1(i))) );
    
        tyy = ((2*visc_1(1,i))/(3*(i-1)*dx))*(...
            ((2*dvdeta)/(blayer_1(i) - ramp_func_1(i)))...
            - (dudeta/(blayer_1(i) - ramp_func_1(i)))*( (i-1)*dx*drdxi + 0.75*ramp_func_1(i) ) );
    
        txy = (visc_1(1,i)*dudeta)/(c2_1*(((i-1)*dx)^(0.75))*(blayer_1(i) - ramp_func_1(i)));
        
        txx_1(i) = txx; txy_1(i) = txy; tyy_1(i) = tyy;
    
        shear_stress_1(i) = n1*(txx*t1 + txy*t2) + n2*(txy*t1 + tyy*t2);
        %

        %
        pres_grad(i) = (pres_1(i+1) - pres_1(i-1))/(2*dx);
        %
        %
        %
    end
shear_stress_1(1) = shear_stress_1(2); shear_stress_1(end) = shear_stress_1(end-1);
txx_1(1) = txx_1(2); txx_1(end) = txx_1(end-1);
txy_1(1) = txy_1(2); txy_1(end) = txy_1(end-1);
tyy_1(1) = tyy_1(2); tyy_1(end) = tyy_1(end-1);
pres_grad(1) = pres_grad(2); pres_grad(end) = pres_grad(end-1);

if c == 1
    c_str = 'Concave';
    c_str2 = 'c0';
else
    c_str = 'Convex';
    c_str2 = 'c1';
end
if s == 1
    s_str = 'semicircle';
    s_str2 = 'sc';
else
    s_str = 'sine';
    s_str2 = 'si';
end
if h == 1
    h_str = '1% wall length';
    h_str2 = 'h01';
elseif h == 2
    h_str = '2.5% wall length';
    h_str2 = 'h025';
else
    h_str = '5% wall length';
    h_str2 = 'h05';
end

% Shear plots
%
%
figure(fid_s)
plot(0:(1/1000):1,flat_shear_1,'--k','LineWidth',1)
hold on
grid on
plot(0:(1/1000):1,shear_stress_1,'b','LineWidth',1)
plot([dp/10 dp/10], ylim, '-.k','LineWidth',1)
plot([(dp+3)/10 (dp+3)/10], ylim, '-.k','LineWidth',1)
axis([0 1 0 2])
legend('Flat Plate Shear','Deformed Wall Shear')
title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
xlabel('x/L'); ylabel('Shear')
set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% saveas(figure(fid_s),sprintf(strcat('plots/shear_plots/wall_shear%d_',c_str2,s_str2,h_str2,'_%d.jpg'),d,dp))
%
fid_s = fid_s + 1;
%
figure(fid_s)
plot(0:dx:1,flat_pres_1,'--k','LineWidth',1,'HandleVisibility','off')
hold on
grid on
plot(0:dx:1,flat_pres_grad,'--k','LineWidth',1,'HandleVisibility','off')
plot(0:dx:1,nd_pres,'LineWidth',1)
plot(0:dx:1,pres_grad,'LineWidth',1)
plot([dp/10 dp/10], ylim, '-.k','LineWidth',1)
plot([(dp+3)/10 (dp+3)/10], ylim, '-.k','LineWidth',1)
axis([0 1 -10 10])
legend('Pressure','Pressure Gradient')
title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
xlabel('x/L'); ylabel('Pressure / Pressure Gradient')
set(figure(fid_s), 'Position', [100, 100, 1000, 600])
%
fid_s = fid_s + 1;
%
%
% figure(fid_s)
% plot(0:1/1000:1,txx_1,'LineWidth',1)
% hold on
% plot(0:1/1000:1,txy_1,'LineWidth',1)
% plot(0:1/1000:1,tyy_1,'LineWidth',1)
% plot([dp/10 dp/10], ylim, '-.k','LineWidth',1)
% plot([(dp+3)/10 (dp+3)/10], ylim, '-.k','LineWidth',1)
% axis([0 1 -0.5 2])
% legend('t_x_x','t_x_y','t_y_y')
% title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% % saveas(figure(fid_s),sprintf(strcat('plots/shear_plots/shear_comps%d_',c_str2,s_str2,h_str2,'_%d.jpg'),d,dp))
% %
% fid_s = fid_s + 1;

%            end
%        end
%    end
% end
% %--------------------------------------------------
% %           Medium Length Deformations
% %--------------------------------------------------
% d = 2;
% for s = 1:2
%     for c = 1:2
%         for h = 1:3
%             for dp = 1:5
%     % Pr 1
%     %
%     %
% fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p2{dp}));
% if ~exist(strcat('chi_1/',fina),'dir')
%     continue
% end
% shear_stress_1 = zeros(1001,1);
% txx_1 = zeros(1001,1);
% txy_1 = zeros(1001,1);
% tyy_1 = zeros(1001,1);
% 
% try
%     load(strcat('chi_1/',fina,'u_bar.mat')); str_vel_1 = vl_u_save;
% catch
%     continue
% end
% load(strcat('chi_1',fina,'nd_pres.mat')); pres_1 = nd_pres;
% load(strcat('chi_1/',fina,'v_bar.mat')); tra_vel_1 = vl_v;
% load(strcat('chi_1/',fina,'mu_bar.mat')); visc_1 = vl_mu;
% load(strcat('chi_1/',fina,'dens_bar.mat')); dens_1 = vl_dens;
% load(strcat('chi_1/',fina,'bl_bar.mat')); blayer_1 = vl_del;
% 
% if h == 1
%     he = 0.01;
% elseif h == 2
%     he = 0.025;
% else
%     he = 0.05;
% end
% 
% if c == 1
%     ce = 0;
% else
%     ce = 1;
% end
% 
% if s == 1
%     if dp == 5
%         ramp_func_1 = defor_geo(1001,1/10,9/10,he,ce);
%     else
%         ramp_func_1 = defor_geo(1001,dp/10,(dp+5)/10,he,ce);
%     end
% else
% 	if dp == 5
%         ramp_func_1 = sine_defor_geo(1001,1/10,9/10,he,ce);
% 	else
%         ramp_func_1 = sine_defor_geo(1001,dp/10,(dp+5)/10,he,ce);
% 	end
% end
% 
% % str velocity
% %
% %
% fsv = str_vel_1(2,:); fsv_1 = fsv(2:2:end); fsv_2 = fsv(3:2:end-2);
% fsv_1_interp = interp1(1/1000:1/500:1-(1/1000),fsv_1,1/1000:1/1000:1-(1/1000));
% fsv_2_interp = interp1(1/500:1/500:1-(1/500),fsv_2,1/500:1/1000:1-(1/500));
% fsv_2_interp = [fsv_1_interp(1), fsv_2_interp, fsv_1_interp(end)];
% fsv_average = 0.5.*(fsv_1_interp + fsv_2_interp);
% fsv_average = [str_vel_1(2,1),fsv_average,str_vel_1(2,end)];
% 
% % tra velocity
% %
% %
% ftv = tra_vel_1(2,:); ftv_1 = ftv(2:2:end); ftv_2 = ftv(3:2:end-2);
% ftv_1_interp = interp1(1/1000:1/500:1-(1/1000),ftv_1,1/1000:1/1000:1-(1/1000));
% ftv_2_interp = interp1(1/500:1/500:1-(1/500),ftv_2,1/500:1/1000:1-(1/500));
% ftv_2_interp = [ftv_1_interp(1), ftv_2_interp, ftv_1_interp(end)];
% ftv_average = 0.5.*(ftv_1_interp + ftv_2_interp);
% ftv_average = [tra_vel_1(2,1),ftv_average,tra_vel_1(2,end)];
% 
%     for i = 2:1000
%         
%         %	Pr = 1
%         %
%         %
%         drdxi = (ramp_func_1(i+1) - ramp_func_1(i-1))/(2*dx);
%         n1 = -drdxi/((drdxi^2 + 1)^(1/2)); n2 = 1/((drdxi^2 + 1)^(1/2));
%         t1 = n2; t2 = -n1;
%     
% %         dudeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(str_vel_1(2,i)/dy);
% %         dvdeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(tra_vel_1(2,i)/dy);
%         dudeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(fsv_average(i)/dy);
%         dvdeta = (dens_1(1,i)/((gam*c3_1)^(1/2)))*(ftv_average(i)/dy);
%     
%         txx = ((2*visc_1(1,i))/(3*(i-1)*dx))*(...
%             -((2*dudeta)/(blayer_1(i) - ramp_func_1(i)))*( (i-1)*dx*drdxi + 0.75*ramp_func_1(i) )...
%             - (dvdeta/(blayer_1(i) - ramp_func_1(i))) );
%     
%         tyy = ((2*visc_1(1,i))/(3*(i-1)*dx))*(...
%             ((2*dvdeta)/(blayer_1(i) - ramp_func_1(i)))...
%             - (dudeta/(blayer_1(i) - ramp_func_1(i)))*( (i-1)*dx*drdxi + 0.75*ramp_func_1(i) ) );
%     
%         txy = (visc_1(1,i)*dudeta)/(c2_1*(((i-1)*dx)^(0.75))*(blayer_1(i) - ramp_func_1(i)));
%         
%         txx_1(i) = txx; txy_1(i) = txy; tyy_1(i) = tyy;
%     
%         shear_stress_1(i) = n1*(txx*t1 + txy*t2) + n2*(txy*t1 + tyy*t2);
%     end
%     
% shear_stress_1(1) = shear_stress_1(2); shear_stress_1(end) = shear_stress_1(end-1);
% txx_1(1) = txx_1(2); txx_1(end) = txx_1(end-1);
% txy_1(1) = txy_1(2); txy_1(end) = txy_1(end-1);
% tyy_1(1) = tyy_1(2); tyy_1(end) = tyy_1(end-1);
% 
% if c == 1
%     c_str = 'Concave';
%     c_str2 = 'c0';
% else
%     c_str = 'Convex';
%     c_str2 = 'c1';
% end
% if s == 1
%     s_str = 'semicircle';
%     s_str2 = 'sc';
% else
%     s_str = 'sine';
%     s_str2 = 'si';
% end
% if h == 1
%     h_str = '1% wall length';
%     h_str2 = 'h01';
% elseif h == 2
%     h_str = '2.5% wall length';
%     h_str2 = 'h025';
% else
%     h_str = '5% wall length';
%     h_str2 = 'h05';
% end
% 
% % Shear plots
% %
% %
% figure(fid_s)
% plot(0:(1/1000):1,flat_shear_1,'--k','LineWidth',1)
% hold on
% grid on
% plot(0:(1/1000):1,shear_stress_1,'b','LineWidth',1)
% if dp == 5
%     plot([1/10 1/10], ylim, '-.k','LineWidth',1)
%     plot([9/10 9/10], ylim, '-.k','LineWidth',1)
% else
%     plot([dp/10 dp/10], ylim, '-.k','LineWidth',1)
%     plot([(dp+5)/10 (dp+5)/10], ylim, '-.k','LineWidth',1)
% end
% axis([0 1 0 2])
% legend('Flat Plate Stress','Deformed Wall Stress')
% title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% xlabel('x/L'); ylabel('Shear')
% set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% % saveas(figure(fid_s),sprintf(strcat('plots/shear_plots/wall_shear%d_',c_str2,s_str2,h_str2,'_%d.jpg'),d,dp))
% %
% fid_s = fid_s + 1;
% %
% % figure(fid_s)
% % plot(0:1/1000:1,txx_1,'LineWidth',1)
% % hold on
% % plot(0:1/1000:1,txy_1,'LineWidth',1)
% % plot(0:1/1000:1,tyy_1,'LineWidth',1)
% % if dp == 5
% %     plot([1/10 1/10], ylim, '-.k','LineWidth',1)
% %     plot([9/10 9/10], ylim, '-.k','LineWidth',1)
% % else
% %     plot([dp/10 dp/10], ylim, '-.k','LineWidth',1)
% %     plot([(dp+5)/10 (dp+5)/10], ylim, '-.k','LineWidth',1)
% % end
% % axis([0 1 -0.5 2])
% % legend('t_x_x','t_x_y','t_y_y')
% % title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% % set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% % % saveas(figure(fid_s),sprintf(strcat('plots/shear_plots/shear_comps%d_',c_str2,s_str2,h_str2,'_%d.jpg'),d,dp))
% % 
% % 
% % fid_s = fid_s + 1;
% 
%             end
%         end
%     end
% end