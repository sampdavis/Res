clear
clc
close all

%Ward's Data
load('datadump/Ward_data/CHI_20/0_deg/H-U-PSI_1')
load('datadump/Ward_data/CHI_20/0_deg/H-U-PSI_251')
load('datadump/Ward_data/CHI_20/0_deg/H-U-PSI_501')
load('datadump/Ward_data/CHI_20/0_deg/H-U-PSI_751')
load('datadump/Ward_data/CHI_20/0_deg/H-U-PSI_998')
load('datadump/Ward_data/CHI_20/0_deg/PRESSURE-OUT_998_0')

%My data
load('chi20/Pr1/semicircle/convex/1000pts/flat/u_bar.mat')
load('chi20/Pr1/semicircle/convex/1000pts/flat/sf_bar.mat')
load('chi20/Pr1/semicircle/convex/1000pts/flat/p_bar.mat')

%Pressure Plots
figure(1); plot(0:1/1000:1,PRESSURE_OUT_998_0(:,2)); hold on; plot(0:1/1000:1,vl_pres)
legend('Ward','Sampson'); xlabel('x/L'); ylabel('p_b_a_r'); title('p_b_a_r plots')
set(figure(1), 'Position', [100, 100, 1000, 600])

% %Velocity Plots
% figure(3); plot(H_U_PSI_1(:,2),0:1/999:1); hold on; plot(vl_u_save(:,1),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamwise velocity plot at x/L = 0')
% set(figure(3), 'Position', [100, 100, 1000, 600])
% saveas(figure(3),'plots/data_compare_plots/chi2_str_vel_1.jpg')
% %
% figure(4); plot(H_U_PSI_251(:,2),0:1/999:1); hold on; plot(vl_u_save(:,251),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamwise velocity plots at x/L = 0.25')
% set(figure(4), 'Position', [100, 100, 1000, 600])
% saveas(figure(4),'plots/data_compare_plots/chi2_str_vel_2.jpg')
% %
% figure(5); plot(H_U_PSI_501(:,2),0:1/999:1); hold on; plot(vl_u_save(:,501),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamwise velocity plots at x/L = 0.5')
% set(figure(5), 'Position', [100, 100, 1000, 600])
% saveas(figure(5),'plots/data_compare_plots/chi2_str_vel_3.jpg')
% %
% figure(6); plot(H_U_PSI_751(:,2),0:1/999:1); hold on; plot(vl_u_save(:,751),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamwise velocity plots at x/L = 0.75')
% set(figure(6), 'Position', [100, 100, 1000, 600])
% saveas(figure(6),'plots/data_compare_plots/chi2_str_vel_4.jpg')
% %
% figure(7); plot(H_U_PSI_998(:,2),0:1/999:1); hold on; plot(vl_u_save(:,1000),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamwise velocity plots at x/L = 0.99')
% set(figure(7), 'Position', [100, 100, 1000, 600])
% saveas(figure(7),'plots/data_compare_plots/chi2_str_vel_5.jpg')
% %
% 
% %Streamfunction plots
% figure(8); plot(H_U_PSI_1(:,3),0:1/999:1); hold on; plot(vl_psi_save(:,1),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamfunction plots at x/L = 0')
% set(figure(8), 'Position', [100, 100, 1000, 600])
% saveas(figure(8),'plots/data_compare_plots/chi2_str_func_1.jpg')
% %
% figure(9); plot(H_U_PSI_251(:,3),0:1/999:1); hold on; plot(vl_psi_save(:,251),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamfunction plots at x/L = 0.25')
% set(figure(9), 'Position', [100, 100, 1000, 600])
% saveas(figure(9),'plots/data_compare_plots/chi2_str_func_2.jpg')
% %
% figure(10); plot(H_U_PSI_501(:,3),0:1/999:1); hold on; plot(vl_psi_save(:,501),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamfunction plots at x/L = 0.5')
% set(figure(10), 'Position', [100, 100, 1000, 600])
% saveas(figure(10),'plots/data_compare_plots/chi2_str_func_3.jpg')
% %
% figure(11); plot(H_U_PSI_751(:,3),0:1/999:1); hold on; plot(vl_psi_save(:,751),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamfunction plots at x/L = 0.75')
% set(figure(11), 'Position', [100, 100, 1000, 600])
% saveas(figure(11),'plots/data_compare_plots/chi2_str_func_4.jpg')
% %
% figure(12); plot(H_U_PSI_998(:,3),0:1/999:1); hold on; plot(vl_psi_save(:,1000),0:1/500:1)
% legend('Ward','Sampson'); xlabel('x/L'); ylabel('u/U_i_n_f'); title('Streamfunction plots at x/L = 0.99')
% set(figure(12), 'Position', [100, 100, 1000, 600])
% saveas(figure(12),'plots/data_compare_plots/chi2_str_func_5.jpg')
% %

