clear
clc
close all
%--------------------------------------------------
%                   Initial Data
%--------------------------------------------------
c3 = importdata('ip_data\c3con_pr1adi.dat');
il_ip = importdata('ip_data\il_khm_500.dat');
il_p_bl = il_ip(1,3);
c = 1; gam = 1.4;
v_ny = 501; nx = 1001; v_dy = 1/50; dx = 1/1000;
c2 = ((c*c3)/il_p_bl)^(1/4); c1 = (c*c3)/(c2*c2);

%--------------------------------------------------
%--------------------------------------------------
%                    Flat Case
%--------------------------------------------------
load(strcat('chi_1/flat/bl_bar.mat'))
load(strcat('chi_1/flat/p_bar.mat'))
load(strcat('chi_1/flat/u_bar.mat'))
load(strcat('chi_1/flat/sf_bar.mat'))
%--------------------------------------------------
%               Data Calculation
%--------------------------------------------------
vl_dens = zeros(v_ny,nx);
vl_temp = zeros(v_ny,nx);
vl_mu = zeros(v_ny,nx);
vl_v = zeros(v_ny,nx);
eta = zeros(v_ny,nx);

vl_dens(:,1) = (2*gam*c3)./((gam-1).*(1 - vl_u_save(:,1).^2));
vl_temp(:,1) = c3./vl_dens(:,1);
vl_mu(:,1) = gam.*vl_temp(:,1);
eta(v_ny,:) = 1;

for j = 2:v_ny
	eta(j,1) = eta(j-1,1) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,1))+(1/vl_dens(j-1,1))));
end
vl_v(:,1) = 0.75.*eta(:,1).*vl_u_save(:,1) - (0.25*sqrt(gam*c3).*vl_psi_save(:,1))./vl_dens(:,1);
for i = 2:nx-1
	vl_dens(:,i) = (2*gam*c3*vl_pres(i))./((gam-1).*(1 - vl_u_save(:,i).^2));
	vl_temp(:,i) = (c3*vl_pres(i))./vl_dens(:,i);
	vl_mu(:,i) = gam.*vl_temp(:,i);
	for j = 2:v_ny
        eta(j,i) = eta(j-1,i) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,i))+(1/vl_dens(j-1,i))));
	end
	vl_v(:,i) = 0.75.*eta(:,i).*vl_u_save(:,i).*vl_del(i)...
        + ((i-1)*dx).*eta(:,i).*vl_u_save(:,i).*((vl_del(i) - vl_del(i-1))/dx)...
        - (sqrt(gam*c3)./(4.*vl_dens(:,i))).*( vl_psi_save(:,i) + (4*((i-1)*dx).*((vl_psi_save(:,i) - vl_psi_save(:,i-1))./dx)) );
end
save(strcat('chi_1/flat/dens_bar.mat'),'vl_dens');
save(strcat('chi_1/flat/temp_bar.mat'),'vl_temp');
save(strcat('chi_1/flat/mu_bar.mat'),'vl_mu');
save(strcat('chi_1/flat/v_bar.mat'),'vl_v');
save(strcat('chi_1/flat/eta.mat'),'eta');
%--------------------------------------------------
%               Sim Rep 2 Non Dim
%--------------------------------------------------
nd_u = zeros(v_ny,nx);
nd_dens = zeros(v_ny,nx);
nd_temp = zeros(v_ny,nx);
nd_mu = zeros(v_ny,nx);
nd_v = zeros(v_ny,nx);
nd_pres = zeros(nx,1);
nd_bl = zeros(nx,1);

for i = 1:nx
	xi = (i-1)*dx;
	nd_u(:,i) = vl_u_save(:,i);
	nd_dens(:,i) = (1/(c2*c2*(xi^(0.5)))).*vl_dens(:,i);
	nd_temp(:,i) = vl_temp(:,i);
	nd_mu(:,i) = vl_mu(:,i);
	nd_v(:,i) = (c2/(xi^(0.25))).*vl_v(:,i);
	nd_pres(i) = (c1/(xi^(0.5)))*vl_pres(i);
	nd_bl(i) = (c2*(xi^(0.75)))*vl_del(i);
end
                        
save(strcat('chi_1/flat/nd_u.mat'),'nd_u');
save(strcat('chi_1/flat/nd_dens.mat'),'nd_dens');
save(strcat('chi_1/flat/nd_temp.mat'),'nd_temp');
save(strcat('chi_1/flat/nd_mu.mat'),'nd_mu');
save(strcat('chi_1/flat/nd_v.mat'),'nd_v');
save(strcat('chi_1/flat/nd_pres.mat'),'nd_pres');
save(strcat('chi_1/flat/nd_bl.mat'),'nd_bl');

%--------------------------------------------------
%--------------------------------------------------
%                  File Locations
%--------------------------------------------------
% s counter
sisc{1} = sprintf('semicircle/'); sisc{2} = sprintf('sine/');
% c counter;    c1 = convex, c0 = concave
coco{1} = sprintf('c0/'); coco{2} = sprintf('c1/');
% h counter;    h01 = 1% , h025 = 2.5%, h05 = 5%
hhh{1} = sprintf('h01/'); hhh{2} = sprintf('h025/'); hhh{3} = sprintf('h05/');
% d counter
d1d2{1} = sprintf('d1/'); d1d2{2} = sprintf('d2/');
% dp counter
def_p1 = {'1_4/','2_5/','3_6/','4_7/','5_8/','6_9/'};
def_p2 = {'1_6/','2_7/','3_8/','4_9/','1_9/'};
%--------------------------------------------------
%           Short Length Deformations
%--------------------------------------------------
d = 1;
for s = 1:2
    for c = 1:2
        for h = 1:3
            for dp = 1:6
%--------------------------------------------------
            fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p1{dp}));
            if ~exist(strcat('chi_1/',fina),'dir')
                continue
            end
            try
                load(strcat('chi_1/',fina,'bl_bar.mat'))
            catch
                continue
            end
            load(strcat('chi_1/',fina,'p_bar.mat'))
            load(strcat('chi_1/',fina,'u_bar.mat'))
            load(strcat('chi_1/',fina,'sf_bar.mat'))
%--------------------------------------------------
%               Data Calculation
%--------------------------------------------------
            vl_dens = zeros(v_ny,nx);
            vl_temp = zeros(v_ny,nx);
            vl_mu = zeros(v_ny,nx);
            vl_v = zeros(v_ny,nx);
            eta = zeros(v_ny,nx);

            vl_dens(:,1) = (2*gam*c3)./((gam-1).*(1 - vl_u_save(:,1).^2));
            vl_temp(:,1) = c3./vl_dens(:,1);
            vl_mu(:,1) = gam.*vl_temp(:,1);
            eta(v_ny,:) = 1;

            for j = 2:v_ny
                eta(j,1) = eta(j-1,1) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,1))+(1/vl_dens(j-1,1))));
            end
            vl_v(:,1) = 0.75.*eta(:,1).*vl_u_save(:,1) - (0.25*sqrt(gam*c3).*vl_psi_save(:,1))./vl_dens(:,1);
            for i = 2:nx-1
                vl_dens(:,i) = (2*gam*c3*vl_pres(i))./((gam-1).*(1 - vl_u_save(:,i).^2));
                vl_temp(:,i) = (c3*vl_pres(i))./vl_dens(:,i);
                vl_mu(:,i) = gam.*vl_temp(:,i);
                for j = 2:v_ny
                    eta(j,i) = eta(j-1,i) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,i))+(1/vl_dens(j-1,i))));
                end
                vl_v(:,i) = 0.75.*eta(:,i).*vl_u_save(:,i).*vl_del(i)...
                    + ((i-1)*dx).*eta(:,i).*vl_u_save(:,i).*((vl_del(i) - vl_del(i-1))/dx)...
                    - (sqrt(gam*c3)./(4.*vl_dens(:,i))).*( vl_psi_save(:,i) + (4*((i-1)*dx).*((vl_psi_save(:,i) - vl_psi_save(:,i-1))./dx)) );
            end
            save(strcat('chi_1/',fina,'dens_bar.mat'),'vl_dens');
            save(strcat('chi_1/',fina,'temp_bar.mat'),'vl_temp');
            save(strcat('chi_1/',fina,'mu_bar.mat'),'vl_mu');
            save(strcat('chi_1/',fina,'v_bar.mat'),'vl_v');
            save(strcat('chi_1/',fina,'eta.mat'),'eta');
%--------------------------------------------------
%               Sim Rep 2 Non Dim
%--------------------------------------------------
            nd_u = zeros(v_ny,nx);
            nd_dens = zeros(v_ny,nx);
            nd_temp = zeros(v_ny,nx);
            nd_mu = zeros(v_ny,nx);
            nd_v = zeros(v_ny,nx);
            nd_pres = zeros(nx,1);
            nd_bl = zeros(nx,1);

            for i = 1:nx
                xi = (i-1)*dx;
                nd_u(:,i) = vl_u_save(:,i);
                nd_dens(:,i) = (1/(c2*c2*(xi^(0.5)))).*vl_dens(:,i);
                nd_temp(:,i) = vl_temp(:,i);
                nd_mu(:,i) = vl_mu(:,i);
                nd_v(:,i) = (c2/(xi^(0.25))).*vl_v(:,i);
                nd_pres(i) = (c1/(xi^(0.5)))*vl_pres(i);
                nd_bl(i) = (c2*(xi^(0.75)))*vl_del(i);
            end
                        
            save(strcat('chi_1/',fina,'nd_u.mat'),'nd_u');
            save(strcat('chi_1/',fina,'nd_dens.mat'),'nd_dens');
            save(strcat('chi_1/',fina,'nd_temp.mat'),'nd_temp');
            save(strcat('chi_1/',fina,'nd_mu.mat'),'nd_mu');
            save(strcat('chi_1/',fina,'nd_v.mat'),'nd_v');
            save(strcat('chi_1/',fina,'nd_pres.mat'),'nd_pres');
            save(strcat('chi_1/',fina,'nd_bl.mat'),'nd_bl');

%--------------------------------------------------
            end
        end
    end
end
%--------------------------------------------------
%         Medium Length Deformations
%--------------------------------------------------
d = 2;
for s = 1:2
    for c = 1:2
        for h = 1:3
            for dp = 1:5
%--------------------------------------------------
            fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p2{dp}));
            if ~exist(strcat('chi_1/',fina),'dir')
                continue
            end
            try
                load(strcat('chi_1/',fina,'bl_bar.mat'))
            catch
                continue
            end
            load(strcat('chi_1/',fina,'p_bar.mat'))
            load(strcat('chi_1/',fina,'u_bar.mat'))
            load(strcat('chi_1/',fina,'sf_bar.mat'))
%--------------------------------------------------
%               Data Calculation
%--------------------------------------------------
            vl_dens = zeros(v_ny,nx);
            vl_temp = zeros(v_ny,nx);
            vl_mu = zeros(v_ny,nx);
            vl_v = zeros(v_ny,nx);
            eta = zeros(v_ny,nx);

            vl_dens(:,1) = (2*gam*c3)./((gam-1).*(1 - vl_u_save(:,1).^2));
            vl_temp(:,1) = c3./vl_dens(:,1);
            vl_mu(:,1) = gam.*vl_temp(:,1);
            eta(v_ny,:) = 1;

            for j = 2:v_ny
                eta(j,1) = eta(j-1,1) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,1))+(1/vl_dens(j-1,1))));
            end
            vl_v(:,1) = 0.75.*eta(:,1).*vl_u_save(:,1) - (0.25*sqrt(gam*c3).*vl_psi_save(:,1))./vl_dens(:,1);
            for i = 2:nx-1
                vl_dens(:,i) = (2*gam*c3*vl_pres(i))./((gam-1).*(1 - vl_u_save(:,i).^2));
                vl_temp(:,i) = (c3*vl_pres(i))./vl_dens(:,i);
                vl_mu(:,i) = gam.*vl_temp(:,i);
                for j = 2:v_ny
                    eta(j,i) = eta(j-1,i) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,i))+(1/vl_dens(j-1,i))));
                end
                vl_v(:,i) = 0.75.*eta(:,i).*vl_u_save(:,i).*vl_del(i)...
                    + ((i-1)*dx).*eta(:,i).*vl_u_save(:,i).*((vl_del(i) - vl_del(i-1))/dx)...
                    - (sqrt(gam*c3)./(4.*vl_dens(:,i))).*( vl_psi_save(:,i) + (4*((i-1)*dx).*((vl_psi_save(:,i) - vl_psi_save(:,i-1))./dx)) );
            end
            save(strcat('chi_1/',fina,'dens_bar.mat'),'vl_dens');
            save(strcat('chi_1/',fina,'temp_bar.mat'),'vl_temp');
            save(strcat('chi_1/',fina,'mu_bar.mat'),'vl_mu');
            save(strcat('chi_1/',fina,'v_bar.mat'),'vl_v');
            save(strcat('chi_1/',fina,'eta.mat'),'eta');
%--------------------------------------------------
%               Sim Rep 2 Non Dim
%--------------------------------------------------
            nd_u = zeros(v_ny,nx);
            nd_dens = zeros(v_ny,nx);
            nd_temp = zeros(v_ny,nx);
            nd_mu = zeros(v_ny,nx);
            nd_v = zeros(v_ny,nx);
            nd_pres = zeros(nx,1);
            nd_bl = zeros(nx,1);

            for i = 1:nx
                xi = (i-1)*dx;
                nd_u(:,i) = vl_u_save(:,i);
                nd_dens(:,i) = (1/(c2*c2*(xi^(0.5)))).*vl_dens(:,i);
                nd_temp(:,i) = vl_temp(:,i);
                nd_mu(:,i) = vl_mu(:,i);
                nd_v(:,i) = (c2/(xi^(0.25))).*vl_v(:,i);
                nd_pres(i) = (c1/(xi^(0.5)))*vl_pres(i);
                nd_bl(i) = (c2*(xi^(0.75)))*vl_del(i);
            end
            
            save(strcat('chi_1/',fina,'nd_u.mat'),'nd_u');
            save(strcat('chi_1/',fina,'nd_dens.mat'),'nd_dens');
            save(strcat('chi_1/',fina,'nd_temp.mat'),'nd_temp');
            save(strcat('chi_1/',fina,'nd_mu.mat'),'nd_mu');
            save(strcat('chi_1/',fina,'nd_v.mat'),'nd_v');
            save(strcat('chi_1/',fina,'nd_pres.mat'),'nd_pres');
            save(strcat('chi_1/',fina,'nd_bl.mat'),'nd_bl');
%--------------------------------------------------
            end
        end
    end
end