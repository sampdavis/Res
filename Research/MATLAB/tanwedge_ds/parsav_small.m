function parsav_small(fie,vl_pres,vl_del,vl_u_save,vl_psi_save)
%%%

save(strcat(fie,'u_bar.mat'),'vl_u_save')
save(strcat(fie,'sf_bar.mat'),'vl_psi_save')
save(strcat(fie,'p_bar.mat'),'vl_pres')
save(strcat(fie,'bl_bar.mat'),'vl_del')

end