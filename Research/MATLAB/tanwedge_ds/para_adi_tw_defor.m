clear
clc
close all

Pr = 1.d0; gam = 14.d-1; c = 1.d0;
v_ny = 501; v_n = 2*v_ny; v_dy = 10/(v_ny-1);
nx1 = 51; dx1 = 1/(nx1-1);
nx2 = 1001; dx2 = 1/(nx2-1);
nvec1 = 0:dx1:0.95-dx1;
nvl1 = length(nvec1);
nvec2 = 0.95:dx2:1;
nvl2 = length(nvec2);
nvec = [nvec1 nvec2];
nvl = length(nvec);

c3 = importdata('ip_data\c3con_pr1adi.dat');
vl_ip = importdata('ip_data\vl_pr1adi.dat');
vl_ip_enth = vl_ip(:,1);
vl_ip_u = vl_ip(:,2);
vl_ip_psi = vl_ip(:,3);
il_ip = importdata('ip_data\il_khm_500.dat');
il_p_bl = il_ip(1,3);

c2 = ((c*c3)/il_p_bl)^(1/4); c1 = (c*c3)/(c2*c2);
b = 1;

chi_vec = 1:0.5:4;
con = 1; h = 0.025;
def_points = [0.1,0.4; 0.2,0.5; 0.3,0.6; 0.4,0.7; 0.5,0.8; 0.6,0.9];

ij = 0;
for ii = 1:length(chi_vec)
    for jj = 1:length(def_points)
        ij = ij + 1;
        param(ij,1) = chi_vec(ii);
        param(ij,2) = def_points(jj,1);
        param(ij,3) = def_points(jj,2);
    end
end

parfor se = 1:ij

    params = param(se,:);
    
chi = params(1);
con1 = con; h1 = h; 
s_p1 = params(2); e_p1 = params(3);
    
%----BL
vl_del = zeros(nvl,1);
vl_del(1) = 1;

%----Pressure
vl_pres = zeros(nvl,1);
vl_pres(1:end-1) = 1;
vl_pres(end) = 1/(c1*gam*chi*chi*chi);
vl_pres_prev = vl_pres;

%----Ramp function
ramp_func1 = defor_geo(nx1,s_p1,e_p1,h1,con1);
ramp_func2 = defor_geo(nx2,s_p1,e_p1,h1,con1);

rf_b2 = ramp_func2;
rf_b2(2:end) = ramp_func2(2:end)./(c2.*((dx2:dx2:1).^(3/4)));

rf_b1 = ramp_func1;
rf_b1(2:end) = ramp_func1(2:end)./(c2.*((dx1:dx1:1).^(3/4)));

ramp_func = [ramp_func1(1:nvl1) ramp_func2(end-nvl2+1:end)];
rf_b = [rf_b1(1:nvl1) rf_b2(end-nvl2+1:end)];

%----Saved layer profiles
vl_u_save = zeros(v_ny,nx2);
vl_psi_save = zeros(v_ny,nx2);

pres_con = 100; sweep = 0; br = 0; incres = 0;

while pres_con > 1e-8
    sweep = sweep + 1;
    vl_pres = vl_pres_prev;
    
    if pres_con < 1e-2 && incres == 0

        incres = 1;
        
        nx = 1001;
        dx = 1/(nx-1);
        
        %----BL
        vl_del = zeros(nx,1);
        vl_del(1) = 1;
        
        %----Ramp function
        ramp_func = defor_geo(nx,s_p1,e_p1,h1,con1);
        rf_b = ramp_func;
        rf_b(2:end) = ramp_func(2:end)./(c2.*((dx:dx:1).^(3/4)));
        
        %----Interp Pressure
        vl_pres_interp = interp1(nvec,vl_pres,0:dx:1);
        vl_pres_prev = vl_pres_interp;
        vl_pres = vl_pres_prev;
    end
    if incres == 0
        nx = nvl;
    end
        
    for i = 2:nx-1
        
        if incres == 0
            z = nvec(i);
            z_prev = nvec(i-1);
            dx = z - z_prev;
        else
            z = (i-1)*dx;
            z_prev = (i-2)*dx;
        end
    
        if i == 2
            vl_enth_prev = vl_ip_enth;
            vl_u_prev = vl_ip_u;
            vl_psi_prev = vl_ip_psi;
        end
    
        %Initial Guess
        vl_enth = vl_enth_prev;
        vl_u = vl_u_prev;
        vl_psi = vl_psi_prev;

        pres_res = 100; vcount = 0;
    
        while pres_res > 1e-8

            vcount = vcount + 1;
        
            xi1 = z + z_prev;
            p1 = vl_pres(i) + vl_pres(i-1);
            p2 = vl_pres(i+1) - vl_pres(i);
        %------------------------------------------------------------
            pp1 = ( (c1*p1)/((2*xi1)^(1/2)) ) - (1/(gam*chi*chi*chi));

            vl_del(i) = ( ((8*dx)/c2)*((xi1/2)^(1/4))*pp1...
                *( (((gam+1)/2)*pp1 + (1/(chi*chi*chi)))^(-1/2) )...
                - vl_del(i-1)*(3*dx - 4*xi1) )/(3*dx + 4*xi1);
        
            del1 = vl_del(i) + vl_del(i-1);
            
            ramf1 = rf_b(i) + rf_b(i-1);
        %------------------------------------------------------------

            if sweep == 1
                va = zeros(v_n+1,v_n+1);
                vr = zeros(v_n+1,1);
            end

        %Wall BCs
            va(1,1) = 1;
            vr(1) = -vl_u_prev(1) - vl_u(1);

            va(2,2) = 1;
            vr(2) = -vl_psi_prev(1) - vl_psi(1);

            mc = 1;

        for j = 2:v_ny-1 
            enth1 = vl_enth(j) + vl_enth_prev(j);
            psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j);
            psi3 = vl_psi(j) - vl_psi(j-1) + vl_psi_prev(j) - vl_psi_prev(j-1);
            u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j);
            u4 = vl_u(j) + vl_u(j-1) + vl_u_prev(j) + vl_u_prev(j-1);
            u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1);
            u7 = vl_u(j+1) - 2*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2*vl_u_prev(j) + vl_u_prev(j-1);

        %Streamfunction elements
            va(j+mc,(j+mc)-2) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)-1) = -4;
            va(j+mc,j+mc) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)+1) = 4;
        %----Streamfunction RHS
            vr(j+mc) = v_dy*(del1-ramf1)*u4 - 4*psi3;

            mc = mc + 1;

        %Momentum elements
            va(j+mc,(j+mc)-3) = p1*(16*dx*p1 - v_dy*dx*(del1-ramf1)*psi1 - 4*v_dy*xi1*(del1-ramf1)*psi2);
            va(j+mc,(j+mc)-1) = 4*v_dy*v_dy*((gam-1)/gam)*xi1*((del1-ramf1)^2)*p2*u1...
                    - p1*( 32*dx*p1 + 2*v_dy*v_dy*dx*((gam-1)/gam)*((del1-ramf1)^2)*u1 + 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*(u2+u1) );
            va(j+mc,j+mc)     = p1*u5*(del1-ramf1)*v_dy*(dx + 4*xi1);
            va(j+mc,(j+mc)+1) = p1*(16*dx*p1 + v_dy*dx*(del1-ramf1)*psi1 + 4*v_dy*xi1*(del1-ramf1)*psi2);
            va(j+mc,v_n+1)    = 32*dx*p1*u7 - 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2 + v_dy*(del1-ramf1)*u5*(dx*psi1 + 4*xi1*psi2)...
                    + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(4*dx + 8*xi1);
        %----Momentum RHS
            vr(j+mc) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*p1*u1*u2 - 16*dx*p1*p1*u7 - v_dy*(del1-ramf1)*p1*u5*(4*xi1*psi2 + dx*psi1)...
                    + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(8*xi1*p2 - 4*dx*p1);
        end
        %---BL BCs
            va(v_n-1,v_n-1) = 1;
            vr(v_n-1) = 2 - vl_u_prev(v_ny) - vl_u(v_ny);

            va(v_n,v_n-3) = -v_dy*(del1-ramf1);
            va(v_n,v_n-2) = -4;
            va(v_n,v_n-1) = -v_dy*(del1-ramf1);
            va(v_n,v_n)   = 4;
            vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))...
                - 4*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1));

        %Pressure Elements
            va(v_n+1,1) = 0.5*(vl_u(1) + vl_u_prev(1));
            mc = 1;
            for j = 2:v_ny-1
                va(v_n+1,j+mc) = vl_u(j) + vl_u_prev(j);
                mc = mc + 1;
            end
            va(v_n+1,v_n-1) = 0.5*(vl_u(v_ny) + vl_u_prev(v_ny));
            va(v_n+1,v_n+1) = (2*((gam*c3)^(1/2)))/(v_dy*(gam-1));

        %Pressure RHS
            rp = ( (vl_enth_prev(1) + vl_enth(1)) - 0.25*((vl_u_prev(1) + vl_u(1))^2) )...
                + ( (vl_enth_prev(v_ny) + vl_enth(v_ny)) - 0.25*((vl_u_prev(v_ny) + vl_u(v_ny))^2) );
            for j = 2:v_ny-1
                rp = rp + 2*( (vl_enth_prev(j) + vl_enth(j)) - 0.25*((vl_u_prev(j) + vl_u(j))^2) );
            end

            vr(v_n+1) = rp - ((2*((gam*c3)^(1/2)))/(v_dy*(gam-1)))*p1;

    %---------------------------------------------------------------------
            va = sparse(va);
            vx = va\vr;
    %---------------------------------------------------------------------

            mc = 0;
            vl_u_out = zeros(v_ny,1); vl_psi_out = zeros(v_ny,1);
            for j = 1:v_ny
                vl_u_out(j) = vl_u(j) + vx(j+mc);
                mc = mc + 1;
                vl_psi_out(j) = vl_psi(j) + vx(j+mc);
            end
            vl_pres_out = vl_pres(i) + vx(v_n+1);
            
            if vl_pres_out < 0
                vl_pres_prev(1:i-1) = vl_pres(1:i-1);
                vl_pres_prev(i:end-1) = vl_pres(i-1);
                %fprintf('restart sweep at x = %d\n\n', i)
                br = 1;
                break
            else
                pres_res = abs(vx(v_n+1));
                vl_u = vl_u_out;
                vl_psi = vl_psi_out;
                vl_pres(i) = vl_pres_out;
            end
        end

        if br == 1
            break
        end
        if incres == 1
            vl_u_save(:,i) = vl_u(:);
            vl_psi_save(:,i) = vl_psi(:);
        end

    end
    
    if br ~= 1
        pres_con = max(abs(vl_pres_prev - vl_pres));
        %fprintf('chi = %d, sweep = %d,  pres_diff = %.8f\n',chi,sweep,pres_con)
        vl_pres_prev = vl_pres;
    else
        br = 0;
    end

end

%%%
vl_u_save(:,1) = vl_ip_u;
vl_psi_save(:,1) = vl_ip_psi;

% vl_dens = zeros(v_ny,nx);
% vl_temp = zeros(v_ny,nx);
% vl_mu = zeros(v_ny,nx);
% vl_v = zeros(v_ny,nx);
% eta = zeros(v_ny,nx);
% 
% vl_dens(:,1) = (2*gam*c3)./((gam-1).*(1 - vl_u_save(:,1).^2));
% vl_temp(:,1) = c3./vl_dens(:,1);
% vl_mu(:,1) = gam.*vl_temp(:,1);
% 
% eta(v_ny,:) = 1;
% for j = 2:v_ny-1
% 	eta(j,1) = eta(j-1,1) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,1))+(1/vl_dens(j-1,1))));
% end
% vl_v(:,1) = 0.75.*eta(:,1).*vl_u_save(:,1) - (0.25*sqrt(gam*c3).*vl_psi_save(:,1))./vl_dens(:,1);
% 
% for i = 2:nx-1
% 	vl_dens(:,i) = (2*gam*c3*vl_pres(i))./((gam-1).*(1 - vl_u_save(:,i).^2));
%     vl_temp(:,i) = (c3*vl_pres(i))./vl_dens(:,i);
%     vl_mu(:,i) = gam.*vl_temp(:,i);
%     for j = 2:v_ny-1
%         eta(j,i) = eta(j-1,i) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,i))+(1/vl_dens(j-1,i))));
%     end
% 	vl_v(:,i) = 0.75.*eta(:,i).*vl_u_save(:,i).*vl_del(i)...
%         + ((i-1)*dx).*eta(:,i).*vl_u_save(:,i).*((vl_del(i) - vl_del(i-1))/dx)...
%         - (sqrt(gam*c3)./(4.*vl_dens(:,i))).*( vl_psi_save(:,i) + (4*((i-1)*dx).*((vl_psi_save(:,i) - vl_psi_save(:,i-1))./dx)) );
% end
% 
% cit = sprintf('chi%d/sine/convex/1000pts',chi*10);
% rit = sprintf('%d',s_p1*10);

% if deforms == 1
%     if con1 == 1
%         rit = sprintf('convex');
%     else
%         rit = sprintf('concave');
%     end
% elseif deforms == 2
%     if con1 == 1
%         rit = sprintf('convex_concave');
%     else
%         rit = sprintf('concave_convex');
%     end
% end

% mkdir(cit,rit); 
% fie = strcat(cit,'/',rit);
% %%%
% %save(strcat(fie,'/den_bar.mat'),'vl_dens')
% %save(strcat(fie,'/t_bar.mat'),'vl_temp')
% %save(strcat(fie,'/mu_bar.mat'),'vl_mu')
% %save(strcat(fie,'/v_bar.mat'),'vl_v')
% %save(strcat(fie,'/u_bar.mat'),'vl_u_save')
% %save(strcat(fie,'/sf_bar.mat'),'vl_psi_save')
% %save(strcat(fie,'/cons.mat'),'chi','c1','c2')
% %save(strcat(fie,'/p_bar.mat'),'vl_pres')
% xip = (c1./((0:dx:1).^(1/2))).*vl_pres;
% %save(strcat(fie,'/p_nd.mat'),'xip')
% %save(strcat(fie,'/bl_bar.mat'),'vl_del')
% xid = c2.*((0:dx:1).^(3/4)).*vl_del';
% %save(strcat(fie,'/bl_nd.mat'),'xid')
% %save(strcat(fie,'/rampf.mat'),'ramp_func')
parsav_small(vl_pres,vl_del,vl_u_save,vl_psi_save,int8(chi*10),int8(s_p1*10),int8(e_p1*10))
end