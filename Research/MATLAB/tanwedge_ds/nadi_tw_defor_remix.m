clear
clc
close all

%----Setup
Pr = 0.725; gam = 14.d-1; c = 1.d0;
v_ny = 501; v_n = 3*v_ny; v_dy = 10/(v_ny-1);
nx = 1001; dx = 1/(nx-1);

c3 = importdata('ip_data/c3con_pr725adi.dat');
vl_ip = importdata('ip_data/vl_pr725adi.dat');
vl_ip_enth = vl_ip(:,1);
vl_ip_u = vl_ip(:,2);
vl_ip_psi = vl_ip(:,3);
il_ip = importdata('ip_data/il_khm_500.dat');
il_p_bl = il_ip(1,3);

c2 = ((c*c3)/il_p_bl)^(1/4); c1 = (c*c3)/(c2*c2);

chi = 1;

%----Ramp Parameters
% con = 1;
% con = 0;
% h = 0.01;
% h = 0.025;
% h = 0.05;
% def_points = [0.1,0.4; 0.2,0.5; 0.3,0.6; 0.4,0.7; 0.5,0.8; 0.6,0.9];
% def_points = [0.1,0.6; 0.2,0.7; 0.3,0.8; 0.4,0.9; 0.1,0.9];

% for se = 1:length(def_points)
	
% con1 = con; h1 = h; 
% set_points = def_points(se,:);
% s_p = set_points(1); e_p = set_points(2);

%----Ramp function
ramp_func = zeros(nx,1);
%ramp_func = defor_geo(nx,s_p,e_p,h1,con1);
rf_b = ramp_func;
%rf_b(2:end) = ramp_func(2:end)./(c2.*((dx:dx:1).^(3/4)));

%----BL
vl_del = zeros(nx,1);
vl_del(1) = 1;

%----Pressure
vl_pres = zeros(nx,1);
vl_pres(1) = 1;
vl_pres(2:end) = 1/(c1*gam*chi*chi*chi);
vl_pres_prev = vl_pres;

%----Saved layer profiles
vl_enth_save = zeros(v_ny,nx); vl_enth_save(:,1) = vl_ip_enth;
vl_u_save = zeros(v_ny,nx); vl_u_save(:,1) = vl_ip_u;
vl_psi_save = zeros(v_ny,nx); vl_psi_save(:,1) = vl_ip_psi;
vl_enth_save_prev = vl_enth_save;
vl_u_save_prev = vl_u_save;
vl_psi_save_prev = vl_psi_save;

pres_con = 100; sweep = 0; br = 0;

while pres_con > 1e-10
    sweep = sweep + 1;
    
    vl_pres = vl_pres_prev;
    vl_enth_save = vl_enth_save_prev;
    vl_u_save = vl_u_save_prev;
    vl_psi_save = vl_psi_save_prev;
    
    for i = 2:nx-1
        
        z = (i-1)*dx;
        z_prev = (i-2)*dx;
        
        if sweep == 1
            vl_enth_prev = vl_ip_enth; %vl_enth_save(:,i-1);
            vl_u_prev = vl_ip_u; %vl_u_save(:,i-1);
            vl_psi_prev = vl_ip_psi; %vl_psi_save(:,i-1);
        else
            vl_enth_prev = vl_enth_save(:,i-1);
            vl_u_prev = vl_u_save(:,i-1);
            vl_psi_prev = vl_psi_save(:,i-1);
        end
    
        %---Initial Guess
        if sweep == 1 %|| mod(5,sweep) == 0
            vl_enth = vl_enth_prev;
            vl_u = vl_u_prev;
            vl_psi = vl_psi_prev;
        else
           vl_enth = vl_enth_save_prev(:,i);
           vl_u = vl_u_save_prev(:,i);
           vl_psi = vl_psi_save_prev(:,i);
        end
        
        pres_res = 100; vcount = 0;
        
        while pres_res > 1e-8

            vcount = vcount + 1;
        
            xi1 = z + z_prev;
            p1 = vl_pres(i) + vl_pres(i-1);
            p2 = vl_pres(i+1) - vl_pres(i);
        %------------------------------------------------------------
            pp1 = ( (c1*p1)/((2*xi1)^(1/2)) ) - (1/(gam*chi*chi*chi));

            vl_del(i) = ( ((8*dx)/c2)*((xi1/2)^(1/4))*pp1...
                *( (((gam+1)/2)*pp1 + (1/(chi*chi*chi)))^(-1/2) )...
                - vl_del(i-1)*(3*dx - 4*xi1) )/(3*dx + 4*xi1);
        
            del1 = vl_del(i) + vl_del(i-1);
            
            ramf1 = rf_b(i) + rf_b(i-1);
        %------------------------------------------------------------
            va = zeros(v_n+1,v_n+1);
            vr = zeros(v_n+1,1);

        %Wall BCs
            va(1,1) = 1; va(1,4) = -1;
            vr(1) = vl_enth_prev(2) - vl_enth_prev(1) + vl_enth(2) - vl_enth(1);

            va(2,2) = 1;
            vr(2) = -vl_u_prev(1) - vl_u(1);

            va(3,3) = 1;
            vr(3) = -vl_psi_prev(1) - vl_psi(1);

            mc = 2;

        for j = 2:v_ny-1 
            enth1 = vl_enth(j) + vl_enth_prev(j); enth2 = vl_enth(j) - vl_enth_prev(j);
            enth3 = vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1);
            enth4 = vl_enth(j+1) - 2*vl_enth(j) + vl_enth(j-1)...
                +  vl_enth_prev(j+1) - 2*vl_enth_prev(j) + vl_enth_prev(j-1);
            psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j);
            psi3 = vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j);                         %%
            u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j);
            u4 = vl_u(j+1) + vl_u(j) + vl_u_prev(j+1) + vl_u_prev(j);                                   %%
            u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1);
            u7 = vl_u(j+1) - 2*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2*vl_u_prev(j) + vl_u_prev(j-1);

        %Streamfunction elements
            va(j+mc,(j+mc)-2) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)-1) = -4;
            va(j+mc,(j+mc)+1) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)+2) = 4;
        %----Streamfunction RHS
            vr(j+mc) = v_dy*(del1-ramf1)*u4 - 4*psi3;

            mc = mc + 1;

        %Momentum elements
            va(j+mc,(j+mc)-3) = v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
            va(j+mc,(j+mc)-1) = v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(8*xi1*p2 - 4*dx*p1);
            va(j+mc,(j+mc)) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*p1*(u1+u2)...
                + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*u1*(2*dx*p1 - 4*xi1*p2) + 32*dx*p1*p1;
            va(j+mc,(j+mc)+1) = -v_dy*dx*(del1-ramf1)*p1*u5*((4/dx)*xi1 + 1);
            va(j+mc,(j+mc)+3) = -v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
            va(j+mc,v_n+1) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2 - v_dy*dx*(del1-ramf1)*u5*((4/dx)*xi1*psi2+psi1)...
                - v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(8*xi1 + 4*dx) - 32*dx*p1*u7;
        %----Momentum RHS
            vr(j+mc) = 16*dx*u7*p1*p1 - 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2*p1 + v_dy*dx*(del1-ramf1)*p1*u5*((4/dx)*xi1*psi2 + psi1)...
                + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(4*dx*p1 - 8*xi1*p2);
            
            mc = mc + 1;
            
        %Energy elements
            va(j+mc,(j+mc)-5) = Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
            va(j+mc,(j+mc)-4) = 8*dx*(Pr-1)*p1*(u5-u1);
            va(j+mc,(j+mc)-2) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*u1 + 32*dx*p1;
            va(j+mc,(j+mc)-1) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*enth2 - 8*dx*(Pr-1)*p1*(u7 - 2*u1);
            va(j+mc,(j+mc)) = -Pr*v_dy*(del1-ramf1)*enth3*(4*xi1 + dx);
            va(j+mc,(j+mc)+1) = -Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
            va(j+mc,(j+mc)+2) = -8*dx*(Pr-1)*p1*(u5 + u1);
            va(j+mc,v_n+1) = -16*dx*enth4 - dx*(Pr-1)*(4*u5*u5 + 8*u1*u7);
        %----Energy RHS
            vr(j+mc) = 16*dx*p1*enth4 + 4*dx*(Pr-1)*p1*(u5*u5 + 2*u1*u7) - 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*u1*enth2...
                + Pr*v_dy*(del1-ramf1)*enth3*(4*xi1*psi2+dx*psi1);
        end
        %---BL BCs
            va(v_n-2,v_n-2) = 1; vr(v_n-2) = 1 - vl_enth_prev(v_ny) - vl_enth(v_ny);
            
            va(v_n-1,v_n-1) = 1; vr(v_n-1) = 2 - vl_u_prev(v_ny) - vl_u(v_ny);
            
            va(v_n,v_n-4) = -v_dy*(del1-ramf1);
            va(v_n,v_n-3) = -4;
            va(v_n,v_n-1) = -v_dy*(del1-ramf1);
            va(v_n,v_n)   = 4;
            
            vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))...
                - 4*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1));

        %Pressure Elements
            va(v_n+1,1) = 2;
            va(v_n+1,2) = -(vl_u(1) + vl_u_prev(1));
            mc = 2;
            for j = 2:v_ny-1
                va(v_n+1,j+mc) = 4;
                mc = mc + 1;
                va(v_n+1,j+mc) = -2*(vl_u(j) + vl_u_prev(j));
                mc = mc + 1;
            end
            va(v_n+1,v_n-2) = 2;
            va(v_n+1,v_n-1) = -(vl_u(v_ny) + vl_u_prev(v_ny));
            va(v_n+1,v_n+1) = -(4*((gam*c3)^(1/2)))/(v_dy*(gam-1));
            
        %Pressure RHS
            rp = ( 2*(vl_enth_prev(1) + vl_enth(1)) - 0.5*((vl_u_prev(1) + vl_u(1))^2) )...
                + ( 2*(vl_enth_prev(v_ny) + vl_enth(v_ny)) - 0.5*((vl_u_prev(v_ny) + vl_u(v_ny))^2) );
            for j = 2:v_ny-1
                rp = rp + 2*( 2*(vl_enth_prev(j) + vl_enth(j)) - 0.5*((vl_u_prev(j) + vl_u(j))^2) );
            end

            vr(v_n+1) = ((4*((gam*c3)^(1/2)))/(v_dy*(gam-1)))*p1 - rp;

    %---------------------------------------------------------------------
            va = sparse(va);
            vx = va\vr;
    %---------------------------------------------------------------------

            mc = 0;
            vl_enth_out = zeros(v_ny,1); vl_u_out = zeros(v_ny,1); vl_psi_out = zeros(v_ny,1);
            for j = 1:v_ny
                vl_enth_out(j) = vl_enth(j) + vx(j+mc);
                mc = mc + 1;
                vl_u_out(j) = vl_u(j) + vx(j+mc);
                mc = mc + 1;
                vl_psi_out(j) = vl_psi(j) + vx(j+mc);
            end
            vl_pres_out = vl_pres(i) + vx(v_n+1);

            if vl_pres_out < 0
                vl_pres_prev(1:i-1) = vl_pres(1:i-1);
                vl_pres_prev(i:end-1) = vl_pres(i-1);
                    
                vl_enth_save_prev = vl_enth_save;
                vl_u_save_prev = vl_u_save;
                vl_psi_save_prev = vl_psi_save;
                for ii = i:nx
                    vl_enth_save_prev(:,ii) = vl_enth_save(:,i-1);
                    vl_u_save_prev(:,ii) = vl_u_save(:,i-1);
                    vl_psi_save_prev(:,ii) = vl_psi_save(:,i-1);
                end
                
                fprintf('restart sweep at x = %d\n\n', i)
                br = 1;
                break
            else
                pres_res = abs(vx(v_n+1));
                vl_enth_save(:,i) = vl_enth_out;
                vl_u_save(:,i) = vl_u_out;
                vl_psi_save(:,i) = vl_psi_out;
                vl_pres(i) = vl_pres_out;
            end
        end
        if br == 1
            break
        end
        figure(1)
        plot(0:dx:1,vl_pres,'b')
        hold on
        plot(dx*(i-1),vl_pres(i),'or')
        hold off
            
        figure(2)
        xip = (c1./((0:dx:1).^(1/2))).*vl_pres';
        plot(0:dx:1,xip,'b')
        hold on
        plot(dx*(i-1),xip(i),'or')
        hold off
                        
        pause(0.00001)

%         figure(1)
%         plot(vl_enth_save(:,i),0:v_dy:10)
%     
%         figure(2)
%         plot(vl_u_save(:,i),0:v_dy:10)
%     
%         figure(3)
%         plot(vl_psi_save(:,i),0:v_dy:10)
%     
%         pause
                
    end
    
	if br ~= 1
        pres_con = max(abs(vl_pres_prev - vl_pres));
        fprintf('sweep = %d,  pres_diff = %.8f\n',sweep,pres_con)
        
        vl_pres_prev = vl_pres;
        vl_enth_save_prev = vl_enth_save;
        vl_u_save_prev = vl_u_save;
        vl_psi_save_prev = vl_psi_save;
	else
        br = 0;
	end
    
end

% end %parloop end