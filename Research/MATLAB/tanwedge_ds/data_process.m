clear
clc
close all
%--------------------------------------------------------------
%                        Initial Data
%--------------------------------------------------------------
c3 = importdata('ip_data\c3con_pr1adi.dat');
il_ip = importdata('ip_data\il_khm_500.dat');
il_p_bl = il_ip(1,3);
c = 1; gam = 1.4;
v_ny = 501; nx = 1001; v_dy = 1/50; dx = 1/1000;
c2 = ((c*c3)/il_p_bl)^(1/4); c1 = (c*c3)/(c2*c2);
%--------------------------------------------------------------
%                       Flat plate Data
%--------------------------------------------------------------

load(strcat('chi_1/flat/nd_bl.mat')); flat_bl = nd_bl;
%
load(strcat('chi_1/flat/bl_bar.mat')); flat_bl_b = vl_del;
%
load(strcat('chi_1/flat/nd_pres.mat')); flat_pres = nd_pres;
%
load(strcat('chi_1/flat/nd_u.mat')); flat_uvel = nd_u;
%
load(strcat('chi_1/flat/nd_v.mat')); flat_vvel = nd_v;
%
load(strcat('chi_1/flat/nd_dens.mat')); flat_dens = nd_dens;
%
load(strcat('chi_1/flat/nd_temp.mat')); flat_temp = nd_temp;
%
load(strcat('chi_1/flat/nd_mu.mat')); flat_mu = nd_mu;
%
load(strcat('chi_1/flat/sf_bar.mat')); flat_sf_b = vl_psi_save;
% 
figure(1)
plot(0:dx:1,flat_bl)
title('boundary layer')
%
figure(2)
plot(0:dx:1,flat_pres)
title('pressure')
%
figure(3)
plot(flat_uvel(:,251),0:v_dy:10)
title('x-velocity')
%
figure(4)
plot(flat_vvel(:,251),0:v_dy:10)
title('y-velocity')
%
figure(5)
plot(flat_dens(:,251),0:v_dy:10)
title('density')
%
figure(6)
plot(flat_temp(:,251),0:v_dy:10)
title('temperature')
%
figure(7)
plot(flat_mu(:,251),0:v_dy:10)
title('viscosity')
%
figure(8)
plot(flat_sf_b(:,251),0:v_dy:10)
title('streamfunction')

% %--------------------------------------------------------------
% %                       File Locations
% %--------------------------------------------------------------
% % s counter
% sisc{1} = sprintf('semicircle/'); sisc{2} = sprintf('sine/');
% % c counter
% coco{1} = sprintf('concave/'); coco{2} = sprintf('convex/');
% % h counter
% hhh{1} = sprintf('h01/'); hhh{2} = sprintf('h025/'); hhh{3} = sprintf('h05/');
% % d counter
% d1d2{1} = sprintf('defp1/'); d1d2{2} = sprintf('defp2/');
% % dp counter
% def_p1 = {'1_4/','2_5/','3_6/','4_7/','5_8/','6_9/'};
% def_p2 = {'1_6/','2_7/','3_8/','4_9/','1_9/'};
% %--------------------------------------------------------------
% %                     Short Length Deformations
% %--------------------------------------------------------------
% fid_s = 1;
% d = 1;
% %[x1,x2] = meshgrid(0:1/1000:1,0:1/500:1-0.1);
% [x1,x2] = meshgrid(0:1/1000:1,0:1/500:1);
% s = 1; c = 2; h = 2; dp = 3;
% for s = 1:2
% 	for c = 1:2
%        for h = 1:3
%            for dp = 1:6
% %--------------------------------------------------------------
% %--------------------------------------------------------------
% %--------------------------------------------------------------
%             fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p1{dp}));
%             
%             if ~exist(strcat('chi_1/',fina),'dir')
%                continue
%             end
% 
%             try
%                 load(strcat('chi_1/',fina,'nd_bl.mat'))
%             catch
%                continue
%             end
%             
%             bl = nd_bl;
%             %
%             load(strcat('chi_1/',fina,'bl_bar.mat')); bl_b = vl_del;
%             %
%             load(strcat('chi_1/',fina,'nd_pres.mat')); pres = nd_pres;
%             %
%             load(strcat('chi_1/',fina,'nd_u.mat')); uvel = nd_u;
%             %
%             load(strcat('chi_1/',fina,'nd_v.mat')); vvel = nd_v;
%             %
%             load(strcat('chi_1/',fina,'nd_dens.mat')); dens = nd_dens;
%             %
%             load(strcat('chi_1/',fina,'nd_temp.mat')); temp = nd_temp;
%             %
%             load(strcat('chi_1/',fina,'nd_mu.mat')); mu = nd_mu;
%             %
%             load(strcat('chi_1/',fina,'sf_bar.mat')); sf_b = vl_psi_save;
% 
%             if c == 1
%                 c_str = 'Concave';
%                 c_str2 = 'c0';
%             else
%                 c_str = 'Convex';
%                 c_str2 = 'c1';
%             end
%             if s == 1
%                 s_str = 'semicircle';
%                 s_str2 = 'sc';
%             else
%                 s_str = 'sine';
%                 s_str2 = 'si';
%             end
%             if h == 1
%                 h_str = '1% wall length';
%                 h_str2 = 'h01';
%             elseif h == 2
%                 h_str = '2.5% wall length';
%                 h_str2 = 'h025';
%             else
%                 h_str = '5% wall length';
%                 h_str2 = 'h05';
%             end
% %--------------------------------------------------------------
% %                   Flow Variable Profiles
% %--------------------------------------------------------------
% % Density
% %             figure(fid_s)
% %             dens_plot = abs(flat_dens(1:451,:) - dens(1:451,:));
% %             contour(x1,x2,dens_plot,50); colorbar
% %             title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% %             set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% %             fid_s = fid_s + 1;
% % % Temperature
% %             figure(fid_s)
% %             contour(x1,x2,temp,20); colorbar
% %             fid_s = fid_s + 1;
% % % Viscosity
% %             figure(fid_s)
% %             contour(x1,x2,mu,20); colorbar
% %             fid_s = fid_s + 1;
% % strwise vel
% %             figure(fid_s)
% %             contour(x1,x2,nd_u,20); colorbar
% %             title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% %             set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% %             fid_s = fid_s + 1;
% 
% % % Pressure
% %             figure(fid_s)
% %             plot(0:dx:1,flat_pres,'--k','LineWidth',1)
% %             hold on
% %             plot(0:dx:1,pres,'b','LineWidth',1)
% %             plot([dp/10 dp/10], ylim, '-.k')
% %             plot([(dp+3)/10 (dp+3)/10], ylim, '-.k')
% %             legend({'Flat Plate','Deformed Wall'},'Location','northwest')
% %             title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% %             ylabel('p/p_i_n_f'); xlabel('x/L')
% %             axis([0 1 0 3])
% %             set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% % saveas(figure(fid_s),sprintf(strcat('plots/shear_plots/pres_prof_%d_',c_str2,s_str2,h_str2,'_%d.jpg'),d,dp))
% %             fid_s = fid_s + 1;
% 
% % % Boundary Layer
% %             figure(fid_s)
% %             plot(0:dx:1-dx,flat_bl(1:end-1),'--k','LineWidth',1)
% %             hold on
% %             plot(0:dx:1-dx,bl(1:end-1),'b','LineWidth',1)
% %             plot([dp/10 dp/10], ylim, '-.k')
% %             plot([(dp+3)/10 (dp+3)/10], ylim, '-.k')
% %             legend({'Flat Plate','Deformed Wall'},'Location','northwest')
% %             title(strcat(c_str,{' '},s_str,{' '},'with height of',{' '},h_str))
% %             ylabel('delta/y'); xlabel('x/L')
% %             set(figure(fid_s), 'Position', [100, 100, 1000, 600])
% %             fid_s = fid_s + 1;
% 
% %             figure((dp*10)+2)
% %             plot(0:dx:1,bl,'LineWidth',1)
% %             hold on
% %             plot([dp/10 dp/10], ylim, '--k')
% %             plot([(dp+3)/10 (dp+3)/10], ylim, '--k')
% %             axis([0 1 0 0.45])
% 
% %             figure((dp*10)+3)
% %             %plot(uvel(1:2:end,(dp*100)+1),eta(1:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(uvel(1:2:end,(dp*700)+1),0:2/500:1,'LineWidth',1)
% %             hold on
% %             %plot(uvel(2:2:end,(dp*100)+1),eta(2:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(uvel(2:2:end,(dp*700)+1),0:2/500:1-(2/500),'LineWidth',1)
% %             %axis([0 1.5 0 1.5])
% %             axis([-0.01 1.01 0 1.01])
% %             ylabel('y/(e^4 M_i_n_f^3 L^*)'); xlabel('u/U_i_n_f')
% 
% %             figure((dp*10)+4)
% %             %plot(vvel(1:2:end,(dp*100)+1),eta(1:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(vvel(1:2:end,(dp*700)+1),0:2/500:1,'LineWidth',1)
% %             hold on
% %             %plot(vvel(2:2:end,(dp*100)+1),eta(2:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(vvel(2:2:end,(dp*700)+1),0:2/500:1-(2/500),'LineWidth',1)
% %             %axis([0 1.5 0 1.5])
% %             axis([-.01 1 0 1])
% %             ylabel('y/(e^4 M_i_n_f^3 L^*)'); xlabel('(M_i_n_f v)/U_i_n_f')
% 
% %             figure((dp*10)+5)
% %             %plot(temp(1:2:end,(dp*100)+1),eta(1:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(temp(1:2:end,(dp*700)+1),0:2/500:1,'LineWidth',1)
% %             hold on
% %             %plot(temp(2:2:end,(dp*100)+1),eta(2:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(temp(2:2:end,(dp*700)+1),0:2/500:1-(2/500),'LineWidth',1)
% %             axis([0 0.25 0 1.01])
% %             ylabel('y/(e^4 M_i_n_f^3 L^*)'); xlabel('T/(gamma M_i_n_f^2 T_i_n_f)')
% 
% %             figure((dp*10)+6)
% %             %plot(mu(1:2:end,(dp*100)+1),eta(1:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(mu(1:2:end,(dp*700)+1),0:2/500:1,'LineWidth',1)
% %             hold on
% %             %plot(mu(2:2:end,(dp*100)+1),eta(2:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(mu(2:2:end,(dp*700)+1),0:2/500:1-(2/500),'LineWidth',1)
% %             axis([0 0.25 0 1.01])
% %             ylabel('y/(e^4 M_i_n_f^3 L^*)'); xlabel('mu/(M_i_n_f^2 mu_i_n_f)')
% 
% %             figure((dp*10)+7)
% %             %plot(dens(1:2:end,(dp*100)+1),eta(1:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(dens(1:2:end,(dp*700)+1),0:2/500:1,'LineWidth',1)
% %             hold on
% %             %plot(dens(2:2:end,(dp*100)+1),eta(2:2:end,(dp*100)+1),'LineWidth',1)
% %             plot(dens(2:2:end,(dp*700)+1),0:2/500:1-(2/500),'LineWidth',1)
% %             axis([0 30 0 1.01])
% %             ylabel('y/(e^4 M_i_n_f^3 L^*)'); xlabel('(M_i_n_f^2 rho)/rho_i_n_f')
% 
% %--------------------------------------------------------------
% %                   Skin Friction Drag
% %--------------------------------------------------------------
% 
% 
% %--------------------------------------------------------------
% %                   The other thing
% %--------------------------------------------------------------
% 
% %             bl_split = zeros(1001,1);
% %             for i = 1:nx
% %                 jp_prev = uvel(101,i);
% %                 bl_step = 0:bl_b(i)/500:bl_b(i);
% %                 for j = 102:v_ny
% %                     jp_cur = uvel(j,i);
% %                     if jp_cur < jp_prev
% %                         bl_split(i) = bl_step(j-1);
% %                         break
% %                     else
% %                         jp_prev = jp_cur;
% %                     end
% %                 end
% %                 if bl_split(i) == 0
% %                     bl_split(i) = bl_b(i);
% %                 end
% %             end
% %             nd_bl_split = zeros(1001,1);
% %             for i = 1:nx
% %                 xi = (i-1)*dx;
% %                 nd_bl_split(i) = (c2*(xi^(0.75)))*bl_split(i);
% %             end
% %             plot(0:1/1000:1,bl,'LineWidth',1)
% %             hold on
% %             plot(0:1/1000:1,nd_bl_split)
% 
% %--------------------------------------------------------------
% %--------------------------------------------------------------
%            end
%        end
% 	end
% end
% %--------------------------------------------------------------
% %                   Medium Length Deformations
% %--------------------------------------------------------------
% % d = 2;
% % for s = 1:2
% %     for c = 1:2
% %         for h = 1:3
% %             for dp = 1:5
% % %--------------------------------------------------------------
% % %--------------------------------------------------------------
% % %--------------------------------------------------------------
% %             fina = sprintf(strcat(sisc{s},coco{c},hhh{h},d1d2{d},def_p2{dp}));
% %             if ~exist(strcat('chi_1/',fina),'dir')
% %                 continue
% %             end
% %             %
% %             try
% %                 load(strcat('chi_1/',fina,'nd_bl.mat'))
% %             catch
% %                 continue
% %             end
% %             bl = nd_bl;
% %             load(strcat('chi_1/',fina,'bl_bar.mat'))
% %             bl_b = vl_del;
% %             %
% %             load(strcat('chi_1/',fina,'nd_pres.mat'))
% %             pres = nd_pres;
% %             %
% %             load(strcat('chi_1/',fina,'nd_u.mat'))
% %             uvel = nd_u;
% %             %
% %             load(strcat('chi_1/',fina,'nd_v.mat'))
% %             vvel = nd_v;
% %             %
% %             load(strcat('chi_1/',fina,'nd_dens.mat'))
% %             dens = nd_dens;
% %             %
% %             load(strcat('chi_1/',fina,'nd_temp.mat'))
% %             temp = nd_temp;
% %             %
% %             load(strcat('chi_1/',fina,'nd_mu.mat'))
% %             mu = nd_mu;
% %             %
% %             load(strcat('chi_1/',fina,'sf_bar.mat'))
% %             sf_b = vl_psi_save;
% % 
% % %--------------------------------------------------------------
% % %--------------------------------------------------------------
% % 
% %             end
% %         end
% %     end
% % end