clear
clc
close all

Pr = 0.725; gam = 14.d-1; c = 1.d0;
v_ny = 501; v_n = 3*v_ny; v_dy = 10/(v_ny-1);
nx1 = 51; dx1 = 1/(nx1-1);
nx2 = 1001; dx2 = 1/(nx2-1);
nvec1 = 0:dx1:0.95-dx1;
nvl1 = length(nvec1);
nvec2 = 0.95:dx2:1;
nvl2 = length(nvec2);
nvec = [nvec1 nvec2];
nvl = length(nvec);

c3 = importdata('ip_data/c3con_pr725adi.dat');
vl_ip = importdata('ip_data/vl_pr725adi.dat');
vl_ip_enth = vl_ip(:,1);
vl_ip_u = vl_ip(:,2);
vl_ip_psi = vl_ip(:,3);
il_ip = importdata('ip_data/il_khm_500.dat');
il_p_bl = il_ip(1,3);

c2 = ((c*c3)/il_p_bl)^(1/4); c1 = (c*c3)/(c2*c2);

chi = 2;

%-----Deformation parameters
%con = 1; h = 0.01;
%def_points = [0.1,0.4; 0.2,0.5; 0.3,0.6; 0.4,0.7; 0.5,0.8; 0.6,0.9];
% deforms = 1;

%for dp = 1:6
%for deforms = 1:2
% deforms = 2;

% if deforms == 1
    %con1 = con; h1 = h; 
    %set_points = def_points;
    %s_p1 = def_points(dp,1); e_p1 = def_points(dp,2);
% elseif deforms == 2
%     con1 = con_vec(con_ind); h1 = 0.01;
%     s_p1 = 0.1; e_p1 = 0.4;
%     
%     con2 = -con1; h2 = h1;
%     s_p2 = 0.4; e_p2 = 0.7;
% end
    
%----BL
vl_del = zeros(nvl,1);
vl_del(1) = 1;

%----Pressure
vl_pres = zeros(nvl,1);
vl_pres(1:end-1) = 1;
vl_pres(end) = 1/(c1*gam*chi*chi*chi);
vl_pres_prev = vl_pres;

%----Ramp function
% if deforms == 1
%     ramp_func1 = sine_defor_geo(nx1,s_p1,e_p1,h1,con1);
%     ramp_func2 = sine_defor_geo(nx2,s_p1,e_p1,h1,con1);
% elseif deforms == 2
%     ramp_func1 = defor_geo(nx1,s_p1,e_p1,h1,con1);
%     ramp_func2 = defor_geo(nx2,s_p1,e_p1,h1,con1);
% %     ramp_func1 = defor_geo2(nx1,s_p1,e_p1,h1,con1,s_p2,e_p2,h2,con2);
% %     ramp_func2 = defor_geo2(nx2,s_p1,e_p1,h1,con1,s_p2,e_p2,h2,con2);
% end

% rf_b2 = ramp_func2;
% rf_b2(2:end) = ramp_func2(2:end)./(c2.*((dx2:dx2:1).^(3/4)));
% rf_b1 = ramp_func1;
% rf_b1(2:end) = ramp_func1(2:end)./(c2.*((dx1:dx1:1).^(3/4)));
% 
% ramp_func = [ramp_func1(1:nvl1) ramp_func2(end-nvl2+1:end)];
% rf_b = [rf_b1(1:nvl1) rf_b2(end-nvl2+1:end)];

ramp_func = zeros(nvl,1); rf_b = ramp_func;

%----Saved layer profiles
vl_enth_save = zeros(v_ny,nx2);
vl_u_save = zeros(v_ny,nx2);
vl_psi_save = zeros(v_ny,nx2);

pres_con = 100; sweep = 0; br = 0; incres = 0;

while pres_con > 1e-8
    sweep = sweep + 1;
    vl_pres = vl_pres_prev;
    
	if sweep == 1 %pres_con < 1e-2 && incres == 0

        incres = 1;
        
        nx = 1001;
        dx = 1/(nx-1);
        
        %----BL
        vl_del = zeros(nx,1);
        vl_del(1) = 1;
        
        %----Ramp function
        %if deforms == 1
        %    ramp_func = sine_defor_geo(nx,s_p1,e_p1,h1,con1);
        %elseif deforms == 2
        %    ramp_func = defor_geo(nx,s_p1,e_p1,h1,con1);
%             ramp_func = defor_geo2(nx,s_p1,e_p1,h1,con1,s_p2,e_p2,h2,con2);
        %end
        %rf_b = ramp_func;
        %rf_b(2:end) = ramp_func(2:end)./(c2.*((dx:dx:1).^(3/4)));
        ramp_func = zeros(nx,1);
        rf_b = ramp_func;
        
        %----Interp Pressure
        vl_pres_interp = interp1(nvec,vl_pres,0:dx:1);
        vl_pres_prev = vl_pres_interp;
        vl_pres = vl_pres_prev;
	end
    if incres == 0
        nx = nvl;
    end

    for i = 2:nx-1
        
        if incres == 0
            z = nvec(i);
            z_prev = nvec(i-1);
            dx = z - z_prev;
        else
            z = (i-1)*dx;
            z_prev = (i-2)*dx;
        end
    
        if i == 2
            vl_enth_prev = vl_ip_enth;
            vl_u_prev = vl_ip_u;
            vl_psi_prev = vl_ip_psi;
        end
    
        %Initial Guess
        vl_enth = vl_enth_prev;
        vl_u = vl_u_prev;
        vl_psi = vl_psi_prev;

        pres_res = 100; vcount = 0;
    
        while pres_res > 1e-8

            vcount = vcount + 1;
        
            xi1 = z + z_prev;
            p1 = vl_pres(i) + vl_pres(i-1);
            p2 = vl_pres(i+1) - vl_pres(i);
        %------------------------------------------------------------
            pp1 = ( (c1*p1)/((2*xi1)^(1/2)) ) - (1/(gam*chi*chi*chi));

            vl_del(i) = ( ((8*dx)/c2)*((xi1/2)^(1/4))*pp1...
                *( (((gam+1)/2)*pp1 + (1/(chi*chi*chi)))^(-1/2) )...
                - vl_del(i-1)*(3*dx - 4*xi1) )/(3*dx + 4*xi1);
        
            del1 = vl_del(i) + vl_del(i-1);
            
            ramf1 = rf_b(i) + rf_b(i-1);
        %------------------------------------------------------------
            % if sweep == 1
                va = zeros(v_n+1,v_n+1);
                %va = zeros(v_n,9);
                vr = zeros(v_n+1,1);
                %vr_full = zeros(v_n+1,1);
                %vp = zeros(v_n,1);
                %pv = zeros(v_n+1,1);
                %vx = zeros(v_n+1,1);
            % end

        %Wall BCs
            va(1,1) = 1; va(1,4) = -1;
            vr(1) = vl_enth_prev(2) - vl_enth_prev(1) + vl_enth(2) - vl_enth(1);

            va(2,2) = 1;
            vr(2) = -vl_u_prev(1) - vl_u(1);

            va(3,3) = 1;
            vr(3) = -vl_psi_prev(1) - vl_psi(1);

%             va(1,6) = 1; va(1,9) = -1;
%             vr(1) = vl_enth_prev(2) - vl_enth_prev(1) + vl_enth(2) - vl_enth(1);
%             %vr(1) = 2*0.495 - vl_enth_pres(1) - vl_enth(1);
% 
%             va(2,6) = 1;
%             vr(2) = -vl_u_prev(1) - vl_u(1);
% 
%             va(3,6) = 1;
%             vr(3) = -vl_psi_prev(1) - vl_psi(1);

            mc = 2;

        for j = 2:v_ny-1 
            enth1 = vl_enth(j) + vl_enth_prev(j); enth2 = vl_enth(j) - vl_enth_prev(j);
            enth3 = vl_enth(j+1) - vl_enth(j-1) + vl_enth_prev(j+1) - vl_enth_prev(j-1);
            enth4 = vl_enth(j+1) - 2*vl_enth(j) + vl_enth(j-1)...
                +  vl_enth_prev(j+1) - 2*vl_enth_prev(j) + vl_enth_prev(j-1);
            psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j);
            psi3 = vl_psi(j+1) - vl_psi(j) + vl_psi_prev(j+1) - vl_psi_prev(j);                         %%
            u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j);
            u4 = vl_u(j+1) + vl_u(j) + vl_u_prev(j+1) + vl_u_prev(j);                                   %%
            u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1);
            u7 = vl_u(j+1) - 2*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2*vl_u_prev(j) + vl_u_prev(j-1);

        %Streamfunction elements
            va(j+mc,(j+mc)-2) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)-1) = -4;
            va(j+mc,(j+mc)+1) = -v_dy*(del1-ramf1);
            va(j+mc,(j+mc)+2) = 4;
            %
%             va(j+mc,4) = -v_dy*(del1-ramf1);
%             va(j+mc,5) = -4;
%             va(j+mc,7) = -v_dy*(del1-ramf1);
%             va(j+mc,8) = 4;
        %----Streamfunction RHS
            vr(j+mc) = v_dy*(del1-ramf1)*u4 - 4*psi3;

            mc = mc + 1;

        %Momentum elements
            va(j+mc,(j+mc)-3) = v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
            va(j+mc,(j+mc)-1) = v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(8*xi1*p2 - 4*dx*p1);
            va(j+mc,(j+mc)) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*p1*(u1+u2)...
                + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*u1*(2*dx*p1 - 4*xi1*p2) + 32*dx*p1*p1;
            va(j+mc,(j+mc)+1) = -v_dy*dx*(del1-ramf1)*p1*u5*((4/dx)*xi1 + 1);
            va(j+mc,(j+mc)+3) = -v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
            va(j+mc,v_n+1) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2 - v_dy*dx*(del1-ramf1)*u5*((4/dx)*xi1*psi2+psi1)...
                - v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(8*xi1 + 4*dx) - 32*dx*p1*u7;
            %
%             va(j+mc,3) = v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
%             va(j+mc,5) = v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(8*xi1*p2 - 4*dx*p1);
%             va(j+mc,6) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*p1*(u1+u2)...
%                 + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*u1*(2*dx*p1 - 4*xi1*p2) + 32*dx*p1*p1;
%             va(j+mc,7) = -v_dy*dx*(del1-ramf1)*p1*u5*((4/dx)*xi1 + 1);
%             va(j+mc,9) = -v_dy*dx*(del1-ramf1)*p1*((4/dx)*xi1*psi2 + psi1) - 16*dx*p1*p1;
%             
%             vp(j+mc) = 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2 - v_dy*dx*(del1-ramf1)*u5*((4/dx)*xi1*psi2+psi1)...
%                 - v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(8*xi1 + 4*dx) - 32*dx*p1*u7;
        %----Momentum RHS
            vr(j+mc) = 16*dx*u7*p1*p1 - 4*v_dy*v_dy*xi1*((del1-ramf1)^2)*u1*u2*p1 + v_dy*dx*(del1-ramf1)*p1*u5*((4/dx)*xi1*psi2 + psi1)...
                + v_dy*v_dy*((gam-1)/gam)*((del1-ramf1)^2)*(enth1 - 0.25*u1*u1)*(4*dx*p1 - 8*xi1*p2);
            
            mc = mc + 1;
            
        %Energy elements
            va(j+mc,(j+mc)-5) = Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
            va(j+mc,(j+mc)-4) = 8*dx*(Pr-1)*p1*(u5-u1);
            va(j+mc,(j+mc)-2) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*u1 + 32*dx*p1;
            va(j+mc,(j+mc)-1) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*enth2 - 8*dx*(Pr-1)*p1*(u7 - 2*u1);
            va(j+mc,(j+mc)) = -Pr*v_dy*(del1-ramf1)*enth3*(4*xi1 + dx);
            va(j+mc,(j+mc)+1) = -Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
            va(j+mc,(j+mc)+2) = -8*dx*(Pr-1)*p1*(u5 + u1);
            va(j+mc,v_n+1) = -16*dx*enth4 - dx*(Pr-1)*(4*u5*u5 + 8*u1*u7);
            %
%             va(j+mc,1) = Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
%             va(j+mc,2) = 8*dx*(Pr-1)*p1*(u5-u1);
%             va(j+mc,4) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*u1 + 32*dx*p1;
%             va(j+mc,5) = 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*enth2 - 8*dx*(Pr-1)*p1*(u7 - 2*u1);
%             va(j+mc,6) = -Pr*v_dy*(del1-ramf1)*enth3*(4*xi1 + dx);
%             va(j+mc,7) = -Pr*v_dy*(del1-ramf1)*(4*xi1*psi2+dx*psi1)-16*dx*p1;
%             va(j+mc,8) = -8*dx*(Pr-1)*p1*(u5 + u1);
%             
%             vp(j+mc) = -16*dx*enth4 - dx*(Pr-1)*(4*u5*u5 + 8*u1*u7);
        %----Energy RHS
            vr(j+mc) = 16*dx*p1*enth4 + 4*dx*(Pr-1)*p1*(u5*u5 + 2*u1*u7) - 4*v_dy*v_dy*Pr*xi1*((del1-ramf1)^2)*u1*enth2...
                + Pr*v_dy*(del1-ramf1)*enth3*(4*xi1*psi2+dx*psi1);
        end
        %---BL BCs
            va(v_n-2,v_n-2) = 1; vr(v_n-2) = 1 - vl_enth_prev(v_ny) - vl_enth(v_ny);
            
            va(v_n-1,v_n-1) = 1; vr(v_n-1) = 2 - vl_u_prev(v_ny) - vl_u(v_ny);
            
            va(v_n,v_n-4) = -v_dy*(del1-ramf1);
            va(v_n,v_n-3) = -4;
            va(v_n,v_n-1) = -v_dy*(del1-ramf1);
            va(v_n,v_n)   = 4;
            
            vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))...
                - 4*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1));

%             va(v_n-2,6) = 1;
%             vr(v_n-2) = 1 - vl_enth_prev(v_ny) - vl_enth(v_ny);
% 
%             va(v_n-1,6) = 1;
%             vr(v_n-1) = 2 - vl_u_prev(v_ny) - vl_u(v_ny);
% 
%             va(v_n,2) = -v_dy*(del1-ramf1);
%             va(v_n,3) = -4;
%             va(v_n,5) = -v_dy*(del1-ramf1);
%             va(v_n,6)   = 4;
%             vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))...
%                 - 4*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1));

        %Pressure Elements
            va(v_n+1,1) = 2;
            va(v_n+1,2) = -(vl_u(1) + vl_u_prev(1));
            mc = 2;
            for j = 2:v_ny-1
                va(v_n+1,j+mc) = 4;
                mc = mc + 1;
                va(v_n+1,j+mc) = -2*(vl_u(j) + vl_u_prev(j));
                mc = mc + 1;
            end
            va(v_n+1,v_n-2) = 2;
            va(v_n+1,v_n-1) = -(vl_u(v_ny) + vl_u_prev(v_ny));
            va(v_n+1,v_n+1) = -(4*((gam*c3)^(1/2)))/(v_dy*(gam-1));

        % Pressure Elements
%             pv(1) = 2;
%             pv(2) = -(vl_u(1) + vl_u_prev(1));
%             mc = 2;
%             for j = 2:v_ny-1
%                 pv(j+mc) = 4;
%                 mc = mc + 1;
%                 pv(j+mc) = -2*(vl_u(j) + vl_u_prev(j));
%                 mc = mc + 1;
%             end
%             pv(v_n-2) = 2;
%             pv(v_n-1) = -(vl_u(v_ny) + vl_u_prev(v_ny));
%             pv(v_n+1) = -(4*((gam*c3)^(1/2)))/(v_dy*(gam-1));
            
        %Pressure RHS
            rp = ( 2*(vl_enth_prev(1) + vl_enth(1)) - 0.5*((vl_u_prev(1) + vl_u(1))^2) )...
                + ( 2*(vl_enth_prev(v_ny) + vl_enth(v_ny)) - 0.5*((vl_u_prev(v_ny) + vl_u(v_ny))^2) );
            for j = 2:v_ny-1
                rp = rp + 2*( 2*(vl_enth_prev(j) + vl_enth(j)) - 0.5*((vl_u_prev(j) + vl_u(j))^2) );
            end

            vr(v_n+1) = ((4*((gam*c3)^(1/2)))/(v_dy*(gam-1)))*p1 - rp;

%             vr_full = vr;

    %---------------------------------------------------------------------
%     for m = v_n+1:-1:1
%         for ii = m-1:-1:1
%             vr_full(ii) = vr_full(ii)-( (va_full(ii,m)*vr_full(m))/va_full(m,m) );
%             for k = 1:v_n+1
%                 va_full(ii,k) = va_full(ii,k) - ( (va_full(ii,m)*va_full(m,k))/va_full(m,m) );
%             end
%         end
%     end
    
    %%%
% 	vx(1:v_n+1) = 0.d0;
% 
%     vx(1) = vr(1)/va(1,1)
% 
%     do i = 2, v_n+1
%         t = vr(i)
%         do k = 1,i
%             t = t - ( va(i,k)*vx(k) )
%         enddo
%         vx(i) = t/va(i,i)
%     enddo
    %---------------------------------------------------------------------
%     
%     va(v_n-2,2:9) = va(v_n-2,2:9) + va(v_n-1,1:8);
%     vr(v_n-2) = vr(v_n-2) + vr(v_n-1);
%     
%     jj = 1;
%     mn = v_n-4:-3:5;
%     for k = v_n-4:-1:4
%         if jj <= length(mn) && k == mn(jj)
%             vr(k) = vr(k) - (va(k,9)*vr(k+1))/va(k+1,8);
%             va(k,2:9) = va(k,2:9) - (va(k,9)*va(k+1,1:8))/va(k+1,8);
%             
%             jj = jj + 1;
%         end
%         vr(k+1) = vr(k+1) - (va(k+1,8)*vr(k+2))/va(k+2,7);
%         va(k+1,2:8) = va(k+1,2:8) - (va(k+1,8)*va(k+2,1:7))/va(k+2,7);
% 
%         vr(k+2) = vr(k+2) - (va(k+2,7)*vr(k+3))/va(k+3,6);
%         va(k+2,2:7) = va(k+2,2:7) - (va(k+2,7)*va(k+3,1:6))/va(k+3,6);
%     end
%     vr(4) = vr(4) - (va(4,8)*vr(5))/va(5,7);
%     va(4,2:8) = va(4,2:8) - (va(4,8)*va(5,1:7))/va(5,7);
%     
%     vr(5) = vr(5) - (va(5,7)*vr(6))/va(6,6);
%     va(5,2:7) = va(5,2:7) - (va(5,7)*va(6,1:6))/va(6,6);
%     
%     vr(4) = vr(4) - (va(4,7)*vr(5))/va(5,6);
%     va(4,2:7) = va(4,2:7) - (va(4,7)*va(5,1:6))/va(5,6);
%     
%     vr(1) = vr(1) - (va(1,9)*vr(4))/va(4,6);
%     va(1,6:9) = va(1,6:9) - (va(1,9)*va(4,3:6))/va(4,6);
%     
%     vr(1) = vr(1) - (va(1,8)*vr(3))/va(3,6);
%     va(1,6:8) = va(1,6:8) - (va(1,8)*va(3,4:6))/va(3,6);
%     
%     vr(1) = vr(1) - (va(1,7)*vr(2))/va(2,6);
%     va(1,6:7) = va(1,6:7) - (va(1,7)*va(3,5:6))/va(2,6);
    %stop
%             for k = v_n-4:-3:5
%                 vr(k) = vr(k) - (va(k,9)*vr(k+1))/va(k+1,8);
%                 va(k,2:9) = va(k,2:9) - (va(k,9)*va(k+1,1:8))./va(k+1,8);
%             end
%             vr(1) = vr(1) - (va(1,9)*vr(5))/va(5,5);
%             va(1,6) = va(1,6) - (va(1,9)*va(5,2))/va(5,5);
%             va(1,9) = va(1,9) - (va(1,9)*va(5,5))/va(5,5);
%             for k = v_n-3:-1:4
%                 if k == v_n-3
%                     vr(k) = vr(k) - (va(k,8)*vr(k+2))/va(k+2,6);
%                     va(k,8) = va(k,8) - (va(k,8)*va(k+2,6))/va(k+2,6);
%                 else
%                     vr(k) = vr(k) - (va(k,8)*vr(k+1))/va(k+1,7);
%                     va(k,2:8) = va(k,2:8) - (va(k,8)*va(k+1,1:7))./va(k+1,7);
%                 end
%             end
%             for k = v_n-2:-1:4
%                 vr(k) = vr(k) - (va(k,7)*vr(k+1))/va(k+1,6);
%                 va(k,2:7) = va(k,2:7) - (va(k,7)*va(k+1,1:6))./va(k+1,6);
%             end
%             stop
            %----
%             xs(1) = vr(1)/va(1,6);
%             xs(2) = vr(2)/va(2,6);
%             xs(3) = vr(3)/va(3,6);
%             xs(4) = (vr(4) - va(4,3)*xs(1) - va(4,4)*xs(2) - va(4,5)*xs(3))/va(4,6);
%             xs(5) = (vr(5) - va(5,2)*xs(1) - va(5,3)*xs(2) - va(5,4)*xs(3) - va(5,5)*xs(4))/va(5,6);
%             
%             for k = 6:v_n
%                 xssum = 0;
%                 for l = 1:5
%                     xssum = xssum + va(k,l)*xs(k-l);
%                 end
%                 xs(k) = (vr(k) - xssum)/va(k,6);
%             end
%             
%             nsum = 0; dsum = 0;
%             for k = 1:v_n
%                 nsum = nsum + ((pv(k)*xs(k))); %/va(k,6));
%                 dsum = dsum + ((pv(k)*vp(k))/va(k,6));
%             end
%             vx(v_n+1) = (vr(v_n+1) - nsum)/(pv(v_n+1) - dsum);
%             for k = 1:v_n
%                 vx(k) = xs(k) - ((vp(k)*vx(v_n+1))/va(k,6));
%             end
% 
% xx = zeros(v_n,1); ss = zeros(v_n,1);
% 
% xx(1) = vp(1)/va(1,6); %XX(1)=A_end(1)/A(1,6);
% ss(1) = vr(1)/va(1,6); %SS(1)=Rf(1)/A(1,6);
% t3 = pv(1)*xx(1); %t3=P_end(1)*XX(1);
% t4 = pv(1)*ss(1); %t4=P_end(1)*SS(1);
% ii=0;
% 
% for k=2:v_n
%     t1 = vp(k); %t1=A_end(k);
%     t2 = vr(k); %t2=Rf(k);
%     for j=1:5
%         t1 = t1 - va(k,j)*xx(j+ii); %t1=t1-A(k,j)*XX(j+ii);
%         t2 = t2 - va(k,j)*ss(j+ii); %t2=t2-A(k,j)*SS(j+ii);
%     end
%     xx(k) = t1/va(k,6); %XX(k)=t1/A(k,6);
%     ss(k) = t2/va(k,6); %SS(k)=t2/A(k,6);
%     t3 = t3 + pv(k)*xx(k); %t3=t3+P_end(k)*XX(k);
%     t4 = t4 + pv(k)*ss(k); %t4=t4+P_end(k)*SS(k);
% 
%     if k>=6
%         ii=ii+1;
%     end
% end
% 
% vx(v_n+1) = (vr(v_n+1)-t4)/(pv(v_n+1) - t3); %x_out(n+1)=(Rf_end-t4)/(P_end(n+1)-t3);
% vx(1:v_n) = ss(1:v_n) - xx(1:v_n).*vx(v_n+1); %x_out(1:n)=SS(1:n)-XX(1:n).*x_out(n+1);
% 
% %H_out=u_K(1:3:n)+x_out(1:3:n);
% %u_out=u_K(2:3:n)+x_out(2:3:n);
% %psi_out=u_K(3:3:n)+x_out(3:3:n);
% %p_out=p+x_out(n+1);

            va = sparse(va);
            vx = va\vr;
    %---------------------------------------------------------------------

            mc = 0;
            vl_enth_out = zeros(v_ny,1); vl_u_out = zeros(v_ny,1); vl_psi_out = zeros(v_ny,1);
            for j = 1:v_ny
                vl_enth_out(j) = vl_enth(j) + vx(j+mc);
                mc = mc + 1;
                vl_u_out(j) = vl_u(j) + vx(j+mc);
                mc = mc + 1;
                vl_psi_out(j) = vl_psi(j) + vx(j+mc);
            end
            vl_pres_out = vl_pres(i) + vx(v_n+1);
%             figure(10)
%             plot(vl_enth_out,0:1/500:1); title('Enthalpy')
%             figure(20)
%             plot(vl_u_out,0:1/500:1); title('Str Vel')
%             figure(30)
%             plot(vl_psi_out,0:1/500:1); title('Str Func')
%             disp(vl_pres_out)
%             pause

            if vl_pres_out < 0
                %pres_res = 1.d-9;
                vl_pres_prev(1:i-1) = vl_pres(1:i-1);
                vl_pres_prev(i:end-1) = vl_pres(i-1);
                %disp(vl_pres_out)
                fprintf('restart sweep at x = %d\n\n', i)
                br = 1;
                break
            else
                pres_res = abs(vx(v_n+1));
                vl_enth = vl_enth_out;
                vl_u = vl_u_out;
                vl_psi = vl_psi_out;
                vl_pres(i) = vl_pres_out;
            end
            
        end

        if br == 1
            break
        end
        if incres == 1
            vl_enth_save(:,i) = vl_enth(:);
            vl_u_save(:,i) = vl_u(:);
            vl_psi_save(:,i) = vl_psi(:);
        end
        
        if incres == 0
            figure(1)
            plot(nvec,vl_pres,'b')
            hold on
            plot(nvec(i),vl_pres(i),'or')
            hold off
            
            figure(2)
            xip = (c1./(nvec'.^(1/2))).*vl_pres;
            plot(nvec,xip,'b')
            hold on
            plot(nvec(i),xip(i),'or')
            hold off
        else
            figure(1)
            plot(0:dx:1,vl_pres,'b')
            hold on
            plot(dx*(i-1),vl_pres(i),'or')
            hold off
            
            figure(2)
            xip = (c1./((0:dx:1).^(1/2))).*vl_pres;
            plot(0:dx:1,xip,'b')
            hold on
            plot(dx*(i-1),xip(i),'or')
            hold off
        end
        
        pause(0.00001)

    end
    if br ~= 1
        pres_con = max(abs(vl_pres_prev - vl_pres));
        fprintf('sweep = %d,  pres_diff = %.8f\n',sweep,pres_con)
        vl_pres_prev = vl_pres;
    else
        br = 0;
    end

end

%%%
vl_enth_save(:,1) = vl_ip_enth;
vl_u_save(:,1) = vl_ip_u;
vl_psi_save(:,1) = vl_ip_psi;

% vl_dens = zeros(v_ny,nx);
% vl_temp = zeros(v_ny,nx);
% vl_mu = zeros(v_ny,nx);
% vl_v = zeros(v_ny,nx);
% eta = zeros(v_ny,nx);
% 
% vl_dens(:,1) = (2*gam*c3)./((gam-1).*(2.*vl_enth_save(:,1) - vl_u_save(:,1).^2));
% vl_temp(:,1) = c3./vl_dens(:,1);
% vl_mu(:,1) = gam.*vl_temp(:,1);
% 
% eta(v_ny,:) = 1;
% for j = 2:v_ny-1
% 	eta(j,1) = eta(j-1,1) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,1))+(1/vl_dens(j-1,1))));
% end
% vl_v(:,1) = 0.75.*eta(:,1).*vl_u_save(:,1) - (0.25.*sqrt(gam*c3).*vl_psi_save(:,1))./vl_dens(:,1);
% 
% for i = 2:nx
%     vl_dens(:,i) = (2*gam*c3*vl_pres(i))./((gam-1).*(2.*vl_enth_save(:,i) - vl_u_save(:,i).^2));
%     vl_temp(:,i) = (c3*vl_pres(i))./vl_dens(:,i);
%     vl_mu(:,i) = gam.*vl_temp(:,i);
%     for j = 2:v_ny-1
%         eta(j,i) = eta(j-1,i) + (sqrt(gam*c3)*v_dy*0.5*((1/vl_dens(j,i))+(1/vl_dens(j-1,i))));
%     end
%     vl_v(:,i) = 0.75.*eta(:,i).*vl_u_save(:,i).*vl_del(i)...
%         + ((i-1)*dx).*eta(:,i).*vl_u_save(:,i).*((vl_del(i) - vl_del(i-1))/dx)...
%         - (sqrt(gam*c3)./(4.*vl_dens(:,i))).*( vl_psi_save(:,i) + (4*((i-1)*dx).*((vl_psi_save(:,i) - vl_psi_save(:,i-1))./dx)) );
% end

%if deforms == 1
%    cit1 = sprintf('chi%d/Pr0725/sine/convex/1000pts',chi*10);
%    mkdir(cit1,'1')
    fie1 = 'datadump/';%strcat(cit1,'/1');
    sepf = sprintf('_%d_%d',s_p1,e_p1);

%elseif deforms == 2
%    cit2 = sprintf('chi%d/Pr0725/semicircle/convex/1000pts',chi*10);
%    mkdir(cit2,'1')
%    fie = strcat(cit2,'/1');
%end
xip = (c1./((0:dx:1).^(1/2))).*vl_pres;
xid = c2.*((0:dx:1).^(3/4)).*vl_del';

%parsav(fie,vl_pres,xip,vl_del,xid,ramp_func,vl_dens,vl_temp,vl_mu,vl_v,vl_enth_save,vl_u_save,vl_psi_save)
%%%
%save(strcat(fie1,'/den_bar.mat'),'vl_dens')
%save(strcat(fie1,'/temp_bar.mat'),'vl_temp')
%save(strcat(fie1,'/mu_bar.mat'),'vl_mu')
%save(strcat(fie1,'/v_bar.mat'),'vl_v')
save(strcat(fie1,'/sc_enth',sepf,'.mat'),'vl_enth_save')
save(strcat(fie1,'/sc_u',sepf,'.mat'),'vl_u_save')
save(strcat(fie1,'/sc_sf',sepf,'.mat'),'vl_psi_save')
%save(strcat(fie1,'/cons.mat'),'chi','c1','c2')
save(strcat(fie1,'/sc_p',sepf,'.mat'),'vl_pres')
%xip = (c1./((0:dx:1).^(1/2))).*vl_pres;
%save(strcat(fie1,'/p_nd.mat'),'xip')
save(strcat(fie1,'/sc_bl',sepf,'.mat'),'vl_del')
%xid = c2.*((0:dx:1).^(3/4)).*vl_del';
%save(strcat(fie1,'/bl_nd.mat'),'xid')
%save(strcat(fie1,'/rampf.mat'),'ramp_func')
% %
% save(strcat(fie2,'/den_bar.mat'),'vl_dens')
% save(strcat(fie2,'/t_bar.mat'),'vl_temp')
% save(strcat(fie2,'/mu_bar.mat'),'vl_mu')
% save(strcat(fie2,'/v_bar.mat'),'vl_v')
% save(strcat(fie2,'/u_bar.mat'),'vl_u_save')
% save(strcat(fie2,'/sf_bar.mat'),'vl_psi_save')
% save(strcat(fie2,'/cons.mat'),'chi','c1','c2')
% save(strcat(fie2,'/p_bar.mat'),'vl_pres')
% save(strcat(fie2,'/p_nd.mat'),'xip')
% save(strcat(fie2,'/bl_bar.mat'),'vl_del')
% save(strcat(fie2,'/bl_nd.mat'),'xid')
% save(strcat(fie2,'/rampf.mat'),'ramp_func')
%end