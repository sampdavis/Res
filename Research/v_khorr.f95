program kmethod

    implicit none

    double precision, allocatable :: A(:,:), x(:), R(:), up(:)
    double precision, allocatable :: enth(:), vel(:), psi(:), eta(:)
    double precision, allocatable :: enth_out(:), vel_out(:), psi_out(:)
    double precision :: Pr,h,gam,c6,su,t,d1,d2,d3,d,c3t,c3,c3_prev,c3diff
    double precision :: entherr, velerr, psierr, errf, errf_prev
    integer :: n,i,l,k,m,q,cou,f,fid,nn,mc,pts,check
    character*15 :: str

    Pr = 1.d0
    !Pr = 725.d-3

    cou = 0; check = 0; fid = 11; c3_prev = 0.d0; errf = 1.d10

    pts = 0; nn = 1001 + pts; n = 3*nn; h = 10.d0/(nn-1); gam = 14.d-1
    
    c6 = 4.d0*(h**2.d0)*((gam-1.d0)/gam)

    allocate(A(1:n,1:n)); allocate(x(1:n)); allocate(R(1:n))
    allocate(up(1:n)); allocate(enth(1:nn)); allocate(vel(1:nn))
    allocate(psi(1:nn)); allocate(eta(1:nn)); allocate(enth_out(1:nn))
    allocate(vel_out(1:nn)); allocate(psi_out(1:nn))

    do i = 1,nn
        eta(i) = (i-1)*h
    enddo
!-----------------------------------------------------------------
!Initialize up-hat elements
!-----------------------------------------------------------------

	do i = 1,nn
        !enth(i) = 5.d-1 + (eta(i)**2.d0)*exp(-2.d0*eta(i))             !Energy (Adiabatic, Pr=1)
        !enth(i) = 4178.d-4 + eta(i)*(1.d-1*exp(-2.d-1*eta(i)))         !Energy (Adiabatic, Pr=0.725)
        !enth(i) = 4.d-1 + eta(i)*(7.d-1*exp(-2.d-1*eta(i)))            !Energy (H=0.4)
        !enth(i) = 3.d-1 + eta(i)*(7.d-1*exp(-4.d-1*eta(i)))            !Energy (H=0.3)
        !enth(i) = 2.d-1 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.2)
        !enth(i) = 1.d-1 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.1)
        !enth(i) = 75.d-3 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))           !Energy (H=0.075)
        !enth(i) = 5.d-2 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.05)
        !enth(i) = 25.d-3 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))           !Energy (H=0.025)
        !enth(i) = 1.d-2 + eta(i)*(7.d-1*exp(-27.d-2*eta(i)))           !Energy (H=0.01)
        
        enth(i) = 495.d-3 + (eta(i)**2.d0)*exp(-2.d0*eta(i))
        vel(i)  = 1.d0 - exp(-2.d0*eta(i))                             !Momentum
        psi(i)  = eta(i) + 5.d-1*(exp(-2.d0*eta(i)) - 1.d0)            !StreamFunc
	enddo

!-----------------------------------------------------------------
! Loop start
!-----------------------------------------------------------------
1   cou = cou + 1

    A(1:n,1:n) = 0.d0
    x(1:n) = 0.d0
    R(1:n) = 0.d0

    mc = 0
	do i =1,nn
        up(i+mc) = enth(i)
        mc = mc + 1
        up(i+mc) = vel(i)
        mc = mc + 1
        up(i+mc) = psi(i)
	enddo

!-----------------------------------------------------------------
!Matrix initialization
!-----------------------------------------------------------------

    A(1,1) = 1.d0
    !A(1,4) = -1.d0                         !<------------uncomment for adiabatic
    A(2,2) = 1.d0
    A(3,3) = 1.d0

	do i = 4,n-5,3

!Steamfunction elements
        A(i,i-2) = h
        A(i,i-1) = 2.d0
        A(i,i+1) = h
        A(i,i+2) = -2.d0

!Momentum elements
        A(i+1,i-2) = h*up(i+2) - 8.d0                   
        A(i+1,i)   = -c6
        A(i+1,i+1) = c6*up(i+1) + 16.d0
        A(i+1,i+2) = -h*(up(i+4) - up(i-2))
        A(i+1,i+4) = -(h*up(i+2) + 8.d0)

!Energy elements
        A(i+2,i-3) = h*Pr*up(i+2) - 8.d0
        A(i+2,i-2) = 4.d0*(Pr-1.d0)*((up(i+4) - up(i-2)) - 2.d0*up(i+1))
        A(i+2,i)   = 16.d0
        A(i+2,i+1) = -8.d0*(Pr-1.d0)*((up(i+4) - 2.d0*up(i+1) + up(i-2)) - 2.d0*up(i+1))
        A(i+2,i+2) = -h*Pr*(up(i+3) - up(i-3))
        A(i+2,i+3) = -(h*Pr*up(i+2)+8.d0)
        A(i+2,i+4) = -4.d0*(Pr-1.d0)*((up(i+4) - up(i-2)) + 2.d0*up(i+1))
    enddo

    A(n-2,n-2) = 1.d0
    A(n-1,n-1) = 1.d0
    A(n,n-4)   = h
    A(n,n-3)   = 2.d0
    A(n,n-1)   = h
    A(n,n)     = -2.d0

!------------------------------------------------------------------
!Right Hand Side
!------------------------------------------------------------------

    !R(1) = enth(2) - enth(1)                         !<------------Change to this for adiabatic
    R(1) = 495.d-3 - enth(1)                           !<------------Change this for non-adiabatic
    R(2) = -vel(1)
    R(3) = -psi(1)

    mc = 2

    do i = 4,n-5,3
        !S
        R(i) = 2.d0*(psi(mc+1) - psi(mc)) - h*(vel(mc+1) + vel(mc))

        !M
        R(i+1) = (C6/2.d0)*(2.d0*enth(mc) - vel(mc)**2.d0) + h*psi(mc)*(vel(mc+1) - vel(mc-1))&
               + 8.d0*(vel(mc+1) - 2.d0*vel(mc) + vel(mc-1))

        !E
        R(i+2) = 2.d0*(Pr - 1.d0)*(4.d0*(vel(mc+1) - 2.d0*vel(mc) + vel(mc-1))*vel(mc)&
               + (vel(mc+1) - vel(mc-1))**2.d0) + h*Pr*psi(mc)*(enth(mc+1) - enth(mc-1))&
               + 8.d0*(enth(mc+1) - 2.d0*enth(mc) + enth(mc-1))

        mc = mc + 1
    enddo

    R(n-2) = 5.d-1 - enth(nn)
    R(n-1) = 1.d0 - vel(nn)
    R(n)   = 2.d0*(psi(nn) - psi(nn-1)) - h*(vel(nn) + vel(nn-1))

!---------------------------------------------------------------------
!Gaussian elimination
!---------------------------------------------------------------------

	do m = n,1,-1

		do i = m-1,1,-1

            R(i) = R(i)-( (A(i,m)*R(m))/A(m,m) )

			if (m >= n-2) then
				do k = n-10,n
!				do k = 1,n
                    A(i,k) = A(i,k) - ( (A(i,m)*A(m,k))/A(m,m) )

				enddo
			elseif (m < n-2 .and. m >= 11) then
				do k = m-10,m+3

                    A(i,k) = A(i,k) - ( (A(i,m)*A(m,k))/A(m,m) )

				enddo
			elseif (m < 11) then
				do k = 1,m+3

                    A(i,k) = A(i,k) - ( (A(i,m)*A(m,k))/A(m,m) )

				enddo
			endif

		enddo

	enddo

!------------------------------------------------------------------------------
!Forward Substitution
!------------------------------------------------------------------------------

    x(1) = R(1)/A(1,1)

	do i = 2,n
        t = R(i)
		do k = 1,i-1
            t = t - ( A(i,k)*x(k) )
		enddo
        x(i) = t/A(i,i)
	enddo

!----------------------------------------------------------------------------
! Update uphat values
!----------------------------------------------------------------------------
    mc = 0
	do i = 1,nn
        enth_out(i) = enth(i) + x(i+mc)
        mc = mc + 1
        vel_out(i) = vel(i) + x(i+mc)
        mc = mc + 1
        psi_out(i) = psi(i) + x(i+mc)
	enddo

    errf_prev = errf
    errf = maxval(dabs(x(1:n)))

	if (errf < errf_prev) then
        enth(1:nn) = enth_out(1:nn)
        vel(1:nn) = vel_out(1:nn)
        psi(1:nn) = psi_out(1:nn)
	else
        check = 1
	endif

	if (cou > 30) then
        print *, "Not converging"
        read *, f
		do i = 1,nn
            print *, enth(i), vel(i), psi(i)
		enddo
        read *, f
	endif

	if (check /= 1) goto 1

    open(unit=fid, file='vl_pr1adi_near_1000.dat', status='new', action='write')
    do i = 1,nn
                    !Enthalpy, Velocity, Streamfunction
        write(fid,*), enth(i),     vel(i),      psi(i)
    enddo
    close(unit=fid)
    fid = fid + 1

!--------------
! c3
!--------------
    c3t = 5.d-1*(2.d0*enth(1) - vel(1)**2.d0)
    do i = 2,nn-1
        c3t = c3t + (2.d0*enth(i) - vel(i)**2.d0)
    enddo
    c3t = c3t + 5.d-1*(2.d0*enth(nn) - vel(nn)**2.d0)

    c3 = (((h**2.d0)*((gam-1.d0)**2.d0))/(4.d0*gam))*(c3t**2.d0)
    
    open(unit=99, file='c3con_pr1adi_near_1000.dat', status='new', action='write')
    write(99,*), c3
    close(unit=99) 

    deallocate(A); deallocate(x); deallocate(R); deallocate(up)
    deallocate(enth); deallocate(vel); deallocate(psi); deallocate(eta)
    deallocate(enth_out); deallocate(vel_out); deallocate(psi_out)

end program