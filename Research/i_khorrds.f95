program inv_khorrds
    implicit none

    double precision, allocatable :: ia(:,:),ir(:),ix(:),il_eta_prev(:),il_u_prev(:),il_v_prev(:)
    double precision, allocatable :: il_rho_prev(:),il_p_prev(:),il_eta(:),il_u(:),il_v(:),il_rho(:)
    double precision, allocatable :: il_p(:),il_v_out_prev(:),il_rho_out_prev(:),il_p_out_prev(:),vl_psi_prev(:)
    double precision, allocatable :: il_rho_out(:),il_v_out(:),il_p_out(:),vl_enth_prev(:),vl_u_prev(:)
    double precision :: v_dy,i_dy,Pr,chi,gam,c,c1,c2,c3,c5,z,z_prev,err_out_max,err_out_max_old
    double precision :: rho1,rho2,rho3,v1,v2,v3,p1,p2,p3,g1,g2,del1,del2,eta1,xi1,lam,err_out(4)
    double precision :: il_del,il_del_prev,il_g,il_g_prev,il_g_out_prev,il_g_out,dx,t,f1,f2,f3
    double precision :: c10,f4,f5,f6,f7,f8,f9,f10,f11,v4,p4,rho4,xi2,xi3,xi4
    integer :: nx,v_ny,v_n,i_ny,i_n,i_fid,e,icount,mc,i,j,k,m,v_fid,f,ii,ij
    character*40 :: str
    character(*), parameter :: vlip = 'c:\users\sampson\documents\github\res\research\vl_ip\' !'
    character(*), parameter :: ilip = 'c:\users\sampson\documents\github\res\research\il_ip\' !'
    character(*), parameter :: vlds = 'c:\users\sampson\documents\github\res\research\vl_ds\' !'
    character(*), parameter :: ilds = 'c:\users\sampson\documents\github\res\research\il_ds\' !'

    v_ny = 501; v_n = 3*v_ny; v_dy = 10.d0/dble(v_ny - 1)
    i_ny = 501; i_n = 3*i_ny; i_dy = 1.d0/dble(i_ny - 1)
    nx = 501; dx = 1.d0/(nx-1.d0)

    v_fid = 11; i_fid = 12

    Pr = 1.d0; chi = 1.d0; gam = 14.d-1; c = 1.d0

    open(unit=9, file=vlip//"c3con_pr1001.dat", status="old", action="read")
    open(unit=10, file=ilip//"c5con_khm_500.dat", status="old", action="read")
    read(9,*), c3
    read(10,*), c5
    close(unit=9)
    close(unit=10)

    allocate(il_u_prev(1:i_ny)); allocate(il_v_prev(1:i_ny))
    allocate(il_rho_prev(1:i_ny)); allocate(il_p_prev(1:i_ny))
    allocate(ia(1:i_n+1,1:i_n+1))
    allocate(ir(1:i_n+1)); allocate(ix(1:i_n+1))
    allocate(il_eta(1:i_ny)); allocate(il_v(1:i_ny))
    allocate(il_rho(1:i_ny)); allocate(il_p(1:i_ny))
    allocate(il_v_out_prev(1:i_ny)); allocate(il_rho_out_prev(1:i_ny))
    allocate(il_p_out_prev(1:i_ny)); allocate(il_v_out(1:i_ny))
    allocate(il_rho_out(1:i_ny)); allocate(il_p_out(1:i_ny))
    allocate(vl_enth_prev(1:v_ny)); allocate(vl_u_prev(1:v_ny))
    allocate(vl_psi_prev(1:v_ny))

    e = 0
1   e = e + 1
    print *, "downstream step ", e
    z = dble(e)*dx
    z_prev = dble(e-1)*dx

	if (e == 1) then
        !----------------Viscous Layer (IP)-----------------------
        open(unit=v_fid, file=vlip//'vl_pr1001.dat', status='old', action='read')
        do j = 1,v_ny
            read(v_fid,*), vl_enth_prev(j), vl_u_prev(j), vl_psi_prev(j)
        enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        !----------------Inviscid Layer (IP)----------------------
        open(unit=i_fid, file=ilip//"il_khm_500.dat", status="old", action="read")
        do j = 1,i_ny
            il_eta(j) = dble(j-1)*i_dy
            read(i_fid,*), il_v_prev(j), il_rho_prev(j), il_p_prev(j)
        enddo
        close(unit = i_fid)
        i_fid = i_fid + 2

        il_del_prev = 1.d0; il_g_prev = 1.d0

        c2 = ((c*c3)/il_p_prev(1))**(1.d0/4.d0)
        c1 = (c*c3)/(c2*c2)
	else
        !----------------Inviscid Layer (DS)-----------------------
        write(str, "('il_ds_',i3.3,'.dat')"), e-1
        open(unit=i_fid, file=ilds//str, status='old', action='read')
        do j = 1,i_ny
            read(i_fid,*), il_v_prev(j), il_rho_prev(j), il_p_prev(j), il_del, il_g
        enddo
        close(unit=i_fid)
        i_fid = i_fid + 2
	endif
!---------------------------------------------------------------
!                  Initial Guesses (Inviscid)
!---------------------------------------------------------------

    il_del = 99.d-2

    il_v(1:i_ny) = il_v_prev(1:i_ny)
    il_rho(1:i_ny) = il_rho_prev(1:i_ny)
    il_p(1:i_ny) = il_p_prev(1:i_ny)
    il_g = il_g_prev
    err_out_max_old = 1.d2
!---------------------------------------------------------------
!                         Loop Start
!---------------------------------------------------------------
    icount = 0
2   icount = icount + 1
    print *, icount

!---------------------------------------------------------------
!                   Matrix Elements and RHS
!---------------------------------------------------------------

    ia(1:i_n+1,1:i_n+1) = 0.d0; ir(1:i_n+1) = 0.d0

    ia(1,1) = 1.d0

    ir(1) = ((z+z_prev)/dx)*(il_del-il_del_prev) + 75.d-2*(il_del+il_del_prev) - il_v(1) - il_v_prev(1)

    mc = 0

    do j = 2, i_ny
        rho1 = il_rho(j)+il_rho(j-1)+il_rho_prev(j)+il_rho_prev(j-1)
        rho2 = il_rho(j)+il_rho(j-1)-il_rho_prev(j)-il_rho_prev(j-1)
        rho3 = il_rho(j)-il_rho(j-1)+il_rho_prev(j)-il_rho_prev(j-1)
        v1 = il_v(j)+il_v(j-1)+il_v_prev(j)+il_v_prev(j-1)
        v2 = il_v(j)+il_v(j-1)-il_v_prev(j)-il_v_prev(j-1)
        v3 = il_v(j)-il_v(j-1)+il_v_prev(j)-il_v_prev(j-1)
        p1 = il_p(j)+il_p(j-1)+il_p_prev(j)+il_p_prev(j-1)
        p2 = il_p(j)+il_p(j-1)-il_p_prev(j)-il_p_prev(j-1)
        p3 = il_p(j)-il_p(j-1)+il_p_prev(j)-il_p_prev(j-1)
        g1 = il_g+il_g_prev; g2 = il_g-il_g_prev
        del1 = il_del+il_del_prev; del2 = il_del-il_del_prev
        xi1 = z+z_prev; eta1 = il_eta(j)+il_eta(j-1)

!-----------------------Khorrami's M and RHS elements (corrected)------------------------
!----------------------------------------------------------------------------------------
        ! xi2 = (xi1*i_dy)/dx; xi3 = c5*((xi1/dx) + 75.d-2)
        ! c10 = 4.d0/(gam+1.d0)
        ! f1 = del1 - c5*g1; f2 = (i_dy/4.d0) - xi2
        ! f3 = (xi1/dx)*(eta1*(del2 - c5*g2) - 2.d0*del2) + 75.d-2*eta1*f1 - 15.d-1*del1 + v1
        ! f4 = 25.d-2*i_dy*v1 - xi2*v2; f5 = f1*f4 + f3*v3
        ! f6 = p1*rho3 - gam*rho3*p1; f7 = xi2*(rho1*p2 - gam*rho2*p1) - 5.d-1*i_dy*rho1*p1
        ! f8 = f1*(xi2*(gam*p1-p2) + 5.d-1*i_dy*p1) + f3*p3
        ! f9 = f1*(xi2*(gam*rho2-rho1) + 5.d-1*i_dy*rho1) - gam*f3*rho3

        ! !continuity
        ! ia(j+mc,(j+mc)-1) = rho3 - rho1
        ! ia(j+mc,j+mc)     = v3 - f3 - xi2*f1
        ! ia(j+mc,(j+mc)+2) = rho3 + rho1
        ! ia(j+mc,(j+mc)+3) = v3 + f3 - xi2*f1
        ! ia(j+mc,i_n+1)   = c5*xi2*rho2 - eta1*xi3*rho3
        
        ! ir(j+mc) = xi2*rho2*f1 - rho3*f3 - v3*rho1

        ! mc = mc + 1
        ! !momentum
        ! ia(j+mc,(j+mc)-2) = rho1*(f3 - f1*f2 - v3)
        ! ia(j+mc,(j+mc)-1) = - f5
        ! ia(j+mc,j+mc)     = 16.d0
        ! ia(j+mc,(j+mc)+1) = -rho1*(f3 + f1*f2 + v3)
        ! ia(j+mc,(j+mc)+2) = -f5
        ! ia(j+mc,(j+mc)+3) = -16.d0
        ! ia(j+mc,i_n+1)   = rho1*(c5*f4 + eta1*xi3*v3)

        ! ir(j+mc) = rho1*f5 + 16.d0*p3

        ! mc = mc + 1
        ! !energy   
        ! ia(j+mc,(j+mc)-3) = f6
        ! ia(j+mc,(j+mc)-2) = gam*p1*f3+f8
        ! ia(j+mc,(j+mc)-1) = -(rho1*f3-f9)
        ! ia(j+mc,j+mc)     = f6
        ! ia(j+mc,(j+mc)+1) = -(gam*p1*f3-f8)
        ! ia(j+mc,(j+mc)+2) =  rho1*f3+f9
        ! ia(j+mc,i_n+1)   = c5*f7 - eta1*xi3*f6

        ! ir(j+mc) = f1*f7 - f3*f6

!----------------------------Homebrew M and RHS elements---------------------------------
!----------------------------------------------------------------------------------------
        f1 = c5*g1 - del1
        f2 = del2*(eta1 - 2.d0) - eta1*c5*g2
        f3 = 3.d0*del1*(eta1-2.d0) - 3.d0*eta1*c5*g1 + 4.d0*v1

        !continuity
        ia(j+mc,(j+mc)-1) = 4.d0*dx*(rho3-rho1)
        ia(j+mc,j+mc)     = 4.d0*i_dy*xi1*f1 - 4.d0*xi1*f2 - dx*f3 + 4.d0*dx*v3
        ia(j+mc,(j+mc)+2) = 4.d0*dx*(rho3+rho1)
        ia(j+mc,(j+mc)+3) = 4.d0*i_dy*xi1*f1 + 4.d0*xi1*f2 + dx*f3 + 4.d0*dx*v3
        ia(j+mc,i_n+1)   = 4.d0*i_dy*xi1*c5*rho2 - 4.d0*xi1*eta1*c5*rho3 - 3.d0*dx*c5*eta1*rho3
        
        ir(j+mc) = -4.d0*i_dy*xi1*f1*rho2 - 4.d0*xi1*f2*rho3 - dx*f3*rho3 - 4.d0*dx*rho1*v3

        mc = mc + 1
        !momentum
        ia(j+mc,(j+mc)-2) = 4.d0*i_dy*rho1*xi1*f1 - 4.d0*rho1*xi1*f2 - rho1*dx*f3 + 4.d0*dx*rho1*v3 - i_dy*dx*f1*rho1 
        ia(j+mc,(j+mc)-1) = 4.d0*i_dy*v2*xi1*f1 + 4.d0*v3*xi1*f2 + v3*dx*f3 - i_dy*dx*f1*v1
        ia(j+mc,j+mc)     = -64.d0*dx
        ia(j+mc,(j+mc)+1) = 4.d0*i_dy*rho1*xi1*f1 + 4.d0*rho1*xi1*f2 + rho1*dx*f3 + 4.d0*dx*rho1*v3 - i_dy*dx*f1*rho1
        ia(j+mc,(j+mc)+2) = 4.d0*i_dy*v2*xi1*f1 + 4.d0*v3*xi1*f2 + v3*dx*f3 - i_dy*dx*f1*v1
        ia(j+mc,(j+mc)+3) = 64.d0*dx
        ia(j+mc,i_n+1)   = 4.d0*i_dy*xi1*c5*rho1*v2 - 4.d0*xi1*c5*eta1*rho1*v3&
                        - 3.d0*dx*eta1*c5*rho1*v3 - i_dy*dx*c5*rho1*v1

        ir(j+mc) = -4.d0*i_dy*xi1*rho1*v2*f1 - 4.d0*xi1*rho1*v3*f2 - dx*rho1*v3*f3&
                - 64.d0*dx*p3 + dx*i_dy*rho1*v1*f1

        mc = mc + 1
        !energy	
        ia(j+mc,(j+mc)-3) = 4.d0*dx*(rho1*p3 - gam*p1*rho3)
        ia(j+mc,(j+mc)-2) = 4.d0*i_dy*xi1*f1*(p2-gam*p1) + 4.d0*xi1*f2*(p3+gam*p1)&
                        + dx*f3*(p3+gam*p1) - 2.d0*i_dy*dx*f1*p1
        ia(j+mc,(j+mc)-1) = 4.d0*i_dy*xi1*f1*(rho1-gam*rho2) - 4.d0*xi1*f2*(rho1+gam*rho3)&
                        - dx*f3*(rho1+gam*rho3) - 2.d0*i_dy*dx*f1*rho1
        ia(j+mc,j+mc)     = 4.d0*dx*(rho1*p3 - gam*p1*rho3)
        ia(j+mc,(j+mc)+1) = 4.d0*i_dy*xi1*f1*(p2-gam*p1) + 4.d0*xi1*f2*(p3-gam*p1)&
                        + dx*f3*(p3-gam*p1) - 2.d0*i_dy*dx*f1*p1
        ia(j+mc,(j+mc)+2) = 4.d0*i_dy*xi1*f1*(rho1-gam*rho2) + 4.d0*xi1*f2*(rho1-gam*rho3)&
                        + dx*f3*(rho1-gam*rho3) - 2.d0*i_dy*dx*f1*rho1
        ia(j+mc,i_n+1) = 4.d0*i_dy*xi1*c5*(rho1*p2-gam*p1*rho2) - 4.d0*xi1*eta1*c5*(rho1*p3-gam*p1*rho3)&
                        - 3.d0*dx*eta1*c5*(rho1*p3-gam*p1*rho3) - 2.d0*dx*i_dy*c5*rho1*p1

        ir(j+mc) = -4.d0*i_dy*xi1*f1*(rho1*p2-gam*p1*rho2) - 4.d0*xi1*f2*(rho1*p3-gam*p1*rho3)&
                -dx*f3*(rho1*p3-gam*p1*rho3) + 2.d0*i_dy*dx*f1*rho1*p1

    enddo

!-----------------------------------2 point avrgd BCs------------------------------------
!----------------------------------------------------------------------------------------
    rho4 = il_rho(i_ny)+il_rho_prev(i_ny)
    v4 = il_v(i_ny)+il_v_prev(i_ny)
    p4 = il_p(i_ny)+il_p_prev(i_ny)
    g1 = il_g+il_g_prev; g2 = il_g-il_g_prev
    xi1 = z+z_prev; xi3 = c5*(75.d-1 + (xi1/dx))
    xi4 = ((5.d-1*xi1)**(1.d0/2.d0)) / (c2*c2*chi*chi*chi)
    f10 = 5.d-1*c5*(75.d-2*g1 + ((xi1*g2)/dx))
    f11 = rho4 - 2.d0*((gam+1.d0)/(gam-1.d0))
    c10 = 4.d0/(gam+1.d0)

    !Density
    ia(i_n-1,i_n-1) = f10*f10 + (2.d0/(gam-1.d0))*xi4
    ia(i_n-1,i_n+1) = xi3*f10*f11

    ir(i_n-1) = -(2.d0/(gam-1.d0))*xi4*rho4 - f10*f10*f11

    !Pressure
    ia(i_n,i_n) = 1.d0
    ia(i_n,i_n+1) = -c10*xi3*f10

    ir(i_n) = c10*(f10*f10 - ((gam-1.d0)/(2.d0*gam))*xi4) - p4

    !Trans Velocity
    ia(i_n+1,i_n-2) = f10
    ia(i_n+1,i_n+1) = 5.d-1*xi3*(v4 - c10*f10)
    
    ir(i_n+1) = c10*(f10*f10 - xi4) - v4*f10

!---------------------------------------------------------------
!               Gaussian Elimination
!---------------------------------------------------------------
    do m = i_n+1,1,-1
        do i = m-1,1,-1
            ir(i) = ir(i)-( (ia(i,m)*ir(m))/ia(m,m) )
            do k = 1,i_n+1
                ia(i,k) = ia(i,k) - ( (ia(i,m)*ia(m,k))/ia(m,m) )
            enddo
        enddo
    enddo
!---------------------------------------------------------------
!               Forward Substitution
!---------------------------------------------------------------
    ix(1:i_n+1) = 0.d0

    ix(1) = ir(1)/ia(1,1)
    do i = 2, i_n+1
        t = ir(i)
        do k = 1,i
            t = t - ( ia(i,k)*ix(k) )
        enddo
        ix(i) = t/ia(i,i)
    enddo
!---------------------------------------------------------------
!              Convergence Check & Uphat Update
!---------------------------------------------------------------
3   mc = 0
	do j = 1,i_ny
        il_v_out(j) = il_v(j) + ix(j+mc)
        mc = mc + 1
        il_rho_out(j) = il_rho(j) + ix(j+mc)
        mc = mc + 1
        il_p_out(j) = il_p(j) + ix(j+mc)
	enddo
    il_g_out = il_g + ix(i_n+1)
    print *, il_g_out

    il_v(1:i_ny) = il_v_out(1:i_ny)
    il_rho(1:i_ny) = il_rho_out(1:i_ny)
    il_p(1:i_ny) = il_p_out(1:i_ny)
    il_g = il_g_out
    
    write(str, "('il_ds_',i3.3,'.dat')"), icount
    open(unit=i_fid, file=ilds//str, status='new', action='write')
    do j = 1,i_ny
        write(i_fid,*), il_v(j), il_rho(j), il_p(j), il_del, il_g
    enddo
    close(unit=i_fid)
    i_fid = i_fid + 2

    if (icount < 10) goto 2

    deallocate(il_u_prev); deallocate(il_v_prev); deallocate(il_rho_prev)
    deallocate(il_p_prev); deallocate(ia); deallocate(ir); deallocate(ix)
    deallocate(il_eta); deallocate(il_v); deallocate(il_rho); deallocate(il_p)
    deallocate(il_v_out_prev); deallocate(il_rho_out_prev); deallocate(il_p_out_prev)
    deallocate(il_v_out); deallocate(il_rho_out); deallocate(il_p_out)
    deallocate(vl_enth_prev); deallocate(vl_u_prev); deallocate(vl_psi_prev)

end program