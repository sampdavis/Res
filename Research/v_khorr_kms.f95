program kmethod

    implicit none

    double precision, allocatable :: A(:,:), x(:), R(:), up(:)
    double precision, allocatable :: enth(:), vel(:), psi(:), eta(:)
    double precision, allocatable :: enth_out(:), vel_out(:), psi_out(:)
    double precision :: Pr,h,gam,c6,su,t,d1,d2,d3,d,c3t,c3,c3_prev,c3diff
    double precision :: entherr, velerr, psierr, errf, errf_prev, axsum
    integer :: n,i,l,k,m,q,cou,f,fid,nn,mc,pts,check
    character*15 :: str

    Pr = 1.d0
    !Pr = 725.d-3

    cou = 0; check = 0; fid = 11; c3_prev = 0.d0; errf = 1.d10

    pts = 0; nn = 501 + pts; n = 3*nn; h = 10.d0/(nn-1); gam = 14.d-1
    
    c6 = 4.d0*(h**2.d0)*((gam-1.d0)/gam)

    allocate(A(1:n,1:9)); allocate(x(1:n)); allocate(R(1:n))
    allocate(up(1:n)); allocate(enth(1:nn)); allocate(vel(1:nn))
    allocate(psi(1:nn)); allocate(eta(1:nn)); allocate(enth_out(1:nn))
    allocate(vel_out(1:nn)); allocate(psi_out(1:nn))

    do i = 1,nn
        eta(i) = (i-1)*h
    enddo
!-----------------------------------------------------------------
!Initialize up-hat elements
!-----------------------------------------------------------------

	do i = 1,nn
        !enth(i) = 5.d-1 + (eta(i)**2.d0)*exp(-2.d0*eta(i))             !Energy (Adiabatic, Pr=1)
        !enth(i) = 4178.d-4 + eta(i)*(1.d-1*exp(-2.d-1*eta(i)))         !Energy (Adiabatic, Pr=0.725)
        !enth(i) = 4.d-1 + eta(i)*(7.d-1*exp(-2.d-1*eta(i)))            !Energy (H=0.4)
        !enth(i) = 3.d-1 + eta(i)*(7.d-1*exp(-4.d-1*eta(i)))            !Energy (H=0.3)
        !enth(i) = 2.d-1 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.2)
        !enth(i) = 1.d-1 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.1)
        !enth(i) = 75.d-3 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))           !Energy (H=0.075)
        !enth(i) = 5.d-2 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))            !Energy (H=0.05)
        !enth(i) = 25.d-3 + eta(i)*(7.d-1*exp(-3.d-1*eta(i)))           !Energy (H=0.025)
        !enth(i) = 1.d-2 + eta(i)*(7.d-1*exp(-27.d-2*eta(i)))           !Energy (H=0.01)
        
        enth(i) = 495.d-3 + (eta(i)**2.d0)*exp(-2.d0*eta(i))             !Energy (99% Adiabatic)
        vel(i)  = 1.d0 - exp(-2.d0*eta(i))                             !Momentum
        psi(i)  = eta(i) + 5.d-1*(exp(-2.d0*eta(i)) - 1.d0)            !StreamFunc
	enddo

!-----------------------------------------------------------------
! Loop start
!-----------------------------------------------------------------
1   cou = cou + 1

    A(1:n,1:9) = 0.d0
    x(1:n) = 0.d0
    R(1:n) = 0.d0

    mc = 0
	do i =1,nn
        up(i+mc) = enth(i)
        mc = mc + 1
        up(i+mc) = vel(i)
        mc = mc + 1
        up(i+mc) = psi(i)
	enddo

!-----------------------------------------------------------------
!Matrix initialization
!-----------------------------------------------------------------

    A(1,6) = 1.d0
    !A(1,9) = -1.d0                         !<------------uncomment for adiabatic
    A(2,6) = 1.d0
    A(3,6) = 1.d0

	do i = 4,n-5,3

!Steamfunction elements
        A(i,4) = h
        A(i,5) = 2.d0
        A(i,7) = h
        A(i,8) = -2.d0

!Momentum elements
        A(i+1,3) = h*up(i+2) - 8.d0                   
        A(i+1,5)   = -c6
        A(i+1,6) = c6*up(i+1) + 16.d0
        A(i+1,7) = -h*(up(i+4) - up(i-2))
        A(i+1,9) = -(h*up(i+2) + 8.d0)

!Energy elements
        A(i+2,1) = h*Pr*up(i+2) - 8.d0
        A(i+2,2) = 4.d0*(Pr-1.d0)*((up(i+4) - up(i-2)) - 2.d0*up(i+1))
        A(i+2,4)   = 16.d0
        A(i+2,5) = -8.d0*(Pr-1.d0)*((up(i+4) - 2.d0*up(i+1) + up(i-2)) - 2.d0*up(i+1))
        A(i+2,6) = -h*Pr*(up(i+3) - up(i-3))
        A(i+2,7) = -(h*Pr*up(i+2)+8.d0)
        A(i+2,8) = -4.d0*(Pr-1.d0)*((up(i+4) - up(i-2)) + 2.d0*up(i+1))
    enddo

    A(n-2,6) = 1.d0
    A(n-1,6) = 1.d0
    A(n,2)   = h
    A(n,3)   = 2.d0
    A(n,5)   = h
    A(n,6)   = -2.d0

!------------------------------------------------------------------
!Right Hand Side
!------------------------------------------------------------------

    !R(1) = enth(2) - enth(1)                         !<------------Change to this for adiabatic
    R(1) = 495.d-3 - enth(1)                           !<------------Change this for non-adiabatic
    R(2) = -vel(1)
    R(3) = -psi(1)

    mc = 2

    do i = 4,n-5,3
        !S
        R(i) = 2.d0*(psi(mc+1) - psi(mc)) - h*(vel(mc+1) + vel(mc))

        !M
        R(i+1) = (C6/2.d0)*(2.d0*enth(mc) - vel(mc)**2.d0) + h*psi(mc)*(vel(mc+1) - vel(mc-1))&
               + 8.d0*(vel(mc+1) - 2.d0*vel(mc) + vel(mc-1))

        !E
        R(i+2) = 2.d0*(Pr - 1.d0)*(4.d0*(vel(mc+1) - 2.d0*vel(mc) + vel(mc-1))*vel(mc)&
               + (vel(mc+1) - vel(mc-1))**2.d0) + h*Pr*psi(mc)*(enth(mc+1) - enth(mc-1))&
               + 8.d0*(enth(mc+1) - 2.d0*enth(mc) + enth(mc-1))

        mc = mc + 1
    enddo

    R(n-2) = 5.d-1 - enth(nn)
    R(n-1) = 1.d0 - vel(nn)
    R(n)   = 2.d0*(psi(nn) - psi(nn-1)) - h*(vel(nn) + vel(nn-1))

!----------------------------------------------------------------------------
! Matrix solving shenanigans
!----------------------------------------------------------------------------

    A(n-2,1:9) = A(n-2,1:9) + A(n-1,1:9)
    if (Pr == 1.d0) then
        A(n-3,1:9) = A(n-3,1:9) + A(n-2,1:9)
        do l = 6,n-6,3
            A(n-(l-1),1:9) = A(n-(l-1),1:9) + A(n-l,1:9)
        enddo
    endif

    ! l = 9
    do k = n-4,5,-3
        do m = 2,9
            R(k) = R(k) - (A(k,9)/A(k+1,8))*R(k+1)
            A(k,m) = A(k,m) - (A(k,9)/A(k+1,8))*A(k+1,m-1)
        enddo
    enddo
    ! l = 8
    do k = n-3,4,-1
        do m = 2,8
            R(k) = R(k) - (A(k,8)/A(k+1,7))*R(k+1)
            A(k,m) = A(k,m) - (A(k,8)/A(k+1,7))*A(k+1,m-1)
        enddo
    enddo
    ! l = 7
    do k = n-2,4,-1
        do m = 2,7
            R(k) = R(k) - (A(k,7)/A(k+1,6))*R(k+1)
            A(k,m) = A(k,m) - (A(k,7)/A(k+1,6))*A(k+1,m-1)
        enddo
    enddo

    if (A(1,9) == 0) then
        x(1) = R(1)/A(1,6)
    else
        x(1) = (R(1) - x(4)*A(1,9))/A(1,6)
    endif

    m = 4
    do k = 2,5
        axsum = 0
        do l = 1,k-1
            axsum = axsum + A(k,l+m)*x(l)
        enddo
        x(k) = (R(k) - axsum)/A(k,6)
        m = m - 1
    enddo

    do k = 6,n
        axsum = 0
        do l = 1,5
            axsum = axsum + A(k,l)*x(k-l)
        enddo
        x(k) = (R(k) - axsum)/A(k,6)
    enddo


!----------------------------------------------------------------------------
! Update uphat values
!----------------------------------------------------------------------------
    mc = 0
	do i = 1,nn
        enth_out(i) = enth(i) + x(i+mc)
        mc = mc + 1
        vel_out(i) = vel(i) + x(i+mc)
        mc = mc + 1
        psi_out(i) = psi(i) + x(i+mc)
	enddo

    entherr = dabs(sum(enth_out(1:nn)) - sum(enth(1:nn)))
    velerr = dabs(sum(vel_out(1:nn)) - sum(vel(1:nn)))
    psierr = dabs(sum(psi_out(1:nn)) - sum(psi(1:nn)))

    errf_prev = errf
    errf = max(entherr,velerr,psierr)

	if (errf < errf_prev) then
        enth(1:nn) = enth_out(1:nn)
        vel(1:nn) = vel_out(1:nn)
        psi(1:nn) = psi_out(1:nn)
	else
        check = 1
	endif

	if (cou > 30) then
        print *, "Not converging"
        read *, f
		do i = 1,nn
            print *, enth(i), vel(i), psi(i)
		enddo
        read *, f
	endif
	if (check /= 1) goto 1
    print *, cou
    open(unit=fid, file='vl_nearadi_pr1.dat', status='new', action='write')
    do i = 1,nn
                    !Enthalpy, Velocity, Streamfunction
        write(fid,*), enth(i),     vel(i),      psi(i)
    enddo
    close(unit=fid)
    fid = fid + 1

!--------------
! c3
!--------------
    c3t = 5.d-1*(2.d0*enth(1) - vel(1)**2.d0)
    do i = 2,nn-1
        c3t = c3t + (2.d0*enth(i) - vel(i)**2.d0)
    enddo
    c3t = c3t + 5.d-1*(2.d0*enth(nn) - vel(nn)**2.d0)

    c3 = (((h**2.d0)*((gam-1.d0)**2.d0))/(4.d0*gam))*(c3t**2.d0)
    
    open(unit=99, file='c3con_nearadi_pr1.dat', status='new', action='write')
    write(99,*), c3
    close(unit=99) 

    deallocate(A); deallocate(x); deallocate(R); deallocate(up)
    deallocate(enth); deallocate(vel); deallocate(psi); deallocate(eta)
    deallocate(enth_out); deallocate(vel_out); deallocate(psi_out)

end program