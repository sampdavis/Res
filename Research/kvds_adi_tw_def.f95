program khorr_vds_adi_tanwedge_deformed

    implicit none
    
    double precision, allocatable :: vl_del(:),vl_pres(:),vl_pres_prev(:),ramp_func(:),rf_b(:)!,sprs_val(:)!,csc_val(:)
    double precision :: pr,gam,c,v_dy,dx,dx1,dx2,nvec1(47),nvec2(51),nvec(98),c3,vl_ip_enth(501)
    double precision :: vl_ip_u(501),vl_ip_psi(501),x1,x2,il_p_wall,c1,c2,chi,con,h,def_points(2)
    double precision :: s_p,e_p,vl_u_save(501,1001),vl_psi_save(501,1001),pres_con,xi_interp,z,z_prev
    double precision :: vl_enth_prev(501),vl_u_prev(501),vl_psi_prev(501),vl_u(501),vl_enth(501),vl_psi(501)
    double precision :: vl_pres_interp(1001),pres_res,xi1,p1,p2,pp1,del1,ramf1,va(1003,1003),vr(1003),vx(1003)
    double precision :: enth1,psi1,psi2,psi3,u1,u2,u4,u5,u7,rp,vl_u_out(501),vl_psi_out(501),vl_pres_out
    double precision :: vl_dens(501,1001),vl_temp(501,1001),vl_mu(501,1001),eta(501,1001),vl_v(501,1001),tg
    !integer, allocatable :: sprs_row(:),sprs_col(:)
    integer :: v_ny,v_n,nx1,nx2,nvl_1,nvl_2,nvl,nx,i,j,v_fid,sweep,br,incres,ii,jj,vcount,mc,f,ig,kg,mg !,si,spind,iii,jjj,ci,new_col
    
    character(*), parameter :: matfile = 'c:/users/sampson/documents/gitlab/res/research/matlab/tanwedge_ds/'
    character(*), parameter :: rampfile = 'chi20/Pr1/sine/convex/1000pts/csv_ramps/'
    character(*), parameter :: vlip = 'c:/users/sampson/documents/gitlab/res/research/vl_ip/'
    character(*), parameter :: ilip = 'c:/users/sampson/documents/gitlab/res/research/il_ip/'
    character(*), parameter :: vlds = 'c:/users/sampson/documents/gitlab/res/research/vl_ds/'
    
    !double precision :: control(20),info(90)
    !integer*8 :: symbolic,numeric
    !integer*4 :: sys

    !-----------------HERE WE GOOOOOOO----------------------
    pr = 1.d0; gam = 14.d-1; c = 1.d0;
    v_ny = 501; v_n = 2*v_ny; v_dy = 10.d0/dble(v_ny-1);
    nx1 = 51; dx1 = 1.d0/dble(nx1 - 1);
    nx2 = 1001; dx2 = 1.d0/dble(nx2 - 1);
    
    !------------Sparse index-------------------------------
    !spind = 9 + (v_ny - 2)*10
    
    do i = 1,47
        nvec1(i) = (i-1)*dx1
    enddo
    do i = 1,51
        nvec2(i) = 95.d-2 + (i-1)*dx2
    enddo
    
    nvl_1 = 47; nvl_2 = 51; nvl = 98;
    nvec(1:47) = nvec1; nvec(48:98) = nvec2
    
    !-------------------Allocating Stuff---------------------
    allocate(vl_del(nvl))
    allocate(vl_pres(nvl)); allocate(vl_pres_prev(nvl))
    allocate(ramp_func(nvl)); allocate(rf_b(nvl))
    !allocate(sprs_val(spind)); allocate(sprs_row(spind)); allocate(sprs_col(v_n+1))

    !-------------------Viscous Layer (IP)-------------------
    v_fid = 11;
    open(unit=v_fid, file=vlip//'vl_pr1adi.dat', status='old', action='read')
    do j = 1,v_ny
        read(v_fid,*), vl_ip_enth(j), vl_ip_u(j), vl_ip_psi(j)
    enddo
    close(unit=v_fid)
    v_fid = v_fid + 2
    !-------------------Inviscid Layer (IP)------------------
    open(unit=12, file=ilip//"il_khm_500.dat", status="old", action="read")
    read(12,*), x1, x2, il_p_wall
    close(unit = 12)
    
    !-------------------C1, C2, C3 constants------------------
    open(unit=9, file=vlip//"c3con_pr1adi.dat", status="old", action="read")
    read(9,*), c3
    close(unit=9)

    c2 = ((c*c3)/il_p_wall)**(1.d0/4.d0)
    c1 = (c*c3)/(c2*c2)
    
    !-----------------------Chi vector-----------------------
    chi = 2.d0 !(/ 1.d0,15.d-1,2.d0,25.d-1,3.d0,35.d-1,4.d0 /)
    
    !----------------Deformation Parameters------------------
    con = 1; h = 25.d-3; def_points = (/1.d-1, 4.d-1/);
    s_p = def_points(1); e_p = def_points(2);
    
    !-----------------Initialized BL & Pres------------------
    vl_del(1:nvl) = 0.d0; vl_del(1) = 1.d0;
    
    vl_pres_prev(1:nvl-1) = 1.d0; vl_pres_prev(nvl) = 1.d0/(c1*gam*chi*chi*chi);
    
    !------------------Deformation shaping-------------------
    open(unit=14, file=matfile//rampfile//'coarse_ramp14_xl.csv', status='old', action='read')
    read(14,*), ramp_func(1:nvl)
    close(unit=14)
    
    rf_b(1) = 1.d0
    do i = 2,nvl_1
        rf_b(i) = ramp_func(i)/( c2*(((i-1)*dx1)**(75.d-2)) )
    enddo
    do i = 1,nvl_2
        rf_b(nvl+i) = ramp_func(i)/(  c2*((95.d-2 + (i-1)*dx2)**(75.d-2)) )
    enddo
    
    !----------------Saved Layer Profiles--------------------
    vl_u_save = 0.d0; vl_u_save(1:v_ny,1) = vl_ip_u;
    vl_psi_save = 0.d0; vl_psi_save(1:v_ny,1) = vl_ip_psi;
    
    !----------------Oh boy! Here I go looping!--------------
    pres_con = 100; sweep = 0; br = 0; incres = 0;
    
10  sweep = sweep + 1

    vl_pres = vl_pres_prev
    
    !-------------------Fine it up!----------------------
    ! if (pres_con < 1.d-2 .and. incres == 0) then
    
        ! deallocate(vl_del); deallocate(ramp_func); deallocate(rf_b)
        
        ! nx = 1001
        ! dx = 1.d0/dble(nx-1)

        ! allocate(vl_del(nx)); allocate(ramp_func(nx)); allocate(rf_b(nx))
    
        ! vl_del = 0.d0; vl_del(1) = 1.d0
        
        ! !---------------Deformation---------------------------
        ! open(unit=16, file=matfile//rampfile//'ramp14_xl.csv', status='old', action='read')
        ! read(16,*), ramp_func(1:nx)
        ! do i = 1,nx
            ! if (i == 1) then
                ! rf_b(i) = ramp_func(i)
            ! endif
            ! rf_b(i) = ramp_func(i)/( c2*(((i-1)*dx)**(75.d-2)) )
        ! enddo
        ! close(unit=16)
        ! !-------Interp Pressure
        ! xi_interp = 0.d0
        ! ii = 1; jj = 2
        ! vl_pres_interp(ii) = vl_pres(ii)

! 30      xi_interp = xi_interp + dx
        ! if (ii == nx) goto 40
        ! ii = ii + 1
        ! if (xi_interp < nvec(jj)) then
            ! vl_pres_interp(ii) = vl_pres(jj-1)&
            ! + (xi_interp - nvec(jj-1))*((vl_pres(jj) - vl_pres(jj-1))/(nvec(jj) - nvec(jj-1)))
            ! goto 30
        ! elseif (xi_interp > nvec(jj)) then
            ! jj = jj + 1
            ! vl_pres_interp(ii) = vl_pres(jj-1)&
            ! + (xi_interp - nvec(jj-1))*((vl_pres(jj) - vl_pres(jj-1))/(nvec(jj) - nvec(jj-1)))
            ! goto 30
        ! elseif (xi_interp == nvec(jj)) then
            ! vl_pres_interp(ii) = vl_pres(jj)
            ! jj = jj + 1
            ! goto 30
        ! endif

! 40      incres = 1

        ! deallocate(vl_pres); deallocate(vl_pres_prev)
        ! allocate(vl_pres(nx)); allocate(vl_pres_prev(nx))

        ! vl_pres_prev = vl_pres_interp;
        ! vl_pres = vl_pres_prev
        
    ! endif
    if (incres == 0) then
        nx = nvl;
    endif
 
    !-----------Downstream March------------------
    do i = 2,nx-1
    
        if (incres == 0) then
            z = nvec(i);
            z_prev = nvec(i-1);
            dx = z - z_prev;
        else
            z = (i-1)*dx;
            z_prev = (i-2)*dx;
        endif
    
        if (i == 2) then
            vl_enth_prev = vl_ip_enth;
            vl_u_prev = vl_ip_u;
            vl_psi_prev = vl_ip_psi;
        endif
        
        !-----------Initial Guesses------------
        vl_enth = vl_enth_prev
        vl_u = vl_u_prev
        vl_psi = vl_psi_prev
        
        pres_res = 100; vcount = 0;
        
20      vcount = vcount + 1
        !print *, sweep, vcount
        
        xi1 = z + z_prev
        p1 = vl_pres(i) + vl_pres(i-1); p2 = vl_pres(i+1) - vl_pres(i)

        !-------------Tangent Wedging-------------
        pp1 = ( (c1*p1)/((2.d0*xi1)**(5.d-1)) ) - (1.d0/(gam*chi*chi*chi))

        vl_del(i) = ( ((8.d0*dx)/c2)*((xi1/2.d0)**(25.d-2))*pp1&
            *( (((gam+1.d0)/2.d0)*pp1 + (1.d0/(chi*chi*chi)))**(-5.d-1) )&
            - vl_del(i-1)*(3.d0*dx - 4.d0*xi1) )/(3.d0*dx + 4.d0*xi1)

        del1 = vl_del(i) + vl_del(i-1)

        ramf1 = rf_b(i) + rf_b(i-1)

        !------------------------------------------
        
        va(1:v_n+1,1:v_n+1) = 0.d0; vr(1:v_n+1) = 0.d0

        
        !---------Filling out Matrix and RHS-------
        
        !----------Wall BCs------------------------
        va(1,1) = 1.d0; vr(1) = -vl_u_prev(1) - vl_u(1)
        va(2,2) = 1.d0; vr(2) = -vl_psi_prev(1) - vl_psi(1)
        
        mc = 1;

        do j = 2,v_ny-1
            enth1 = vl_enth(j) + vl_enth_prev(j)
            psi1 = vl_psi(j) + vl_psi_prev(j); psi2 = vl_psi(j) - vl_psi_prev(j)
            psi3 = vl_psi(j) - vl_psi(j-1) + vl_psi_prev(j) - vl_psi_prev(j-1)
            u1 = vl_u(j) + vl_u_prev(j); u2 = vl_u(j) - vl_u_prev(j)
            u4 = vl_u(j) + vl_u(j-1) + vl_u_prev(j) + vl_u_prev(j-1)
            u5 = vl_u(j+1) - vl_u(j-1) + vl_u_prev(j+1) - vl_u_prev(j-1)
            u7 = vl_u(j+1) - 2*vl_u(j) + vl_u(j-1) +  vl_u_prev(j+1) - 2*vl_u_prev(j) + vl_u_prev(j-1)
            
            ! print *, j
            ! print *, enth1
            ! print *, psi1
            ! print *, psi2
            ! print *, psi3
            ! print *, u1
            ! print *, u2
            ! print *, u4
            ! print *, u5
            ! print *, u7
            ! read *, f

        !-------Streamfunction Matrix Elements---
            va(j+mc,(j+mc)-2) = -v_dy*(del1-ramf1)
            va(j+mc,(j+mc)-1) = -4.d0
            va(j+mc,j+mc) = -v_dy*(del1-ramf1)
            va(j+mc,(j+mc)+1) = 4.d0
            
        !----------Streamfunction RHS------------

            vr(j+mc) = v_dy*(del1-ramf1)*u4 - 4.d0*psi3
            
        !----------------------------------------

            mc = mc + 1

        !-------Momentum Matrix Elements---------
            va(j+mc,(j+mc)-3) = p1*(16.d0*dx*p1 - v_dy*dx*(del1-ramf1)*psi1 - 4.d0*v_dy*xi1*(del1-ramf1)*psi2)
            va(j+mc,(j+mc)-1) = 4.d0*v_dy*v_dy*((gam-1.d0)/gam)*xi1*((del1-ramf1)**2.d0)*p2*u1&
                    - p1*( 32.d0*dx*p1 + 2.d0*v_dy*v_dy*dx*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*u1&
                    + 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*(u2+u1) )
            va(j+mc,j+mc)     = p1*u5*(del1-ramf1)*v_dy*(dx + 4.d0*xi1)
            va(j+mc,(j+mc)+1) = p1*(16.d0*dx*p1 + v_dy*dx*(del1-ramf1)*psi1 + 4.d0*v_dy*xi1*(del1-ramf1)*psi2)
            va(j+mc,v_n+1)    = 32.d0*dx*p1*u7 - 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*u1*u2&
                    + v_dy*(del1-ramf1)*u5*(dx*psi1 + 4.d0*xi1*psi2)&
                    + v_dy*v_dy*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*(enth1 - 25.d-2*u1*u1)*(4.d0*dx + 8.d0*xi1)
            
        !------------Momentum RHS----------------
            
            vr(j+mc) = 4.d0*v_dy*v_dy*xi1*((del1-ramf1)**2.d0)*p1*u1*u2 - 16.d0*dx*p1*p1*u7&
                    - v_dy*(del1-ramf1)*p1*u5*(4.d0*xi1*psi2 + dx*psi1)&
                    + v_dy*v_dy*((gam-1.d0)/gam)*((del1-ramf1)**2.d0)*(enth1 - 25.d-2*u1*u1)*(8.d0*xi1*p2 - 4.d0*dx*p1)
                    
        !----------------------------------------
            
        enddo

        !-------Bl BCs
        va(v_n-1,v_n-1) = 1.d0
        vr(v_n-1) = 2 - vl_u_prev(v_ny) - vl_u(v_ny)

        va(v_n,v_n-3) = -v_dy*(del1-ramf1)
        va(v_n,v_n-2) = -4.d0
        va(v_n,v_n-1) = -v_dy*(del1-ramf1)
        va(v_n,v_n)   = 4.d0
        vr(v_n)   = v_dy*(del1-ramf1)*(vl_u_prev(v_ny) + vl_u_prev(v_ny-1) + vl_u(v_ny) + vl_u(v_ny-1))&
            - 4.d0*(vl_psi_prev(v_ny) - vl_psi_prev(v_ny-1) + vl_psi(v_ny) - vl_psi(v_ny-1))

        !------------Pressure Elements-----------
        va(v_n+1,1) = 5.d-1*(vl_u(1) + vl_u_prev(1))

        mc = 1
        do j = 2,v_ny-1
            va(v_n+1,j+mc) = vl_u(j) + vl_u_prev(j)
            mc = mc + 1
        enddo
        va(v_n+1,v_n-1) = 5.d-1*(vl_u(v_ny) + vl_u_prev(v_ny))
        va(v_n+1,v_n+1) = (2.d0*((gam*c3)**(5.d-1)))/(v_dy*(gam-1.d0))

        !-----------------Pressure RHS---------------
        rp = ( (vl_enth_prev(1) + vl_enth(1)) - 25.d-2*((vl_u_prev(1) + vl_u(1))**2.d0) )&
            + ( (vl_enth_prev(v_ny) + vl_enth(v_ny)) - 25.d-2*((vl_u_prev(v_ny) + vl_u(v_ny))**2.d0) )
        do j = 2,v_ny-1
            rp = rp + 2.d0*( (vl_enth_prev(j) + vl_enth(j)) - 25.d-2*((vl_u_prev(j) + vl_u(j))**2.d0) )
        enddo

        vr(v_n+1) = rp - ((2.d0*((gam*c3)**(5.d-1)))/(v_dy*(gam-1.d0)))*p1
        
        open(unit=v_fid, file='va.dat', status='new', action = 'write')
        do j = 1,v_n+1
            write(v_fid,*), va(j,1:v_n+1)
        enddo
        close(unit=v_fid)
        v_fid = v_fid + 2
        
        open(unit=v_fid, file='vr.dat', status='new', action = 'write')
        do j = 1,v_n+1
           write(v_fid,*), vr(j)
           print *, vr(j)
        enddo
        close(unit=v_fid)
        
        read *, f
        
        !-----------------CC Formating---------------
        ! si = 0; ci = 0;
        ! do iii = 1,v_n+1
            ! do jjj = 1,v_n+1
                ! if (va(jjj,iii) /= 0.0) then
                    ! si = si + 1
                    ! sprs_val(si) = va(jjj,iii)
                    ! sprs_row(si) = jjj
                    ! if (new_col == 1) then
                        ! ci = ci + 1
                        ! sprs_col(ci) = si
                        ! new_col = 0
                    ! endif
                ! endif
            ! enddo
            ! new_col = 1;
        ! enddo
        
    !---------------------------------------------------------------
    !               Gaussian Elimination (subroutine)
    !---------------------------------------------------------------
    do mg = v_n+1,1,-1
        do ig = mg-1,1,-1
            vr(ig) = vr(ig)-( (va(ig,mg)*vr(mg))/va(mg,mg) )
            do kg = 1,v_n+1
                va(ig,kg) = va(ig,kg) - ( (va(ig,mg)*va(mg,kg))/va(mg,mg) )
            enddo
        enddo
    enddo

    !---------------------------------------------------------------
    !               Forward Substitution (subroutine)
    !---------------------------------------------------------------
    vx(1:v_n+1) = 0.d0

    vx(1) = vr(1)/va(1,1)

    do ig = 2, v_n+1
        tg = vr(ig)
        do kg = 1,ig
            tg = tg - ( va(ig,kg)*vx(kg) )
        enddo
        vx(ig) = tg/va(ig,ig)
    enddo
        
        
        ! !--------------Matrix Solve------------------
        ! !   Set default control parameters
        ! call umf4def(control)
        ! !
        ! !   From the matrix data, create the symbolic factorization information
        ! call umf4sym(v_n+1,v_n+1,sprs_col,sprs_row,sprs_val,symbolic,control,info)
        ! !
        ! !   From the symbolic factorization information, carry out the numeric factorization
        ! call umf4num(sprs_col,sprs_row,sprs_val,symbolic,numeric,control,info)
        ! !
        ! !   Free the memory associated with the symbolic factorization
        ! call umf4fsym(symbolic)
        ! !
        ! !   Solve the linear system
        ! call umf4sol(sys,vx,vr,numeric,control,info)
        ! !
        ! !   Free the memory associated with the numeric factorization
        ! call umf4fnum(numeric)
        ! !
        ! !-------------------------------------------

        mc = 0
        do j = 1,v_ny
            vl_u_out(j) = vl_u(j) + vx(j+mc)
            mc = mc + 1
            vl_psi_out(j) = vl_psi(j) + vx(j+mc)
        enddo
        vl_pres_out = vl_pres(i) + vx(v_n+1)
        print *, vl_pres_out

        if (vl_pres_out < 0) then
            vl_pres_prev(1:i-1) = vl_pres(1:i-1)
            vl_pres_prev(i:nx-1) = vl_pres(i)
            goto 10
        endif

        vl_u = vl_u_out; vl_psi = vl_psi_out
        vl_pres(i) = vl_pres_out

        pres_res = dabs(vx(v_n+1))

        if (incres == 1) then
            vl_u_save(1:v_ny,i) = vl_u(1:v_ny)
            vl_psi_save(1:v_ny,i) = vl_psi(1:v_ny)
        endif

        if (pres_res > 1.d-8) goto 20

    enddo

    pres_con = maxval(dabs(vl_pres(1:nx) - vl_pres_prev(1:nx)))
    
    print *, '','sweep = ', sweep,'pressure_diff = ', pres_con
    read *, f
    if (pres_con > 1.d-8) then
        vl_pres_prev = vl_pres
        goto 10
    endif

    !--------------Data Save----------------------
    vl_dens = 0.d0; vl_temp = 0.d0; vl_mu = 0.d0; vl_v = 0.d0; eta = 0.d0

    eta(v_ny,1:nx) = 1.d0

    do i = 1,nx
        do j = 1,v_ny
            vl_dens(j,i) = (2.d0*gam*c3*vl_pres(i))/( (gam-1.d0)*(1.d0 - vl_u_save(j,i)**2.d0) )
            vl_temp(j,i) = (c3*vl_pres(i))/vl_dens(j,i)
            vl_mu(j,i) = gam*vl_temp(j,i)
        enddo
    enddo
    do i = 1,nx
        do j = 2,v_ny-1
            eta(j,i) = eta(j-1,i) + (((gam*c3)**(5.d-1))*v_dy*5.d-1*((1.d0/vl_dens(j,i))+(1.d0/vl_dens(j-1,i))))
        enddo
    enddo
    do i = 1,nx
        if (i == 1) then
            do j = 1,v_ny
                vl_v(j,1) = 75.d-2*eta(j,1)*vl_u_save(j,1) - ((25.d-2*((gam*c3)**(5.d-1))*vl_psi_save(j,1))/vl_dens(j,1))
            enddo
        else
            do j = 1,v_ny
                vl_v(j,i) = 75.d-2*eta(j,i)*vl_u_save(j,i)*vl_del(i)&
                + ((i-1)*dx)*eta(j,i)*vl_u_save(j,i)*((vl_del(i) - vl_del(i-1))/dx)&
                - (((gam*c3)**(5.d-1))/(4.d0*vl_dens(j,i)))*( vl_psi_save(j,i)&
                + (4.d0*((i-1)*dx)*((vl_psi_save(j,i) - vl_psi_save(j,i-1))/dx)) )
            enddo
        endif
    enddo
    
end program