\title{AFRL Summer Fellowship Summary}
\author{Sampson Davis}
\date{\today}
\documentclass[11pt]{article}
\setlength\parskip{\baselineskip}
\usepackage{amsmath}
\usepackage[paper=a4paper,dvips,top=1.5cm,left=1.5cm,right=1.5cm,
    foot=1cm,bottom=1.5cm]{geometry}
\begin{document}

\maketitle

This summer the problem of upstream influence caused by a compression ramp in hypersonic-laminar flow was analyzed. The scaling introduced in [Ref 1] shows the upstream influence length, normalized by the boundary layer thickness, scales according to the following parameter: $\frac{Re \delta p}{\gamma M^2_\infty p_\infty}$. The parameter is an estimate of the ratio of viscous to pressure forces in the hypersonic viscous layer. This parameter was used used in [Ref 1] in computation of parabolized Navier-Stokes equations to predict upstream influence that resulted in good agreement with experiments. Though the computational results are promising, there is more to be understood about the mechanisms leading to the upstream influence. The goal of this summer was then to use asymptotic inviscid and viscous compressible hypersonic flow to reproduce the results in [Ref 1] and dig deeper into the underlying phenomena.

To attempt to analyze this problem, an asymptotic theory for hypersonic flow is required. The paper from Khorrami & Smith [Ref 2] was used as a basis for this theory, the primary motivation being that this source was able to transform a system of PDEs into a system of ODEs without losing much accuracy and thus easing the problem immensely. But, unlike [Ref 2] an explicit method was seen as being more favorable to utilize in integrating the layers’ ODEs. Thus, before the compression ramp could be analyzed the results in [Ref 2] on a flat plate had to be reproduced using an explicit method to both verify [Ref 2] and justify using the explicit method.

Though hyperbolic, the “Golden Egg” of [Ref 2] is the use of a similarity solution with the asymptotic expansions of the equations to reduce the PDEs used in the analysis into a system of ODEs. Once normalized, the physical quantities in these equations are expanded in terms of powers of the small perturbation parameter and the free-stream Mach number to determine the non-dimensional first-order equations of motion in both layers. Near the leading edge of the plate (x \rightarrow 0) the physical quantities have a similarity form based on the shape of the boundary layer near the leading edge. Basically, using the shape of the boundary layer, i.e. $\delta ~ x^\frac{3}{4}$ , the behavior of the flow variables in both layers can be estimated and that estimation is then used as the basis for the similarity solution of the equations of motion in both layers.

Assuming a 2D laminar and perfect gas flow, in the viscous layer the Prandtl hypersonic boundary layer equations are valid and used. Once the non-dimensional first-order form of these equations are found they are then transformed into a coordinate system where the boundary layer changes from a smooth curve into a straight line and set at $\eta = 1$, the wall set at $\eta = 0$, the leading edge at $\xi = 1$, and the trailing edge at $\xi = 1$. The similarity solution is then subsequently implemented, resulting in the following equations of motion.

$ C_2 \xi (\delta \frac{\partial (\rho u)}{\partial \xi} - \eta \frac{d \delta}{d \xi} \frac{\partial (\rho u)}{\partial \eta}) - \frac{3}{4} C_2 \eta \delta \frac{\partial (\rho u)}{\partial \eta} - \frac{1}{2} C_2 \delta \rho u + \frac{\partial (\rho v)}{\partial \eta} = 0$
\\
$ \xi C_2^2 \delta^2 (\rho u \frac{\partial u}{\partial \xi} - \frac{\rho u \eta}{\delta} \frac{d \delta}{d \xi} \frac{\partial u}{\partial \eta} + C_1\frac{\partial p}{\partial \xi}) =
\frac{1}{2}C_1 C_2^2 \delta^2 p + \frac{3}{4} C_2^2 \delta^2 \rho u \eta \frac{\partial u}{\partial \eta} - \rho v C_2 \delta \frac{\partial u}{\partial \eta}
+ \mu \frac{\partial^2 u}{\partial \eta^2} + \frac{\partial \mu}{\partial \eta} \frac{\partial u}{\partial \eta} $
\\
$ \xi C_2^2 \delta^2 ( \rho u \frac{\partial H}{\partial \xi} - \frac{\rho u \eta}{\delta} \frac{\partial H}{\partial \eta} \frac{d \delta}{d \xi}) =
\frac{3}{4} \rho u \eta C_2^2 \delta^2 \frac{\partial H}{\partial \eta} - \rho v C_2 \delta \frac{\partial H}{\partial \eta} + \frac{\partial}{\partial \eta}
(\frac{\mu}{Pr} \frac{\partial H}{\partial \eta} + (1 - \frac{1}{Pr})\mu u \frac{\partial u}{\partial \eta}) $

Here $C_1$ and $C_2$ are constants associated with the pressure and boundary layer thickness respectively. It should be noted that at the leading edge, where $\xi \rightarrow 0$, these equations reduce from a set of PDEs to a set of hyperbolic ODEs as the terms attached to $\xi$ becoming negligible and $\bar{p}$ and $\bar{\delta}$, being the non-dimensional pressure and boundary layer thickness respectively, become unity at the leading edge. It should also be noted that $\bar{p}$ and $\bar{\delta}$, in the viscous layer, are dependent only on the horizontal coordinate $\xi$.

In the inviscid layer, the Euler equations take a non-dimensional first order form and are transformed very much like the equations used in the viscous layer, but now with the vertical coordinate going from the boundary layer edge at $\eta = 0$, to the shock layer at $\eta = 1$. A similarity solution different from the one in the viscous layer is then implemented, resulting in the following equations of motion.

$ 4 \xi ( (C_5 g - \delta)\frac{\partial \rho}{\partial \xi} + V_2 \frac{\partial \rho}{\partial \eta} ) + V_1 \frac{\partial \rho}{\partial \eta} + 4 \rho \frac{\partial v}{\partial \eta} = 0
$
\\
$ 4 \xi ( (C_5 g - \delta)(\rho \frac{\partial u}{\partial \xi} + \frac{\partial p}{\partial \xi}) + V_2(\rho \frac{\partial u}{\partial \eta} + \frac{\partial p}{\partial \eta}) ) + \rho V_1 \frac{\partial u}{\partial \eta} - 3( (C_5 g - \delta) \eta + \delta)\frac{\partial p}{\partial \eta} - 2(C_5 g - \delta)(\rho u + p) = 0
$
\\
$ 4 \xi( (C_5 g - \delta) \rho \frac{\partial v}{\partial \xi} + \rho V_2 \frac{\partial v}{\partial \eta} ) + \rho V_1 \frac{\partial v}{\partial \eta} + 4 \frac{\partial p}{\partial \eta} - v \rho (C_5 g - \delta) = 0
$
\\
$ 4 \xi ((C_5 g - \delta)(\rho\frac{\partial p}{\partial \xi} - \gamma p \frac{\partial \rho}{\partial \xi}) + V_2(\rho \frac{\partial p}{\partial \eta} - \gamma p \frac{\partial \rho}{\partial \eta})) + V_1(\rho \frac{\partial p}{\partial \eta} - \gamma p \frac{\partial \rho}{\partial \eta}) - 2(C_5 g - \delta) \rho p = 0
$
\\
$
V_1 = 4 v - 3 \eta (C_5 g - \delta) - 3 \delta
$
\\
$
V_2 = (\eta - 1) \frac{d \delta}{d \xi} - \eta C_5 \frac{d g}{d \xi}
$

Here $C_4$ is a constant associated with the shock layer thickness, $g$. $C_2$ is the same constant seen in the viscous layer. As with the viscous layer equations, the inviscid layer equations of motion also drastically reduce near the leading edge, with $\xi \rightarrow 0$. Here also $\bar{\delta}$ and now $\bar{g}$ is dependent only on η and are each unity at the leading edge.

In the theory introduced by [Ref 2] the equations of motion in both layers are coupled as seen with the constant $C_2$ not being able to be determined without both layers being solved. To do this the pressure in both layers needs to match at the boundary layer. And so the method used was to first take the equations of motion of the viscous layer and transform them into a system of 5 ODEs shown below. The unknowns that need to be solved for in the viscous layer at the leading edge are $C_1$, $C_2$, $\frac{\partial^2 u}{\partial \eta^2}$ at the wall, and $\frac{\partial^2 H}{\partial \eta^2}$ at the wall. The boundary conditions are then set with the no slip condition and the total enthalpy equation, i.e., $\bar{u} = 0$ and $\bar{v} = 0$ at the wall and the enthalpy is whatever the wall enthalpy is set to be. In the adiabatic case, for Pr = 1, $\bar{H} = 0$, $\frac{\partial^2 H}{\partial \eta^2}$ at the wall. At the boundary layer, $u = U_\infty$, and thus, $\bar{u} = 1$ and consequently through the total enthalpy equation, $\bar{H} = \frac{1}{2}$ at the boundary layer for all cases.

The viscous layer at the leading edge is then solved from $\eta = 0$ to $\eta = 1$, i.e. from the wall to the boundary layer, using a pattern search shooting method developed this summer by Dr. Ward that maps initial guesses of the unknowns onto the output of $\bar{u}$ at $η = 1$ and modifies the initial guesses until $\bar{u} = 1$ at $\eta = 1$, i.e. until the layer converges. In the inviscid layer this method is repeated, but now integrating from $\eta = 1$ to $\eta = 0$ (i.e., from the shock layer to the boundary layer), where the relevant equations of motion are manipulated into a system of ODEs. Here, the boundary conditions are set by the Rankine-Hugoniot relations for an oblique shock at the shock layer and at the boundary layer by $\rho \rightarrow 0$. In the inviscid layer the unknowns to solve for are $C_2$ and $C_4$.

To solve both layers at the leading edge an initial guess for $C_2$ is made and held constant. The values for $C_2$ for various cases found by [Ref 2] are used to inform this guess. With this set the viscous layer is solved in the manner previously explained and then the inviscid layer. The pressure at the boundary layer for each layer is then matched and if the difference is below tolerance the process is repeated from the beginning, but with a $C_2$ value slightly increased, affectively “growing” the boundary layer until the pressure in both layers meet tolerance.

Once the layers at the leading edge converge, we now have inlet conditions for the problem and can now march downstream to the leading edge, repeating the same fundamental process at each step. The big difference downstream is that the equations will now retain the $\xi$ dependent terms that were negligible at the leading edge and that $\bar{\delta}$, $\bar{g}$, and $\bar{p}$ are no longer unity in their respective layers. A first order backwards finite difference is used to address the $\frac{\partial}{\partial \xi}$ terms. Additionally, $\bar{\delta}$ and $\bar{p}$ replace $C_2$ and $C_1$ as unknowns in the viscous layer and $\bar{g}$ replaces $C_4$ in the inviscid layer unknowns (as well as $\bar{\delta}$ replacing $C_2$). Since the constants have already been determined in the initial profile they do not have to be determined again.

Unfortunately, this method only works close to the leading edge where $\xi$ is small, on account of the inviscid layer equations becoming more and more unstable with increasing downstream distance. To get past this instability an implicit Runge-Kutta numerical method was implemented in the inviscid layer. This method worked marginally better, but converging farther downstream is still difficult. We’re confident a solution for convergence exists, the issue is that finding a solution is costly with current methods. But, since the parameter introduced in [Ref 1] is only applicable in the viscous layer then it may be that a less accurate solution in the inviscid layer could still be viable. This means that the Tangent Wedge method or possibly the method of characteristics could be used if the current implicit method is decided to be unviable. So, far no similar concessions seem to need to be made in the viscous layer, but that remains to be definitively confirmed.

Though this method has yet to completely work for the flat plate, equations of motion for the compression ramp were still constructed. The following equations are the Prandtl Hypersonic boundary layer and Euler equations, respectively, and have been transformed in the same way as the flat plate equations above, but with an addition initial coordinate transformation from a flat surface to a ramp geometry. The term describing this geometry comes from Cassel in [Ref 3] and controls the angle of the ramp and the radius at the corner. The following are the final equations of motion for both layers after all relevant transformations and substitutions are made.
$

In conclusion, the asymptotic expansions of the equations of motion for viscous and inviscid layer still hold promise for finding an analytical solution to the problem introduced in [Ref 1], the biggest obstacle is finding and implementing an inexpensive method of find a convergent solution for the two layers downstream.

\end{document} 