\chapter{The Model, The Math, and The Method}

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\section{Introduction}

In this chapter the underlying mathematics is broadly derived and reviewed. The equations of motion and other relevant equations are presented, as well as the various scalings used in the viscous and invicid layers. Additionally, the dowstream marching method used to solve the viscous layer is explained as well as how it is used in tandem with the tangent wedge method.

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\section{The Model}

\subsection{Preliminary Equations}

As previously stated, at it's most basic configuration, a steady 2D hypersonic free stream flows over a flat plate with a sharp leading edge such that the shock wave formed is attached at the leading edge. The regions formed underneath the shock wave are described as the viscous layer (VL) and inviscid layer (IL). In the VL, which is between the wall and the boundary layer edge, the Prandtl Boundary Layer Equations govern the flow therein. They are the continuity equation,

\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial (\rho^* u^*)}{\partial x^*} + \ddfrac{\partial (\rho^* v^*)}{\partial y^*} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the streamwise momentum equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho^* u^* \ddfrac{\partial u^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial u^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial x^*} + \ddfrac{\partial}{\partial y^*} \bigg(\mu^* \ddfrac{\partial u^*}{\partial y^*}\bigg),
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the transverse momentum equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial p^*}{\partial y^*} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
and the energy equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho^* u^* \ddfrac{\partial H^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial H^*}{\partial y^*} = \ddfrac{\partial}{\partial y^*}\bigg(\ddfrac{\mu^*}{Pr} \ddfrac{\partial H^*}{\partial y^*}\bigg) + \ddfrac{\partial}{\partial y^*}\bigg[\bigg(1 - \ddfrac{1}{Pr}\bigg) \mu^* u^* \ddfrac{\partial u^*}{\partial y^*}\bigg].
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
Similarly, in the IL between the boundary layer edge and the shock wave, the Euler Equations govern the flow. They are the continuity equation,

\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial (\rho^* u^*)}{\partial x^*} + \ddfrac{\partial (\rho^* v^*)}{\partial y^*} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the streamwise momentum equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho^* u^* \ddfrac{\partial u^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial u^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial x^*},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the transverse momentum equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho^* u^* \ddfrac{\partial v^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial v^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial y^*},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
and the energy equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho^* u^* \ddfrac{\partial p^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial p^*}{\partial y^*} = \gamma p^* \Big(u^* \ddfrac{\partial \rho^*}{\partial x^*} + v^* \ddfrac{\partial \rho^*}{\partial y^*}\Big).
\end{equation}
\end{center}
\vspace{-3mm}

In addition to the equations of motion, assuming a perfect gas with a specific heat ration $\gamma = 1.4$ for air, the equation of state through both layers is,

\vspace{-15mm}
\begin{center}
\begin{equation}
p^* = \rho^* R^* T^*,
\end{equation}
\end{center}
\vspace{-5mm}

\noindent
as well as the total enthalpy equation,
\vspace{-12mm}
\begin{center}
\begin{equation}
H^* = \ddfrac{\gamma}{\gamma - 1} \frac{p^*}{\rho^*} + \ddfrac{u^{*2} + v^{*2}}{2},
\end{equation}
\end{center}
\vspace{-2mm}

\noindent
and the linear temperature-viscosity law,
\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\mu^*}{\mu^*_{\infty}} = C\ddfrac{ T^*}{T^*_{\infty}}
\end{equation}
\end{center}
\vspace{-2mm}

\noindent
in the viscous layer, where $C$ is the Chapman-Rubesin constant and is here taken to be unity.

For each layer there are two sets of boundary conditions required. At the wall, the no slip condition is imposed, thus,

\vspace{-15mm}
\begin{center}
\begin{equation}
u^* = 0 \hspace{10mm} \& \hspace{10mm} v^* = 0.
\end{equation}
\end{center}

\noindent
and since the wall is insulated, then according to the Fourier heat conduction law, $q^* = -k \frac{\partial T^*}{\partial y^*}$, at the wall,

\vspace{-15mm}
\begin{center}
\begin{equation}
\ddfrac{\partial T^*}{\partial y^*} = \ddfrac{\partial H^*}{\partial y^*} = 0.
\end{equation}
\end{center}

At the boundary layer edge, in the VL, the velocity components are,

\vspace{-13mm}
\begin{center}
\begin{equation}
u^* = U^*_{\infty} \hspace{10mm} \& \hspace{10mm} v^* = U^*_{\infty} \ddfrac{d \delta^*}{d x^*},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
and, through the total enthalpy equation,

\vspace{-13mm}
\begin{center}
\begin{equation}
H^* = \ddfrac{1}{2} U^{*2}_{\infty},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
due to the scaling of the density (covered in section 3.2.2) at the boundary layer tending towards infinity and thus $\frac{p^*}{\rho^*}$ tending towards zero.

Again at the boundary layer, but this time in the IL, the boundary condition imposed is,

\vspace{-13mm}
\begin{center}
\begin{equation}
v^* = \ddfrac{d \delta^*}{d x^*},
\end{equation}
\end{center}
\vspace{-3mm}

the same condition imposed in the VL. Then, at the shock layer the boundary conditions are given by the Rankine-Hugoniot conditions,

%\begin{center}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\end{center}

\subsection{Non-dimensionalizations}

\begin{center}

Here we have the hypersonic interaction parameter and small perturbation parameter defined, respecively, as, 

\begin{equation}
\chi = \ddfrac{M_{\infty}}{Re^{1/6}_{\infty}} \hspace{10mm} \& \hspace{10mm} \epsilon = \ddfrac{M^{1/2}_{\infty}}{Re^{1/4}_{\infty}} = \ddfrac{\chi^{3/2}}{M_{\infty}}
\end{equation}

the HIP is different from the usual because it's more convenient to have it be order 1 (I think). ***why should I bring this up since the scaling is indp of chi?***

Using the small perturbation parameter, we get first order asymptotic expansion expressions for the non-dimensionalization of the flow parameters in each layer.

\end{center}

\begin{center}

\begin{tabular}{|c|cc|c|c|cc|}

ND & VL & IL & & ND & VL & IL \\
\vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} \\
----- & ----- & ----- & & ----- & ----- & ----- \\
\vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} & \vspace{-1.5mm} \\
$ \ddfrac{x^*}{L^*} $ & $\epsilon^4 M^4_{\infty} x$ & $\epsilon^4 M^4_{\infty} x$ & & $ \ddfrac{p^*}{p_{\infty}^*} $ & $\gamma p$ & $\gamma p$\\
\vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} \\
$ \ddfrac{y^*}{L^*} $ & $\epsilon^4 M^3_{\infty} y$ & $\epsilon^4 M^3_{\infty} y$ & & $ \ddfrac{\rho^*}{\rho_{\infty}^*}$ & $M^{-2}_{\infty} \rho$ & $\rho$ \\
\vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} \\
$ \ddfrac{\delta^*}{L^*} $ & $\epsilon^4 M^3_{\infty} \delta$ & $\epsilon^4 M^3_{\infty} \delta$ & & $ \ddfrac{\mu^*}{\mu_{\infty}^*} $ & $M_{\infty}^2 \mu$ & $0$ \\
\vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} \\
$ \ddfrac{u^*}{U_{\infty}^*}$ & $u$ & $1 + M^{-2}_{\infty} u$ & & $ \ddfrac{T^*}{T_{\infty}^*}$ & $\gamma M_{\infty}^2 T$ & $\gamma T$ \\
\vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} & \vspace{-1mm} \\
$ \ddfrac{v^*}{U_{\infty}^*} $ & $M^{-1}_{\infty} v$ & $M^{-1}_{\infty} v$ & & $ \ddfrac{H^*}{U_{\infty}^{*2}} $ & $H$ & $\frac{1}{2} + M_{\infty}^{-2} H$

\end{tabular}

\end{center}

Substituting in the non-dimensional expressions into the equations of motions, in the VL the continuity equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial (\rho u)}{\partial x} + \ddfrac{\partial (\rho v)}{\partial y} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the streamwise momentum equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\rho u \ddfrac{\partial u}{\partial x} + \rho v \ddfrac{\partial u}{\partial y} = -\ddfrac{\partial p}{\partial x} + \ddfrac{\partial}{\partial y}\Big(\mu \ddfrac{\partial u}{\partial y}\Big),
\end{equation}
\end{center}
\vspace{-3mm}

the transverse equation is,
\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial p}{\partial y} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

and the energy equation is,
\vspace{-12mm}
\begin{center}
\begin{equation}
\rho u \ddfrac{\partial H}{\partial x} + \rho v \ddfrac{\partial H}{\partial y} = \ddfrac{\partial}{\partial y}\Big(\ddfrac{\mu}{Pr} \ddfrac{\partial H}{\partial y}\Big) + \ddfrac{\partial}{\partial y}\Big[\Big(1 - \ddfrac{1}{Pr}\Big) \mu u \ddfrac{\partial u}{\partial y}\Big],
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
and in the IL the continuity equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\ddfrac{\partial \rho u}{\partial x} + \ddfrac{\partial \rho v}{\partial y} = 0,
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the streamwise momentum equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\rho u \ddfrac{\partial u}{\partial x} + \rho v \ddfrac{\partial u}{\partial y} = -\ddfrac{\partial p}{\partial x},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
the transverse momentum equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\rho u \ddfrac{\partial v}{\partial x} + \rho v \ddfrac{\partial v}{\partial y} = -\ddfrac{\partial p}{\partial y},
\end{equation}
\end{center}
\vspace{-3mm}

\noindent
and the energy equation is,

\vspace{-12mm}
\begin{center}
\begin{equation}
\rho u \ddfrac{\partial p}{\partial x} + \rho v \ddfrac{\partial p}{\partial y} = \gamma p (u \ddfrac{\partial \rho}{\partial x} + v \ddfrac{\partial \rho}{\partial y}).
\end{equation}
\end{center}
\vspace{-3mm}

The boundary conditions similarly change. At the wall, the velocity boundary conditions remain the same,

\begin{center}
\begin{equation}
u = 0 \& v = 0
\end{equation}
\end{center}

and the insulated wall,

\begin{center}
\begin{equation}
\ddfrac{\partial T}{\partial y} = \ddfrac{\partial H}{\partial y} = 0.
\end{equation}
\end{center}

\noindent
At the boundary layer edge, coming from the viscous layer, the velocity boundary conditions are,

\begin{center}
\begin{equation}
u = 1 \& v = \ddfrac{d \delta}{d x},
\end{equation}
\end{center}

and the total enthalpy becomes,

\begin{center}
\begin{equation}
H = \ddfrac{1}{2}.
\end{equation}
\end{center}

\noindent
From the inviscid layer, the boundary conditions remain the same at the boundary layer edge. At the shock wave the Rankine-Hugoniot relations become,

%\begin{center}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\begin{equation}

%\end{equation}
%\end{center}

\subsection{Similarity Solution}

In finding self-similar solutions to high speed flow a common method used is the Howarth-Dorodnitsyn transformation. But, this isn't entirely accurate when strong interaction is present. Near the leading edge of the plate where the interaction between the VL and the IL is strong it assumed that the shock wave behaves as $y = g \propto x^n$, where $n$ is unknown. Through inviscid and viscous layer arguments, more fully explored by Khorrami (YEAR) and Stewartson (YEAR), it's determined that the shock wave grows propotional to the downstream distance like $x^{3/4}$. From this result the downstream behaviour of the non-dimensional flow variables can be determined in both layers. From this behaviour the similarity solution can in turn be determined and used to solve the equations of motion, as illustrated in section ***INSERT HERE***.

\subsection{Tangent Wedge}

Though the equations of motion in the IL have been present, they are only used in reduced form (further explained in section ***INSERT HERE***) at the leading edge of the wall. Further downstream the tangent wedge approximation is used to determine the pressure on the boundary layer and the boundary layer thickness.

\begin{center}

\begin{equation}
\ddfrac{p^*}{p_\infty^*}  = 1 + \ddfrac{\gamma (\gamma + 1)}{4} K^2 + \gamma K \sqrt{ \Big( \ddfrac{\gamma + 1}{4} \Big)^2 K^2 + 1} , \hspace{10mm}  K = M_{\infty} \ddfrac{d \delta^*}{d x^*}
\end{equation}

\begin{equation}
p = \ddfrac{1}{\gamma} + \ddfrac{\gamma + 1}{4} \bigg(\ddfrac{d \delta}{d x} \bigg)^2 + \ddfrac{d \delta}{d x} \Bigg[ \bigg(\ddfrac{\gamma + 1}{4} \bigg) \bigg( \ddfrac{d \delta}{d x} \bigg)^2 + 1 \Bigg]^{1/2}
\end{equation}

\end{center}

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\section{The Math}

\subsection{Coor Change and Sim Sol'd Equations}

In order to simplify the computation, the curved boundary layer and shock wave are transformed into a straight lines. The region from the plate to the boundary layer, starting in the ($x,y$) coordinate is transformed into the ($\xi,\eta$) coordinate system such that,

\vspace{-3mm}

\begin{center}
\begin{equation}
\xi = x, \hspace{5mm} 0 \leq \xi \leq 1 \hspace{5mm} \& \hspace{5mm} \eta = \ddfrac{y - r(x)}{\delta (x) - r (x)}, \hspace{5mm} 0 \leq \eta \leq 1,
\end{equation}
\end{center}

\vspace{-3mm}

\noindent
where $r(x)$ is the function describing the shape of the plate, dependent only of the streamwise distance. Similarly, in the region from the boundary layer to the shock wave, the ($x,y$) coordinate is transformed into the ($\xi,\eta$) coordinate system such that,

\vspace{-3mm}

\begin{center}
\begin{equation}
\xi = x, \hspace{5mm} 0 \leq \xi \leq 1 \hspace{5mm} \& \hspace{5mm} \eta = \ddfrac{y - \delta (x)}{g (x) - \delta (x)}, \hspace{5mm} 0 \leq \eta \leq 1.
\end{equation}
\end{center}

\vspace{-3mm}

%
%
%           DAT GOOD GOOD COORD CHANGE
%
%

%
%
%           UPDATED EQUATIONS HERE?
%
%

As introduced in section ***INSERT HERE***, using the downstream behaviour of the flow variables similarity representations are tabulated below,

\begin{center}

\begin{tabular}{|ll|ll|}

Viscous Layer & & Inviscid Layer & \\
\hline
$ u \sim O(1) $ & $ u = \bar{u}(\xi,\eta) $ & $ u \sim x^{-1/2} $ & $ u = C_2^2 \xi^{-1/2} \bar{u}(\xi,\eta) $\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$v \sim x^{-1/4}$ & $v = C_2 \xi^{-1/4} \bar{v}(\xi,\eta)$ & $v \sim x^{-1/4}$ & $v = C_2 \xi^{-1/4} \bar{v}(\xi,\eta)$\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$p \sim x^{-1/2} $ & $p = C_1 \xi^{-1/2} \bar{p}(\xi)$ & $p \sim x^{-1/2}$ & $p = C_2^2 \xi^{-1/2} \bar{p}(\xi,\eta)$\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$\rho \sim x^{-1/2}$ & $\rho = C C^{-2}_2 \xi^{-1/2} \bar{\rho}(\xi,\eta)$ & $\rho \sim O(1)$ & $\rho = \bar{\rho}(\xi,\eta)$\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$T \sim O(1)$ & $T = \bar{T}(\xi,\eta)$ & $T \sim x^{-1/2}$ & $T = C_2^2 \xi^{-1/2} \bar{T}(\xi,\eta)$\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$H \sim O(1)$ & $H = \bar{H}(\xi,\eta)$ & $H \sim x^{-1/2}$ & $H = C_2^2 \xi^{-1/2} \bar{H}(\xi,\eta)$\\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$\mu \sim O(1)$ & $\mu = C \bar{\mu}(\xi,\eta)$ & -------------- & --------------------------- \\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$\delta \sim x^{3/4}$ & $\delta = C_2 \xi^{3/4} \bar{\delta}(\xi)$ & $\delta \sim x^{3/4}$ & $\delta = C_2 \xi^{3/4} \bar{\delta}(\xi)$ \\
\vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} & \vspace{-2mm} \\
$r \sim x^{3/4}$ & $r = C_2 \xi^{3/4} \bar{r}(\xi)$ & $g \sim x^{3/4}$ & $g = C_4 \xi^{3/4} \bar{g}(\xi)$

\end{tabular}

\end{center}

Due to the scaling of the density, near the boundary layer edge in the VL the density becomes singular. To avoid this the coordinate $\eta$ is stretched to $\zeta$ such that

\begin{center}
\begin{equation}
\zeta = \mathlarger{\int}_0^1 \ddfrac{\bar{\rho}}{{\gamma C_3}^{1/2}} d \eta.
\end{equation}
\end{center}

\noindent
To satisfy the continuity equation, the streamfuction $\psi(\xi,\eta)$ is chosen to be,

\begin{center}
\begin{equation}
\ddfrac{1}{4} \psi + \xi \ddfrac{\partial \psi}{\partial \xi} = \ddfrac{\bar{\rho}}{(\gamma C_3)^{1/2}} \Bigg[ \bigg( \ddfrac{3}{4} \eta \bar{u} \bar{\delta} - \bar{v} \bigg) + \xi \eta \bar{u} \ddfrac{d \bar{\delta}}{d \xi}\Bigg], \hspace{7mm} \ddfrac{\partial \psi}{\partial \eta} = \ddfrac{1}{(\gamma C_3)^{1/2}} \bar{u} \bar{\rho} \bar{\delta}.
\end{equation}
\end{center}

The final equations of motion is the VL are thus as follows. The stream function is,

\begin{equation}
\ddfrac{\partial \psi}{\partial \zeta} - \bar{u} \bar{\delta} = 0,
\end{equation}

\noindent
the streamwise momentum equation is,

\begin{multline}
 4 \xi (\bar{\delta} - \bar{r})^2 \bar{p} \bar{u} \ddfrac{\partial \bar{u}}{\partial \xi} - \bar{p} (\bar{\delta} - \bar{r})\bigg(4 \xi \ddfrac{\partial \psi}{\partial \xi} + \psi \bigg) \ddfrac{\partial \bar{u}}{\partial \zeta} + 2 \xi \ddfrac{\gamma - 1}{\gamma} (2 \bar{H} - \bar{u}^2)(\bar{\delta} - \bar{r})^2 \ddfrac{\partial \bar{p}}{\partial \xi} \\
- \ddfrac{\gamma - 1}{\gamma}(\bar{\delta} - \bar{r})^2 (2 \bar{H} - \bar{u}^2) \bar{p} - 4 \bar{p}^2 \ddfrac{\partial^2 \bar{u}}{\partial \zeta^2} = 0,
\end{multline}

\noindent
the energy equation is,

\begin{multline}
4 Pr \xi \bar{u} (\bar{\delta} - \bar{r})^2 \ddfrac{\partial \bar{H}}{\partial \xi} - Pr (\bar{\delta} - \bar{r}) \bigg(4 \xi \ddfrac{\partial \psi}{\partial \xi} + \psi\bigg) \ddfrac{\partial \bar{H}}{\partial \zeta} - 4 \bar{p} \ddfrac{\partial^2 \bar{H}}{\partial \zeta^2} \\
- 4 \bar{p} (Pr - 1) \bigg(\ddfrac{\partial u}{\partial \zeta}\bigg)^2 - 4 \bar{p} \bar{u} (Pr - 1) \ddfrac{\partial^2 \bar{u}}{\partial \zeta^2} = 0
\end{multline}

\noindent
and for the pressure the auxiliary pressure equation is,

\begin{equation}
\bar{p} = \ddfrac{(\gamma - 1)}{2 (\gamma C_3)^{1/2}}  \mathlarger{\int}_0^{\infty} \big(2 \bar{H} - \bar{u}^2 \big) d \zeta.
\end{equation}

The IL equations go through a similar process, sans coordinate stretch and stream function. Different though is that the IL equations are only used to solve for the IL at the leading edge. With $\xi = 0$, equations that would normally be full partial differential equations reduce down to ordinary differential equations. There are as follows, the continuity equation,

%\begin{center}
%\begin{equation}

%\end{equation}
%\end{center}

the streamwise momentum equation,

%\begin{center}
%\begin{equation}

%\end{equation}
%\end{center}

the transverse momentum equation,

%\begin{center}
%\begin{equation}

%\end{equation}
%\end{center}

and the energy equation,

%\begin{center}
%\begin{equation}

%\end{equation}
%\end{center}

\noindent
where $C_5$ is an unknown constant.

%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
%-----------------------------------------------------------------------
\section{The Method}

\subsection{Linearization and the Initial Profile}

At the leading edge the flow variables in the VL are solved as described in Khorrami (YEAR), but with change that the energy equation is removed from the system of equations. With the Prandtl number being held at unity the viscous dissipation and heat conduction are balanced out in the flow, resulting in the total enthalpy being held constant throughout the VL. Since $\bar{H} = \ddfrac{1}{2}$ at the boundary layer edge that means that $\bar{H} = \ddfrac{1}{2}$ throughout the boundary layer which, with $Pr = 1$, results in the linearized energy equation completely zeroing out and placing irreconcilable zeroes along the matrix diagonal. At the leading edge this isn't a major problem as the one can make a guess for the total enthalpy at each point that is off the expected value and simply keep iterating the solution until the matrix becomes singular and the solution has converged. But, any further downstream the removal of the energy equation becomes a necessary consession to solve for adiabatic flow.

The IL at the leading edge needs no special treatment and is solved in the manner described by Khorrami(YEAR). With the two layers determined at the leading edge the constants $C_1$ and $C_2$ can be explicitly determined and the two layers subsequently matched via the pressure in each layer at the boundary layer edge, meaning,

%\begin{center}
%\begin{equation}
%PRESSURE MATCH SHIIIIIT
%\end{equation}
%\end{center}

Initial Matrix

\begin{center}

$ \begin{pmatrix}
\bm{B_{\bar{u}_1}} \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
\bm{B_{\bar{\psi}_1}} \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
S_{2} \hspace{-3mm} &
S_{2} \hspace{-3mm} &
\bm{S_{2}} \hspace{-3mm} &
S_{2} \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
M_{2} \hspace{-3mm} &
0 \hspace{-3mm} &
M_{2} \hspace{-3mm} &
\bm{M_{2}} \hspace{-3mm} &
M_{2} \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
0 \hspace{-3mm} &
S_{3} \hspace{-3mm} &
S_{3} \hspace{-3mm} &
\bm{S_{3}} \hspace{-3mm} &
S_{3} \hspace{-3mm} &
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
0 \hspace{-3mm} &
M_{3} \hspace{-3mm} &
0 \hspace{-3mm} &
M_{3} \hspace{-3mm} &
\bm{M_{3}} \hspace{-3mm} &
M_{3} \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \vspace{-1.4mm} \\
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\ddots \hspace{-3mm} &
\vdots \vspace{-1.4mm} \\
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \hspace{-3mm} &
S_{J-1} \hspace{-3mm} &
S_{J-1} \hspace{-3mm} &
\bm{S_{J-1}} \hspace{-3mm} &
S_{J-1} \hspace{-3mm} &
0 \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \hspace{-3mm} &
M_{J-1} \hspace{-3mm} &
0 \hspace{-3mm} &
M_{J-1} \hspace{-3mm} &
\bm{M_{J-1}} \hspace{-3mm} &
M_{J-1} \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
\bm{B_{\bar{u}_J}} \hspace{-3mm} &
0 \vspace{-1.4mm} \\
0 \hspace{-3mm} &
\cdots \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
0 \hspace{-3mm} &
S_{J} \hspace{-3mm} &
S_{J} \hspace{-3mm} &
S_{J} \hspace{-3mm} &
\bm{S_{J}} \vspace{-1.4mm} \\
\end{pmatrix} \hspace{-2mm}
\begin{pmatrix}
\check{u}_{1} \vspace{-1.5mm} \\
\check{\psi}_{1} \vspace{-1.5mm} \\
\check{u}_{2} \vspace{-1.5mm} \\
\check{\psi}_{2} \vspace{-1.5mm} \\
\check{u}_{3} \vspace{-1.5mm} \\
\check{\psi}_{3} \vspace{-1.5mm} \\
\vdots \vspace{-1.5mm} \\
\check{u}_{J-1} \vspace{-1.5mm} \\
\check{\psi}_{J-1} \vspace{-1.5mm} \\
\check{u}_{J} \vspace{-1.5mm} \\
\check{\psi}_{J}
\end{pmatrix} \hspace{-1mm}
= \hspace{-1mm}
\begin{pmatrix}
R_{B_{\bar{u}_1}} \vspace{-1.5mm} \\
R_{B_{\bar{\psi}_1}} \vspace{-1.5mm} \\
R_{S} \vspace{-1.5mm} \\
R_{M} \vspace{-1.5mm} \\
R_{S} \vspace{-1.5mm} \\
R_{M} \vspace{-1.5mm} \\
\vdots \vspace{-1.5mm} \\
R_{S} \vspace{-1.5mm} \\
R_{M} \vspace{-1.5mm} \\
R_{B_{\bar{u}_J}} \vspace{-1.5mm} \\
R_{M}
\end{pmatrix} $

\end{center}

\subsection{Downstream Algorithm}

As previously stated, downstream the energy equations is dropped completely. In addition, the matrix under inspection has an additional row and column added with consists of the linearized auxillary pressure equation and the pressure elements in the momentum equation, respectively. Pressure is not explicitly present in the streamfunction. Thus, the matrix in general appears like so.

\begin{center}

$ \begin{pmatrix}
\bm{\mathnormal{B_{\bar{u}_1}}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\bm{\mathnormal{B_{\bar{\psi}_1}}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathnormal{S_{2}} \hspace{-3mm} &
\mathnormal{S_{2}} \hspace{-3mm} &
\bm{\mathnormal{S_{2}}} \hspace{-3mm} &
\mathnormal{S_{2}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathnormal{M_{2}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{M_{2}} \hspace{-3mm} &
\bm{\mathnormal{M_{2}}} \hspace{-3mm} &
\mathnormal{M_{2}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathnormal{M_{2}} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{S_{3}} \hspace{-3mm} &
\mathnormal{S_{3}} \hspace{-3mm} &
\bm{\mathnormal{S_{3}}} \hspace{-3mm} &
\mathnormal{S_{3}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{M_{3}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{M_{3}} \hspace{-3mm} &
\bm{\mathnormal{M_{3}}} \hspace{-3mm} &
\mathnormal{M_{3}} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathnormal{M_{3}} \vspace{-1.4mm} \\
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\vdots \hspace{-3mm} &
\ddots \hspace{-3mm} &
\vdots \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathnormal{S_{J-1}} \hspace{-3mm} &
\mathnormal{S_{J-1}} \hspace{-3mm} &
\bm{\mathnormal{S_{J-1}}} \hspace{-3mm} &
\mathnormal{S_{J-1}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathnormal{M_{J-1}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{M_{J-1}} \hspace{-3mm} &
\bm{\mathnormal{M_{J-1}}} \hspace{-3mm} &
\mathnormal{M_{J-1}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{M_{J-1}} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\bm{\mathnormal{B_{\bar{u}_J}}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathrm{0} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathrm{0} &
\mathnormal{S_{J}} \hspace{-3mm} &
\mathnormal{S_{J}} \hspace{-3mm} &
\mathnormal{S\_{J}} \hspace{-3mm} &
\bm{\mathnormal{S_{J}}} \hspace{-3mm} &
\mathrm{0} \vspace{-1.4mm} \\
\mathnormal{P_{1}} \hspace{-3mm} &
\cdots \hspace{-3mm} &
\mathnormal{P_{n-5}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{P_{n-3}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\mathnormal{P_{n-1}} \hspace{-3mm} &
\mathrm{0} \hspace{-3mm} &
\bm{\mathnormal{P_{n+1}}} \vspace{-1.4mm} \\
\end{pmatrix} \hspace{-2mm}
\begin{pmatrix}
\mathnormal{\check{u}_{1}} \vspace{-1.5mm}\\
\mathnormal{\check{\psi}_{1}} \vspace{-1.5mm}\\
\mathnormal{\check{u}_{2}} \vspace{-1.5mm}\\
\mathnormal{\check{\psi}_{2}} \vspace{-1.5mm}\\
\mathnormal{\check{u}_{3}} \vspace{-1.5mm}\\
\mathnormal{\check{\psi}_{3}} \vspace{-1.5mm}\\
\vdots \vspace{-1.5mm} \\
\mathnormal{\check{u}_{J-1}} \vspace{-1.5mm}\\
\mathnormal{\check{\psi}_{J-1}} \vspace{-1.5mm}\\
\mathnormal{\check{u}_{J}} \vspace{-1.5mm}\\
\mathnormal{\check{\psi}_{J}} \vspace{-1.5mm}\\
\mathnormal{\check{p}_{i}}
\end{pmatrix} \hspace{-1mm}
= \hspace{-1mm}
\begin{pmatrix}
\mathnormal{R_{B_{\bar{u}_1}}} \vspace{-1.5mm}\\
\mathnormal{R_{B_{\bar{\psi}_1}}} \vspace{-1.5mm}\\
\mathnormal{R_{S}} \vspace{-1.5mm}\\
\mathnormal{R_{M}} \vspace{-1.5mm}\\
\mathnormal{R_{S}} \vspace{-1.5mm}\\
\mathnormal{R_{M}} \vspace{-1.5mm}\\
\vdots \vspace{-1.5mm} \\
\mathnormal{R_{S}} \vspace{-1.5mm}\\
\mathnormal{R_{M}} \vspace{-1.5mm} \\
\mathnormal{R_{B_{\bar{u}_J}}} \vspace{-1.5mm}\\
\mathnormal{R_{M}} \vspace{-1.5mm} \\
\mathnormal{R_{P}}
\end{pmatrix} $

\end{center}

Similar to the leading edge profile, the distance from the wall to the boundary layer edge is discretized into $J+1$. All functions are averaged and derivatives replaced with central differences with respect to the $(i-\frac{1}{2},j-\frac{1}{2})$ point in the stream function and the $(i-\frac{1}{2}, j)$ in the momentum equation. The resulting linearized system of equations is solved at each step in tandem with the tangent wedge approximation, used to give an expression for the boundary layer thickness at the current downstream point, such that 

\begin{center}
\begin{equation}
%LINEARIZED EXPRESSION
\end{equation}
\end{center}

\noindent
That value is used in the matrix inversion/solve and the pressure is used to mark convergence for the system. The new pressure value is then fed back into the boundary layer expression and the process is iterated until the pressure change between each iteration converges. This method, while effective, requires multiple sweeps from $2 \leq i \leq I$, from the leading to the trailing edge, though the values at the leading edge remain the same. Here also the pressure is used to mark convergence, but with the whole downstream pressure profile taken into account.
%INCLUDE SOME MATH EXPRESSIONS HERE