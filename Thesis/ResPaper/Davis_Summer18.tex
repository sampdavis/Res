\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.0in]{geometry}
\usepackage{array}
\usepackage{amsmath}
\newcommand\ddfrac[2]{\frac{\displaystyle #1}{\displaystyle #2}}
\usepackage{graphicx}
\graphicspath{ {c:/Users/sampson/Documents/Gitlab/Res/Research/MATLAB/tanwedge_ds/plots/} }

\title{\LARGE{\textbf{AFRL Summer 2018 Report}}}
\author{Sampson Davis}

\begin{document}

\maketitle
%------------------------------------------------------------------
%------------------------------------------------------------------

This summer, the upstream influence in a laminar hypersonic flow over a compression ramp was analyzed using asymptotic expansion theory and the tangent wedge approximation. The scaling introduced in [1] shows that the predicted upstream influence length, normalized by the undisturbed boundary layer thickness, scales according to the following parameter: $\frac{Re \Delta p}{\gamma M^2_\infty p_\infty}$, where $Re$ is the Reynolds number, $\gamma$ is the specific heat, $M_\infty$ and $p_\infty$ are the free-stream Mach number and pressure respectively, and $\Delta p$ is here taken to be the change in pressure across the shock wave. The parameter is an estimate of the ratio of viscous to pressure forces in the hypersonic viscous layer. This parameter was used in [1] in computation of parabolized Navier-Stokes equations to predict upstream influence and was tested against various experimental results, resulting in good agreement with said results. The goal of this summer was then to use asymptotic expansion of compressible hypersonic flow used in [2] to expand the cases the parameter prediction is applied to. In this report for various high speed flows and compression ramp angles only the adiabatic case is considered.

The flow is over a flat plate/compression ramp geometry, referred to generically as "the wall", assuming a 2D laminar and perfect gas, with no real gas or high temperature effects, with an attached shock wave formed at the sharp leading edge. The flow under the shock wave consists of two layers. On top of the wall and below the boundary layer is the viscous layer (VL), where the Prandtl Hypersonic boundary layer equations are valid. The continuity, streamwise momentum, transverse momentum, and energy equations are, respectively,

\begin{center}
\vspace{-7mm}
\begin{equation}
\ddfrac{\partial (\rho^* u^*)}{\partial x^*} + \ddfrac{\partial (\rho^* v^*)}{\partial y^*} = 0,
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho^* u^* \ddfrac{\partial u^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial u^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial x^*} + \ddfrac{\partial}{\partial y^*} \bigg(\mu^* \ddfrac{\partial u^*}{\partial y^*}\bigg),
\end{equation}
\vspace{-2mm}
\begin{equation}
\ddfrac{\partial p^*}{\partial y^*} = 0,
\end{equation}
\vspace{-1mm}
\begin{equation}
\rho^* u^* \ddfrac{\partial H^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial H^*}{\partial y^*} = \ddfrac{\partial}{\partial y^*}\bigg(\ddfrac{\mu^*}{Pr} \ddfrac{\partial H^*}{\partial y^*}\bigg) + \ddfrac{\partial}{\partial y^*}\bigg[\bigg(1 - \ddfrac{1}{Pr}\bigg) \mu^* u^* \ddfrac{\partial u^*}{\partial y^*}\bigg],
\end{equation}

\end{center}

\noindent
where '$*$' denotes the dimensional quantity, $Pr$ is the Prandtl number, $x$ and $y$ are the respective streamwise and transverse lengths, $u$ and $v$ are the respective streamwise and transverse velocities, $p$ is the pressure, $\rho$ is the density, $T$ is the temperature, $H$ is the enthalpy, $\mu$ is the viscosity, and $\delta$ is the boundary layer thickness. Note that $(3)$ implies that pressure in the viscous layer is constant from the wall to the boundary layer and so is only dependent on the downstream distance.

Above the boundary layer and immediately below the shock layer is the inviscid layer (IL) where the Euler equations are valid. The continuity, streamwise momentum, transverse momentum, and energy equations are, respectively, 

\begin{center}
\vspace{-7mm}
\begin{equation}
\ddfrac{\partial (\rho^* u^*)}{\partial x^*} + \ddfrac{\partial (\rho^* v^*)}{\partial y^*} = 0,
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho^* u^* \ddfrac{\partial u^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial u^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial x^*},
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho^* u^* \ddfrac{\partial v^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial v^*}{\partial y^*} = -\ddfrac{\partial p^*}{\partial y^*},
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho^* u^* \ddfrac{\partial p^*}{\partial x^*} + \rho^* v^* \ddfrac{\partial p^*}{\partial y^*} = \gamma p^* \Big(u^* \ddfrac{\partial \rho^*}{\partial x^*} + v^* \ddfrac{\partial \rho^*}{\partial y^*}\Big),
\end{equation}

\end{center}

\noindent
where the flow variable symbols are the same as in the VL.

The equation of state, valid in both layers, the total enthalpy equation in the viscous layer, and linear temperature-viscosity law, respectively, are,

\begin{center}
\vspace{-7mm}
\begin{equation}
p^* = \rho^* R^* T^*
\end{equation}
\vspace{-4mm}
\begin{equation}
H^* = \ddfrac{\gamma}{\gamma - 1} \frac{p^*}{\rho^*} + \ddfrac{u^{*2} + v^{*2}}{2}
\end{equation}
\vspace{-1mm}
\begin{equation}
\ddfrac{\mu^*}{\mu^*_{\infty}} = C\ddfrac{ T^*}{T^*_{\infty}}
\end{equation}

\end{center}

\noindent
where $R^*$ is the universal gas constant, $C$ is the Chapman-Rubesin constant (and is here taken to be unity), and $\gamma$ is the specific heat ratio and assumed to be constant at 1.4.

The hypersonic viscous interaction parameter $\chi$ and the small perturbation parameter $\epsilon$ are here defined to be, respectively,

\begin{center}

$\chi = \ddfrac{M_{\infty}}{Re^{1/6}_{\infty}}$, \hspace{7mm} $\epsilon = \ddfrac{M^{1/2}_{\infty}}{Re^{1/4}_{\infty}} = \ddfrac{\chi^{3/2}}{M_{\infty}}$.

\end{center}

\noindent
The physical quantities are all expanded in terms of the freestream Mach number, $M_{\infty}$, and the small perturbation parameter. The non-dimensionalizations and first order asymptotic representations are thus,

\begin{center}
\vspace{-4mm}
\begin{equation}
\Big( \ddfrac{x^*}{L^*} \hspace{1mm} \ddfrac{y^*}{L^*} \hspace{1mm} \ddfrac{\delta^*}{L^*} \hspace{1mm} \ddfrac{u^*}{U_{\infty}^*} \hspace{1mm} \ddfrac{v^*}{U_{\infty}^*} \hspace{1mm} \ddfrac{p^*}{p_{\infty}^*} \hspace{1mm} \ddfrac{\rho^*}{\rho_{\infty}^*} \hspace{1mm} \ddfrac{\mu^*}{\mu_{\infty}^*} \hspace{1mm} \ddfrac{T^*}{T_{\infty}^*} \hspace{1mm} \ddfrac{H^*}{U_{\infty}^{*2}} \Big)
\end{equation}

\begin{equation}
\begin{tabular}{ccccccccccc}

($x$, & $\epsilon y$, & $\epsilon \delta$, & $u$, & $\epsilon v$, & $\epsilon^2 \gamma M_{\infty}^2 p$, &  $\epsilon^2 \rho$, & $M_{\infty}^2 \mu$, & $\gamma M_{\infty}^2 T$, & $H$) &  in the VL \\

($x$, & $\epsilon y$, & $\epsilon \delta$, & $1 + \epsilon^2 u$, & $\epsilon v$, & $\epsilon^2 \gamma M_{\infty}^2 p$, & $\rho$, & $0$, & $\epsilon^2 \gamma M_{\infty}^2 T$, & $\frac{1}{2} + \epsilon^2 H$) &  in the IL

\end{tabular}
\end{equation}

\end{center}

\noindent
Substituting into the equations of motion, the non-dimensional first-order continuity, streamwise momentum, and energy equations in the VL become, respectively,

\begin{center}
\vspace{-7mm}
\begin{equation}
\ddfrac{\partial (\rho u)}{\partial x} + \ddfrac{\partial (\rho v)}{\partial y} = 0,
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho u \ddfrac{\partial u}{\partial x} + \rho v \ddfrac{\partial u}{\partial y} = -\ddfrac{\partial p}{\partial x} + \ddfrac{\partial}{\partial y}\Big(\mu \ddfrac{\partial u}{\partial y}\Big),
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho u \ddfrac{\partial H}{\partial x} + \rho v \ddfrac{\partial H}{\partial y} = \ddfrac{\partial}{\partial y}\Big(\ddfrac{\mu}{Pr} \ddfrac{\partial H}{\partial y}\Big) + \ddfrac{\partial}{\partial y}\Big[\Big(1 - \ddfrac{1}{Pr}\Big) \mu u \ddfrac{\partial u}{\partial y}\Big],
\end{equation}

\end{center}

\noindent
and in the IL, the non-dimensional first-order continuity, streamwise momentum, transverse momentum, and energy equation become, respectively,

\begin{center}
\vspace{-9mm}
\begin{equation}
\ddfrac{\partial \rho u}{\partial x} + \ddfrac{\partial \rho v}{\partial y} = 0,
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho u \ddfrac{\partial u}{\partial x} + \rho v \ddfrac{\partial u}{\partial y} = -\ddfrac{\partial p}{\partial x},
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho u \ddfrac{\partial v}{\partial x} + \rho v \ddfrac{\partial v}{\partial y} = -\ddfrac{\partial p}{\partial y},
\end{equation}
\vspace{-2mm}
\begin{equation}
\rho u \ddfrac{\partial p}{\partial x} + \rho v \ddfrac{\partial p}{\partial y} = \gamma p (u \ddfrac{\partial \rho}{\partial x} + v \ddfrac{\partial \rho}{\partial y}).
\end{equation}

\end{center}

\newpage

\noindent
The equations of state, total enthalpy, and linear temperature-viscosity law are now, respectively,

\begin{center}
\vspace{-7mm}
\begin{equation}
p = \rho T,
\end{equation}
\vspace{-4mm}
\begin{equation}
H = \ddfrac{1}{2} u^2 + \ddfrac{\gamma}{\gamma - 1} \ddfrac{p}{\rho},
\end{equation}
\vspace{-2mm}
\begin{equation}
\mu = \gamma C T,
\end{equation}

\end{center}

The wall is assumed to be insulated with no velocity slip and at the boundary layer the streamwise velocity is approximately equal to the free-stream velocity. Additionally, the transverse velocity is proportional to the normalized boundary layer growth. With the use the total enthalpy equation the non-dimensional boundary conditions at the wall and the boundary layer edge are thus,

\begin{center}
\vspace{-9mm}
\begin{equation}
u = v = 0, \hspace{5mm} \ddfrac{\partial H}{\partial y} = 0
\end{equation}
\vspace{-2mm}
\begin{equation}
u = 1, \hspace{5mm} v = \ddfrac{d \delta}{d x}, \hspace{5mm} H = \ddfrac{1}{2}.
\end{equation}
\end{center}

\noindent
In the inviscid layer the non-dimensional boundary conditions at the shock wave are the Rankine-Hugoniot shock relations, which are here presented as,

\begin{center}
\vspace{-7mm}
\begin{equation}
u_s = -\ddfrac{2}{\gamma + 1} \Big(\ddfrac{d g}{d x}\Big)^2 \Bigg(1 - \frac{1}{\chi^3 (\frac{d g}{d x})^2}\Bigg),
\end{equation}
\vspace{0.5mm}
\begin{equation}
v_s = \ddfrac{2}{\gamma + 1} \ddfrac{d g}{d x} \Bigg(1 - \frac{1}{\chi^3 (\frac{d g}{d x})^2}\Bigg),
\end{equation}
\vspace{0.5mm}
\begin{equation}
p_s = \ddfrac{2}{\gamma + 1} \Big(\ddfrac{d g}{d x}\Big)^2 \Bigg(1 - \ddfrac{\gamma - 1}{2 \gamma} \frac{1}{\chi^3 (\frac{d g}{d x})^2}\Bigg),
\end{equation}
\vspace{0.5mm}
\begin{equation}
\rho_s = \ddfrac{\gamma + 1}{\gamma - 1} \Bigg(1 + \frac{2}{\chi^3 (\frac{d g}{d x})^2(\gamma - 1)}\Bigg)^{-1},
\end{equation}

\end{center}

\noindent
where $g$ is the function describing the shock wave and the subscript $s$ denotes the flow variables position at the shock wave.

The $x,y$ coordinate system is then transformed into the $\xi,\eta$ coordinate system, which transforms the boundary layer edge curve and shock layer into straight horizontal lines in the viscous layer and inviscid layers,

\begin{center}
\vspace{-9mm}
\begin{equation}
\xi = x, \hspace{5mm} 0 \leq \xi \leq 1 \hspace{5mm} \& \hspace{5mm} \eta = \ddfrac{y - r(x)}{\delta(x) - r(x)}, \hspace{5mm} 0\leq \eta \leq 1, \hspace{3mm} \text{in the VL},
\end{equation}
\begin{equation}
\xi = x, \hspace{5mm} 0 \leq \xi \leq 1 \hspace{5mm} \& \hspace{5mm} \eta = \ddfrac{y - \delta(x)}{g(x) - \delta(x)}, \hspace{5mm} 0\leq \eta \leq 1, \hspace{3mm} \text{in the IL},
\end{equation}

\end{center}

\noindent
where $r(x)$ is the function describing the wall geometry and is equal to $0$ for a flat plate. 

%The resulting continuity, streamwise momentum, and energy equations in the VL are then, respectively,

%\begin{center}
%\vspace{-7mm}
%\begin{equation}
% ({\delta} - {r}) \ddfrac{\partial ({\rho} {u})}{\partial \xi} - \bigg[ \ddfrac{d {r}}{d \xi} + \eta \bigg(\ddfrac{d {\delta}}{d \xi} - \ddfrac{d {r}}{d \xi}\bigg)\bigg] \ddfrac{\partial ({\rho} {u})}{\partial \eta} + \ddfrac{\partial ({\rho} {v})}{\partial \eta} = 0,
%\end{equation}
%\vspace{-1mm}
%\begin{equation}
%(\delta - r)^2 \rho u \ddfrac{\partial u}{\partial \xi} - (\delta - r) \rho u \bigg[ \ddfrac{d r}{d \xi} + \eta \bigg(\ddfrac{d \delta}{d \xi} - \ddfrac{d r}{d \xi}\bigg)\bigg] \ddfrac{\partial u}{\partial \eta} + (\delta - r) \rho v \ddfrac{\partial u}{\partial \eta} + (\delta - r)^2 \ddfrac{\partial p}{\partial \xi} - \ddfrac{\partial}{\partial \eta} \Bigg(\mu \ddfrac{\partial u}{\partial \eta}\bigg) = 0,
%\end{equation}
%\vspace{0.1mm}
%\begin{equation}
%\begin{split}
%\rho u (\delta - r)^2 & \ddfrac{\partial H}{\partial \xi} - \rho u (\delta - r) \bigg[ \ddfrac{d r}{d \xi} + \eta \bigg(\ddfrac{d \delta}{d \xi} - \ddfrac{d r}{d \xi}\bigg)\bigg] \ddfrac{\partial H}{\partial \eta} + \rho v (\delta - r) \ddfrac{\partial H}{\partial \eta} \\
%& - \ddfrac{\partial}{\partial \eta} \bigg(\ddfrac{\mu}{Pr} \ddfrac{\partial H}{\partial \eta}\bigg) - \ddfrac{\partial}{\partial \eta} \bigg[\bigg(1 - \ddfrac{1}{Pr}\bigg) \mu u \ddfrac{\partial u}{\partial \eta}\bigg] = 0,
%\end{split}
%\end{equation}

%\end{center}

%\noindent
%and in the IL the continuity, streamwise momentum, transverse momentum, and energy equations are now, respectively, 
%\begin{center}
%\vspace{-7mm}
%\begin{equation}
%(g - \delta) \ddfrac{\partial \rho}{\partial \xi} + \bigg[(\eta - 1)\ddfrac{d \delta}{d \xi} - \eta \ddfrac{d g}{d \xi} + v\bigg]\ddfrac{\partial \rho}{\partial \eta} + \rho \ddfrac{\partial v}{\partial \eta} = 0,
%\end{equation}
%\vspace{-2mm}
%\begin{equation}
%\rho (g - \delta) \ddfrac{\partial v}{\partial \xi} + \rho \bigg[(\eta - 1) \ddfrac{d \delta}{d \xi} - \eta \ddfrac{d g}{d \xi} + v\bigg] \ddfrac{\partial v}{\partial \eta} + \ddfrac{\partial p}{\partial \eta} = 0,
%\end{equation}
%\vspace{-0.1mm}
%\begin{equation}
%(g - \delta) \bigg(\rho \ddfrac{\partial u}{\partial \xi} + \ddfrac{\partial p}{\partial \xi}\bigg) + \bigg[(\eta - 1)\ddfrac{d \delta}{d \xi} - \eta \ddfrac{d g}{d \xi}\bigg] \bigg(\rho \ddfrac{\partial u}{\partial \eta} + \ddfrac{\partial p}{\partial \eta}\bigg) + \rho v \ddfrac{\partial u}{\partial \eta} = 0,
%\end{equation}
%\vspace{-0.1mm}
%\begin{equation}
%(g - \delta)\bigg(\rho \ddfrac{\partial p}{\partial \xi} - \gamma p \ddfrac{\partial \rho}{\partial \xi}\bigg) + \bigg[(\eta - 1) \ddfrac{d \delta}{d \xi} - \eta \ddfrac{\partial g}{\partial \xi} + v\bigg] \bigg(\rho \ddfrac{\partial p}{\partial \eta} - \gamma p \ddfrac{\partial \rho}{\partial \eta}\bigg) = 0.
%\end{equation}

%\end{center}
 	
Near the leading edge the flow variables acquire a similarity form based on the large growth of the boundary layer in this area. The behavior of the flow variables in terms of the streamwise distance behave as thus in the VL,

\begin{center}
\vspace{-7mm}
\begin{equation}
u \sim O(1), \hspace{2mm} v \sim x^{-1/4}, \hspace{2mm} p \sim x^{-1/2}, \hspace{2mm} \rho \sim x^{-1/2}, \hspace{2mm} T \sim O(1), \hspace{2mm} H \sim O(1), \hspace{2mm} \mu \sim O(1), \hspace{2mm} \delta \sim x^{3/4},
\end{equation}
\end{center}

\noindent
and in the IL,
\begin{center}
\vspace{-7mm}
\begin{equation}
u \sim x^{-1/2}, \hspace{2mm} v \sim x^{-1/4}, \hspace{2mm} p \sim x^{-1/2}, \hspace{2mm} \rho \sim O(1), \hspace{2mm} T \sim x^{-1/2}, \hspace{2mm} H \sim x^{-1/2}, \hspace{2mm} \delta \sim x^{3/4}, \hspace{2mm} g \sim x^{3/4},
\end{equation}
\end{center}

\noindent
leading to the following flow variable similarity solution representations in the $(\xi,\eta)$ coordinate,

\begin{center}
\vspace{-7mm}

\begin{equation}
\begin{tabular}{cc}
$u = \bar{u}(\xi,\eta), \hspace{5mm} v = C_2 \xi^{-1/4} \bar{v}(\xi,\eta), \hspace{5mm} p = C_1 \xi^{-1/2} \bar{p}(\xi), \hspace{5mm} \rho = C C^{-2}_2 \xi^{-1/2} \bar{\rho}(\xi,\eta),$ & \\
& in the VL\\
$T = \bar{T}(\xi,\eta), \hspace{5mm} H = \bar{H}(\xi,\eta), \hspace{5mm} \mu = C \bar{\mu}(\xi,\eta), \hspace{5mm} \delta = C_2 \xi^{3/4} \bar{\delta}(\xi), \hspace{5mm} r = C_2 \xi^{3/4} \bar{r}(\xi),$ &
\end{tabular}
\end{equation}

\begin{equation}
\begin{tabular}{cc}
$u = C_2^2 \xi^{-1/2} \bar{u}(\xi,\eta), \hspace{5mm} v = C_2 \xi^{-1/4} \bar{v}(\xi,\eta), \hspace{5mm} p = C_2^2 \xi^{-1/2} \bar{p}(\xi,\eta), \hspace{5mm} \rho = \bar{\rho}(\xi,\eta),$ & \\
& in the IL\\
$T = C_2^2 \xi^{-1/2} \bar{T}(\xi,\eta), \hspace{5mm} H = C_2^2 \xi^{-1/2} \bar{H}(\xi,\eta), \hspace{5mm} \delta = C_2 \xi^{3/4} \bar{\delta}(\xi), \hspace{5mm} g = C_2 \xi^{3/4} \bar{g}(\xi), $ &
\end{tabular}
\end{equation}

\end{center}

\noindent
where $C_1 = \frac{C C_3}{C_2^2}$, $C_2$, $C_3$, \& $C_4 = C_2 C_5$ are all unknown constants. In the VL, due to the scaling of the density causing it to become infinite at the boundary layer edge, the transverse coordinate $\eta$ is stretched to avoid the resulting singularity, with the new coordinate $\zeta$ defined as,

\begin{center}
\vspace{-7mm}
\begin{equation}
\zeta = \int^1_0 \ddfrac{\bar{\rho}}{{\gamma C_3}^{1/2}} d \eta
\end{equation}

\end{center}

\noindent
A streamfunction is then chosen to satisfy the continuity equation in the VL only,

\begin{center}
\vspace{-5mm}
\begin{equation}
\ddfrac{1}{4} \psi + \xi \ddfrac{\partial \psi}{\partial \xi} = \ddfrac{\bar{\rho}}{(\gamma C_3)^{1/2}}\Big(\Big(\ddfrac{3}{4} \eta \bar{u} \bar{\delta} - \bar{v}\Big) + \xi \eta \bar{u} \ddfrac{d \bar{\delta}}{d \xi}\Big), \hspace{5mm} \ddfrac{\partial \psi}{\partial \eta} = \ddfrac{1}{(\gamma C_3)^{1/2}} \bar{u} \bar{\rho} \bar{\delta}.
\end{equation}

\end{center}

Applying the coordinate change, substituting in the similarity solution representations and using (21), (22), \& (23), the final forms of the streamfunction, streamwise momentum, and energy equation, as well as the auxiliary pressure equation, are in the VL, respectively,

\begin{center}
\vspace{-7mm}
\begin{equation}
\ddfrac{\partial \psi}{\partial \zeta} - \bar{u} \bar{\delta} = 0,
\end{equation}
\vspace{2mm}
\begin{equation}
\begin{split}
4 \xi (\bar{\delta} - \bar{r})^2 \bar{p} \bar{u} \ddfrac{\partial \bar{u}}{\partial \xi} & - \bar{p} (\bar{\delta} - \bar{r})\bigg(4 \xi \ddfrac{\partial \psi}{\partial \xi} + \psi \bigg) \ddfrac{\partial \bar{u}}{\partial \zeta} + 2 \xi \ddfrac{\gamma - 1}{\gamma} (2 \bar{H} - \bar{u}^2)(\bar{\delta} - \bar{r})^2 \ddfrac{\partial \bar{p}}{\partial \xi} \\
& - \ddfrac{\gamma - 1}{\gamma}(\bar{\delta} - \bar{r})^2 (2 \bar{H} - \bar{u}^2) \bar{p} - 4 \bar{p}^2 \ddfrac{\partial^2 \bar{u}}{\partial \zeta^2} = 0,
\end{split}
\end{equation}
\vspace{2mm}
\begin{equation}
4 Pr \xi \bar{u} (\bar{\delta} - \bar{r})^2 \ddfrac{\partial \bar{H}}{\partial \xi} - Pr (\bar{\delta} - \bar{r}) \bigg(4 \xi \ddfrac{\partial \psi}{\partial \xi} + \psi\bigg) \ddfrac{\partial \bar{H}}{\partial \zeta} - 4 \bar{p} \ddfrac{\partial^2 \bar{H}}{\partial \zeta^2} - 4 \bar{p} (Pr - 1) \bigg(\ddfrac{\partial u}{\partial \zeta}\bigg)^2 - 4 \bar{p} \bar{u} (Pr - 1) \ddfrac{\partial^2 \bar{u}}{\partial \zeta^2} = 0,
\end{equation}
\vspace{2mm}
\begin{equation}
\bar{p} = \ddfrac{(\gamma - 1)}{2 (\gamma C_3)^{1/2}} \int^{\infty}_0 \big(2 \bar{H} - \bar{u}^2 \big) d \zeta.
\end{equation}
\end{center}

\noindent
Similarly, in the IL, after applying the coordinate change and substituting in the similarity solution representations, the final forms of the continuity, streamwise momentum, transverse momentum, and energy equations are, respectively,

\begin{center}
\vspace{-7mm}
\begin{equation}
\Big[4 \bar{v} + 3 \bar{\delta} (\eta - 1) - 3 \bar{g} \eta C_5 \Big]\ddfrac{\partial \bar{\rho}}{\partial \eta} + 4 \bar{\rho} \ddfrac{\partial \bar{v}}{\partial \eta} + 4 \xi (C_5 \bar{g} - \bar{\delta}) \ddfrac{\partial \bar{\rho}}{\partial \xi} + 4 \xi \bigg[(\eta - 1)\ddfrac{d \bar{\delta}}{d \xi} - \eta C_5 \ddfrac{d \bar{g}}{d \xi}\bigg] \ddfrac{\partial \bar{g}}{\partial \eta} = 0,
\end{equation}
\vspace{2mm}
\begin{equation}
4 \xi \bar{\rho} (C_5 \bar{g} - \bar{\delta}) \ddfrac{\partial \bar{v}}{\partial \xi} + 4 \xi \bar{\rho} \bigg[(\eta - 1)\ddfrac{d \bar{\delta}}{d \xi} - \eta C_5 \ddfrac{d \bar{g}}{d \xi}\bigg] \ddfrac{\partial \bar{v}}{\partial \eta} - \bar{\rho} \bar{v} (C_5 \bar{g} - \bar{\delta}) + \bar{\rho} \bigg[4 \bar{v} + 3 \bar{\delta} (\eta - 1) - 3 \bar{g} \eta C_5 \bigg] \ddfrac{\partial \bar{v}}{\partial \eta} + 4 \ddfrac{\partial \bar{p}}{\partial \eta} = 0,
\end{equation}
\vspace{2mm}
\begin{equation}
\begin{split}
4 \xi (C_5 \bar{g} - \bar{\delta}) & \bigg(\bar{\rho} \ddfrac{\partial \bar{u}}{\partial \xi} + \ddfrac{\partial \bar{p}}{\partial \xi}\bigg) + 4 \xi \bigg[(\eta - 1)\ddfrac{d \bar{\delta}}{d \xi} - \eta C_5 \ddfrac{d \bar{g}}{d \xi}\bigg] \bigg(\bar{\rho} \ddfrac{\partial \bar{u}}{\partial \eta} + \ddfrac{\partial \bar{p}}{\partial \eta}\bigg) - 2 (C_5 \bar{g} - \bar{\delta}) (\bar{\rho} \bar{u} + \bar{p}) \\
& + \bar{\rho} \Big[4 \bar{v} + 3 \bar{\delta} (\eta - 1) - 3 \bar{g} \eta C_5 \Big] \ddfrac{\partial \bar{u}}{\partial \eta} + \Big[3 \bar{\delta} (\eta - 1) - 3 \eta \bar{g} C_5 \Big]\ddfrac{\partial \bar{p}}{\partial \eta} = 0,
\end{split}
\end{equation}
\vspace{2mm}
\begin{equation}
\begin{split}
4 \xi (C_5 \bar{g} - \bar{\delta}) \bigg(\bar{\rho} \ddfrac{\partial \bar{p}}{\partial \xi} - & \gamma \bar{p} \ddfrac{\partial \bar{\rho}}{\partial \xi}\bigg) + 4 \xi \bigg[(\eta - 1)\ddfrac{d \bar{\delta}}{d \xi} - \eta C_5 \ddfrac{d \bar{g}}{d \xi}\bigg] \bigg(\bar{\rho} \ddfrac{\partial \bar{p}}{\partial \eta} - \gamma \bar{p} \ddfrac{\partial \bar{\rho}}{\partial \eta}\bigg) - 2 (C_5 \bar{g} - \bar{\delta}) \bar{\rho} \bar{p} \\
& + \Big[4 \bar{v} + 3 \bar{\delta} (\eta - 1) - 3 \bar{g} \eta C_5 \Big] \bigg(\bar{\rho} \ddfrac{\partial \bar{p}}{\partial \eta} - \gamma \bar{p} \ddfrac{\partial \bar{\rho}}{\partial \eta}\bigg) = 0,
\end{split}
\end{equation}

\end{center}

\noindent
The boundary conditions in each layer are now,

\begin{center}
\vspace{-7mm}
\begin{equation}
\bar{u} = 0, \hspace{5mm} \psi = 0, \hspace{5mm} \ddfrac{\partial \bar{H}}{\partial \zeta} = 0 \hspace{10mm} \text{at the wall},
\end{equation}
\vspace{-2mm}
\begin{equation}
\bar{u} = 1, \hspace{5mm} \bar{H} = \frac{1}{2}, \hspace{5mm} \bar{v} = \ddfrac{3}{4} \bar{\delta} + \xi \ddfrac{d \bar{\delta}}{d \xi} \hspace{10mm} \text{at the boundary layer},
\end{equation}
\end{center}
\noindent
and at the shock layer,
\begin{center}
\vspace{-7mm}
\begin{equation}
\bar{u}_s = -\ddfrac{2}{\gamma + 1} \Bigg( C_5^2 \bigg(\ddfrac{3}{4} \bar{g} + \xi \ddfrac{d \bar{g}}{d \xi}\bigg) - \ddfrac{\xi^{1/2}}{\chi^3 C_2^2} \Bigg)
\end{equation}
\begin{equation}
\bar{v}_s = \ddfrac{2}{\gamma + 1}\Bigg( C_5\bigg(\ddfrac{3}{4} \bar{g} + \xi \ddfrac{d \bar{g}}{d \xi}\bigg) - \frac{\xi^{1/2}}{ C_5(\frac{3}{4} \bar{g} + \xi \frac{d \bar{g}}{d \xi}) \chi^3 C_2^2 } \Bigg)
\end{equation}
%\vspace{-2mm}
\begin{equation}
\bar{p}_s = \ddfrac{2}{\gamma + 1} \Bigg( C_5^2 \bigg(\ddfrac{3}{4} \bar{g} + \xi \ddfrac{d \bar{g}}{d \xi} \bigg)^2 - \ddfrac{\gamma - 1}{2 \gamma} \ddfrac{\xi^{1/2}}{\chi^3 C_2^2} \Bigg)
\end{equation}
%\vspace{-2mm}
\begin{equation}	
\bar{\rho}_s = \ddfrac{\gamma + 1}{\gamma - 1} \Bigg(1 + \ddfrac{2}{\gamma - 1} \frac{\xi^{1/2}}{ C_5^2 (\frac{3}{4} \bar{g} + \xi \frac{d \bar{g}}{d \xi})^2 \chi^3 C_2^2 } \Bigg)
\end{equation}

\end{center}

\vspace{3mm}

In the initial profile, where $\xi \rightarrow 0$, the equations reduce to non-linear ODEs dependent only on the transverse coordinate with $\bar{p}$, $\bar{\delta}$, and $\bar{g}$ all being unity. In the VL the flow variables $\psi$, $\bar{u}$, and $\bar{H}$ are determined numerically by a relaxation method and $C_3$ by numerical integration. The distance between the wall, at $j = 1$ and the boundary layer, at point $J$, is discretized into $J-1$ points where $1 \leq j \leq J$. The flow variables are averaged with respect to the $(i = 1, j - \frac{1}{2})$ point, where $i$ is the streamwise point, in the stream function and the $(i=1,j)$ point in the momentum and energy equations, and all derivatives replaced with central differences. All flow variables are then assumed to be of the form $\bar{f} = \hat{f} + \check{f}$, where $\check{f} \ll 1$ and a linearized system is obtained that can be written as algebraic equations in the form,

\begin{center}
\vspace{-7mm}
\begin{equation}
(...)\check{f}_{i,j-1} + (...)\check{f}_{i,j} + (...)\check{f}_{i,j+1} = R.H.S. \Rightarrow \mathbf{A} \mathbf{\check{f}} = \mathbf{R}
\end{equation}
\end{center}

The coefficients of $\check{f}$ and the right-hand side are functions of $\hat{f}$, $\mathbf{A}$ is here an $(n \hspace{2mm} \text{x} \hspace{2mm} n)$ matrix with $n = 3J$ and $\mathbf{\hat{f}}$ and $\mathbf{R}$ are column vectors. Initial guesses are made for the flow variables that satisfy the boundary conditions and the $\mathbf{\hat{f}}$ vector is  repeatedly computed by forward Gaussian Elimination and back substitution and added back into the flow variables until $\hat{f} \rightarrow \bar{f}$ and the converged solution is obtained. $C_3$ is then determined by applying the trapezoidal rule to the auxilary pressure equation.

This process is repeated for the IL initial profile. Here the flow variables $\bar{\rho}$, $\bar{p}$, $\bar{v}$, and the constant $C_5$ are determined numerically by a relaxation method. The flow variables are averaged with respect to the $(i = 1, j - \frac{1}{2})$ point in all the equations of motion and the derivatives replaced with central differences. The flow variables and $C_5$ are then assumed to be of the form $\bar{f} = \hat{f} + \check{f}$, where $\check{f} \ll 1$ and a linearized system is obtained that can be written as algebraic equations in the form,

\begin{center}
\vspace{-7mm}
\begin{equation}
(...)\check{f}_{i,j-1} + (...)\check{f}_{i,j} + (...)\check{f}_{i,j+1} + (...)\check{C_5} = R.H.S. \Rightarrow \mathbf{A} \mathbf{\check{f}} = \mathbf{R}
\end{equation}
\end{center}

\noindent
What's different in the IL is that $C_5$ is solved with the rest of the flow variables by adding an additional row and column to $\mathbf{A}$ so that it is here an $(n+1 \hspace{2mm} \text{x} \hspace{2mm} n+1)$ matrix with the extra column holding the $\check{C_5}$ elements from the linearized equations of motion. As in the VL, initial guesses are made for the flow variables that satisfy the boundary conditions and the $\mathbf{\hat{f}}$ vector is repeatedly computed by forward Gaussian Elimination and back substitution and added back into the flow variables until $\hat{f} \rightarrow \bar{f}$ and the converged solution is obtained. Further, the streamwise velocity $\bar{u}$ is determined after the fact directly from the streamwise momentum equation. The VL and the IL at the leading edge are then matched by solving for the $C_1$ and $C_2$ constants, determined as $C_1 = \frac{C C_3}{C_2^2}$ and $C_2 = \big(\frac{C C_3}{\bar{p}^{IL}_{j = 1}}\big)^{\frac{1}{4}}$. For the adiabatic case, with $Pr = 1$, the constants are determined to be,

\begin{center}

$C_1 = 0.39571660$ \\
$C_2 = 0.70349850$ \\
$C_3 = 0.19584416$ \\
$C_4 = 1.18993916$ \\
$C_5 = 1.69145941$

\end{center}

Downstream, and only in the viscous layer, the numerical method is further repeated, but with some major changes. For the adiabatic case, with $Pr = 1$, the energy equation is completely zeroed out once the adiabatic enthalpy is determined in the initial profile meaning that $\bar(H) = 0.5$ everywhere. After discretizing the distance between the leading edge, at the $i=1$ point, and the trailing edge, at the $I$ point, into $I-1$ points, where $1 \leq i \leq I$, the remaining VL flow variables $\psi$ and $\bar{u}$ are averaged with respect to the $(i - \frac{1}{2}, j - \frac{1}{2})$ point in the stream function and the $(i-\frac{1}{2},j)$ point in the momentum equation and all derivatives are replaced with central differences.

The VL initial profile can some-what be determined in isolation from the IL (though doing so leaves $C_2$ unsolved), the downstream viscous layer can't be determined to any degree without also finding the pressure and boundary layer thickness downstream. For the adiabatic case presented here, determining the flow in both the inviscid and viscous layer using the numerical method so far presented can't be done for any great length downstream, as detailed in [2]. To overcome this and determine the behavior of the flow in the downstream VL, in the same way described in [2], the tangent wedge approximation is used, with the caveat that this approach gives no information about the inviscid layer nor the position of the shockwave. Using the tangent wedge approximation, the non-dimensional pressure can be expressed in terms of the boundary layer slope,
 
\begin{center}
\vspace{-7mm}
\begin{equation}
p = \ddfrac{1}{\gamma \chi^3} + \ddfrac{\gamma + 1}{4} \bigg(\ddfrac{d \delta}{d x} \bigg)^2 + \ddfrac{d \delta}{d x} \Bigg[ \bigg(\ddfrac{\gamma + 1}{4} \bigg) \bigg( \ddfrac{d \delta}{d x} \bigg)^2 + \ddfrac{1}{\chi^3} \Bigg]^{1/2},
\end{equation}
\end{center}

\noindent
which, after applying the coordinate change, appropriate similarity representations, averaging the flow variables with respect to the $(i-\frac{1}{2})$ point, since $\bar{p}$ and $\bar{\delta}$ are only dependent on the streamwise distance, and replacing the derivatives with central differences, this expression can then be used to determine the boundary layer thickness as,

\begin{center}
\vspace{-7mm}
\begin{equation}
\bar{\delta}_i = \Bigg[ \ddfrac{8 \Delta \xi}{C_2} \ddfrac{p_1 \big((\xi_i + \xi_{i-1})/2 \big)^{1/4}}{\big[(\gamma + 1) p_1 + 2 \chi^{-3}\big]^{1/2}} + \big(4 (\xi_i + \xi_{i-1}) - 3 \Delta \xi \big) \bar{\delta}_{i-1} \Bigg] \bigg/ \big(4 (\xi_i + \xi_{i-1}) + 3 \Delta \xi \big)
\end{equation}
\end{center}

\noindent
where,
\begin{center}
\vspace{-7mm}
\begin{equation}
p_1 = \ddfrac{C_1 \big(\bar{p}_i + \bar{p}_{i-1} \big)}{\big( 2 (\xi_i + \xi_{i-1}) \big)^{1/2}} - \ddfrac{1}{\gamma \chi^3}
\end{equation}
\end{center}

To bring the upstream influence in directly, when the derivatives are replaced with central differences, $\frac{\partial \bar{p}}{\partial \xi}$ is forward differenced rather than backward differenced, meaning $\frac{\partial \bar{p}}{\partial \xi} \approx \frac{1}{\Delta \xi} ( \bar{p}_{i+1} - \bar{p}_{i} )$ in the linearized equations. As in the initial profile, the flow variables are then assumed to be of the form $\bar{f} = \hat{f} + \check{f}$, where $\check{f} \ll 1$ and a linearized system is obtained that can be written as algebraic equations in the form,

\begin{center}
\vspace{-7mm}
\begin{equation}
(...)\check{f}_{i,j-1} + (...)\check{f}_{i,j} + (...)\check{f}_{i,j+1} + (...)\check{p}_i = R.H.S. \Rightarrow \mathbf{A} \mathbf{\check{f}} = \mathbf{R}
\end{equation}
\end{center}

\noindent
with $\bar{p}_i$ solved alongside the rest of the flow variables similar to how $C_5$ was computed in the IL initial profile. Thus, $\mathbf{A}$ is here a $(n+1 \hspace{2mm} \text{x} \hspace{2mm} n+1)$ matrix with the extra column holding the $\check{p}$ elements in the momentum equation and the extra row holding the auxiliary pressure equation (with the trapezoidal rule applied). For each step downstream the converged values for $\psi$ and $\bar{u}$ from the previous step are used as initial guesses for the next and the $\mathbf{\hat{f}}$ vector is repeatedly computed by forward Gaussian Elimination and back substitution and added back into the flow variables until $\hat{f} \rightarrow \bar{f}$ and the converged solution is obtained for that step. This is repeated for each point downstream until $i = I$ at the trailing edge.

With the inclusion of the tangent wedge approximation, multiple sweeps are required from $i = 2$ to $I-1$ in order for the solution to converge at all steps downstream. In the first sweep the values of $\bar{p}_i$ from $i = 2$ to $I-1$ are assumed to be unity, the value of  $\bar{p}_{i = 1}$. At the trailing edge, where $\frac{d \bar{\delta}}{d \xi} \rightarrow 0$, $\bar{p}_I$ is set at $\frac{1}{C_1 \gamma \chi^3}$, it's theoretical value according to (54), and remains unchanged through the subsequent sweeps. The pressure values from the previous sweep, $[\bar{p}_i]^{n-1}$ , where $n$ is the sweep number, is saved and used as initial guesses in the next sweep. These sweeps are repeated until $[\bar{p}_i]^{n} - [\bar{p}_i]^{n-1} \rightarrow 0$ and the solution has converged.

This method of first determining and matching the viscous and inviscid layers at the leading edge, then marching downstream in the viscous layer in multiple sweeps with the tangent wedge to find the pressure through the entire layer, is used to determine the flow behavior for a range of $\chi$ values and compression ramp angles. The compression ramp considered here is a sharp angled ramp located at $\frac{x}{L} = 0.5$. The pressure profiles from the compression ramp cases are compared to the flat plate case and the point just ahead of the corner where the pressure difference is becomes greater than 1\% to the ramp corner is determined as the upstream influence length. 

As in [1], the ratio of the upstream influence length to the undisturbed boundary layer thickness is predicted using the similarity parameter $\frac{Re \Delta p}{\gamma M_\infty^2 p_\infty}$. The free stream Mach, $M_\infty$, is picked for each case of $\chi$ to best fit the full sweep of compression ramp angles and the corresponding $Re$ number is worked out after. Additionally, $\Delta p$ is determined by the equation for pressure rise across an oblique shock in accordance with Oblique Shock Wave Theory. Thus,

\begin{center}
\vspace{-7mm}
\begin{equation}
	\Delta p = \ddfrac{2 \gamma}{\gamma + 1} \big(M_\infty^2 sin^2 \beta - 1 \big),
\end{equation}
\end{center}

\noindent
where $\beta$ is assumed to be the weak shock angle. The $\chi$ values with the chosen $M_{\infty}$ and resulting $Re$ number are tabulated below.

\begin{center}

\begin{large}
\begin{tabular}{c | c | c}
	$\chi$ & $M_\infty$ & $Re$ \\
	-----&-----&----- \\
	1 & 2.44 & 212.07 \\
	1.5 & 3.50 & 161.38 \\
	2 & 4.62 & 151.15 \\
	2.5 & 5.80 & 155.12 \\
	3 & 7.00 & 161.8 \\
	3.5 & 8.23 & 168.43 \\
	4 & 9.45 & 174.09
\end{tabular}
\end{large}
\end{center}

\newpage

Figure 1 contains the correlation results for the upstream influence length predicted by the similarity parameter against the acutal upstream influence length, all for the various $\chi$ and compression ramp and angles.

\begin{center}

\vspace{-5mm}

\begin{figure}[htbp]
\hspace{-13mm}
\includegraphics[scale = 0.35]{allAoA}
\caption{Correlation Results of Upstream Influence Lengths}
\end{figure}

\end{center}

\vspace{-5mm}

\noindent
It should be noted that the hypersonic flow theory used predicts relatively small $Re$ numbers when compared to the correlation results in [1]. 

\newpage

\section*{References}

[1] Miller, J.H. “Elliptic Length Scales in Laminar, Two-Dimensional Supersonic Flows”, AFRL-RQ-WP-TP-2015-0109, June 2015

\vspace{3mm}
\noindent
[2] A. Farid Khorrami and Frank T. Smith. "Hypersonic aerodynamics on thin bodies with interaction and upstream influence", Journal of Fluid Dynamics, 277, pp 85-108. 1994

\vspace{3mm}
\noindent
[3] Khorrami, A. F. "Hypersonic aerodynamics on flat plates and thin airfoils", D. Phil. thesis, Oxford University (Dept. of Engineering Science and Lady Margaret Hall). 1991

\vspace{3mm}
\noindent
[4] Hayes, W. D. and Probstein, R. F. "Hypersonic Flow Theory, 2nd ed", Academic Press, 1966

\vspace{3mm}
\noindent
[5] Stewartson, K. "The Theory of Laminar Boundary Layers in Compressible Fluids", Clarendon Press, 1964

\vspace{3mm}
\noindent
[6] Tannehill, J.C., Anderson, D.A., and Pletcher, R.H. "Computational Fluid Dynamics and Heat Transfer, 3rd ed.", 2013


\end{document}